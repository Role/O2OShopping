package com.dashihui.afford.wxapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.dashihui.afford.R;
import com.dashihui.afford.business.entity.EtySendToUI;
import com.dashihui.afford.common.base.BaseActivity;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.io.ByteArrayOutputStream;

public class WXEntryActivity extends BaseActivity {

    @ViewInject(R.id.cancel)
    private TextView mTxtCancel;

    private IWXAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aty_share);
        ViewUtils.inject(this);

        // 通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(WXEntryActivity.this, Constants.APP_ID, true);
//        api.handleIntent(getIntent(), this);
        api.registerApp(Constants.APP_ID);
    }


    @Override
    public void onSuccess(EtySendToUI beanSendUI) {

    }

    @Override
    public void onFailure(EtySendToUI beanSendUI) {
    }
    @OnClick({(R.id.cancel),(R.id.wxFriend),(R.id.wxCircleFriends)})
    public void onAllClick(View view){
        switch (view.getId()){
            case R.id.cancel://取消
                onBackPressed();
                break;
            case R.id.wxFriend://微信好友
                updateWXStatus(false);
                break;
            case R.id.wxCircleFriends://微信朋友圈
                updateWXStatus(true);
                break;
            default:
                break;
        }
    }


    /**
     * 更新为微信朋友圈状态
     *
     */
    public void updateWXStatus(boolean isFriend) {
        LogUtils.e("onReq==========0000======>");
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = "http://www.91dashihui.com/share";
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = "下载大实惠APP，即享蔬菜水果、酒水饮料、生活百货0元配送，15分钟到家，快来试试吧~";
        msg.description = "大实惠云商服务平台是您家门口的社区E站，不仅提供蔬菜水果、酒水饮料、生活百货、粮油调料、营养早餐的在线购买和线下配送服务，还为小区业主提供家政、洗衣、丽人、洗车、开锁、维修等便捷的到家服务。旨在开启全新的社区便利生活！";
        Bitmap thumb = BitmapFactory.decodeResource(getResources(), R.drawable.about_icon_logo);
        msg.thumbData = bmpToByteArray(thumb, true);

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("大实惠云商平台");
        req.message = msg;
        req.scene = isFriend ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
        LogUtils.e("onReq==========1111=webpage=====>" );
        finish();
    }

    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis())
                : type + System.currentTimeMillis();
    }

    public static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
