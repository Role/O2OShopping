//
//  Strore_detail_ContentCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
#import "Strore_detail_ContentCell.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
#import "NSString+TextSize.h"
//view
#import "KZLinkLabel.h"
#define kleftSpace 15
#define ktopSpace 11
#define ksepLineHight 0.5
@interface Strore_detail_ContentCell(){
    
}
@property (strong ,nonatomic) KZLinkLabel *goodsTitle;//商品名称
@property (strong ,nonatomic) UILabel *goodsDescribe;//商品描述
@property (retain,nonatomic)  UILabel *goodsSaleCount;//产品销售量
@property (retain,nonatomic)  UILabel *goodsMarkCount;//产品关注
@property (strong ,nonatomic) UILabel *goodsCurrentPrice;//商品当前价格
@property (strong ,nonatomic) UILabel *goodsOldPrice;//产品原价格
@property (strong ,nonatomic) UIImageView *secondSepLineImageView;

@end
@implementation Strore_detail_ContentCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSubview];
    }
    return self;
}
#pragma mark -初始化控件
-(void)setUpSubview {
    //分割线
    UIImage *sepLineImage = [UIImage imageNamed:@"gray_line"];
    UIImageView *firstSepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
    firstSepLineImageView.frame = CGRectMake(0, 0, VIEW_WIDTH, ksepLineHight);
    [self.contentView addSubview:firstSepLineImageView];
    //商品名称
    _goodsTitle = [[KZLinkLabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                               ,CGRectGetMaxY(firstSepLineImageView.frame) + ktopSpace
                                                               ,VIEW_WIDTH - 2*kleftSpace
                                                               , HightScalar(32))];
    _goodsTitle.font = [UIFont systemFontOfSize:FontSize(16)];
    _goodsTitle.automaticLinkDetectionEnabled = YES;
    _goodsTitle.textAlignment = NSTextAlignmentLeft;
    _goodsTitle.lineBreakMode = NSLineBreakByTruncatingTail;
    _goodsTitle.numberOfLines = 2;
    _goodsTitle.textColor = [UIColor colorWithHexString:@"#555555"];
    [self.contentView addSubview:_goodsTitle];
    //商品描述
    _goodsDescribe = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_goodsTitle.frame)
                                                              , CGRectGetMaxY(_goodsTitle.frame)
                                                              , CGRectGetWidth(_goodsTitle.bounds)
                                                              , HightScalar(31))];
    _goodsDescribe.textColor = [UIColor colorWithHexString:@"#e73f18"];
    _goodsDescribe.font = [UIFont systemFontOfSize:FontSize(15)];
    _goodsDescribe.lineBreakMode = NSLineBreakByTruncatingTail;
    _goodsDescribe.numberOfLines = 2;
    [self.contentView addSubview:_goodsDescribe];
    //产品销售量
    _goodsSaleCount = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_goodsDescribe.frame)
                                                              , CGRectGetMaxY(_goodsDescribe.frame)
                                                              , 120
                                                               , HightScalar(31))];
    _goodsSaleCount.textColor = [UIColor colorWithHexString:@"#999999"];
    _goodsSaleCount.font = [UIFont systemFontOfSize:FontSize(15)];
    [self.contentView addSubview:_goodsSaleCount];
    //产品关注
    _goodsMarkCount = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_goodsSaleCount.frame) + 12
                                                               , CGRectGetMinY(_goodsSaleCount.frame)
                                                               , CGRectGetWidth(_goodsSaleCount.bounds)
                                                               , CGRectGetHeight(_goodsSaleCount.bounds))];
    _goodsMarkCount.textColor = [UIColor colorWithHexString:@"#999999"];
    _goodsMarkCount.font = [UIFont systemFontOfSize:FontSize(15)];
    [self.contentView addSubview:_goodsMarkCount];
    //商品当前价格
    _goodsCurrentPrice = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_goodsSaleCount.frame)
                                                               , CGRectGetMaxY(_goodsSaleCount.frame)
                                                               , CGRectGetWidth(_goodsSaleCount.bounds)
                                                               , HightScalar(38))];
    _goodsCurrentPrice.textColor = [UIColor colorWithHexString:@"#e73f18"];
    _goodsCurrentPrice.font = [UIFont systemFontOfSize:FontSize(16)];
    [self.contentView addSubview:_goodsCurrentPrice];
    //产品原价格
    _goodsOldPrice = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_goodsCurrentPrice.frame) + 42
                                                               , CGRectGetMidY(_goodsCurrentPrice.frame) - HightScalar(13)/2
                                                               , CGRectGetWidth(_goodsCurrentPrice.bounds)
                                                               , HightScalar(13))];
    _goodsOldPrice.textColor = [UIColor colorWithHexString:@"#999999"];
    _goodsOldPrice.font = [UIFont systemFontOfSize:FontSize(13)];
    [self.contentView addSubview:_goodsOldPrice];
    
    //分割线
    _secondSepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
    _secondSepLineImageView.frame = CGRectMake(kleftSpace
                                              , CGRectGetMaxY(_goodsCurrentPrice.frame) + 9
                                              , VIEW_WIDTH - kleftSpace
                                              , ksepLineHight);
    [self.contentView addSubview:_secondSepLineImageView];
}

-(void)updateViewWithData:(NSDictionary *)dataDic {
    NSString *title = [dataDic objectForKey:@"title"];
    NSString *desctibe = [dataDic objectForKey:@"describe"];
    NSString *saleCount = [dataDic objectForKey:@"saleCount"];
    NSString *markCount = [dataDic objectForKey:@"markCount"];
    NSString *currentPrice = [dataDic objectForKey:@"currentPrice"];
    NSString *oldePrice = [dataDic objectForKey:@"oldPrice"];
    //商品名字
    CGSize goodsTitleSize = [title sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(2*(VIEW_WIDTH - 2*kleftSpace), HightScalar(32))];
    CGRect goodsTitleFrame = self.goodsTitle.frame;
    if (goodsTitleSize.width > (VIEW_WIDTH - 2*kleftSpace)) {//标题信息大于一行
        goodsTitleFrame.size.height = HightScalar(44);
    }
    self.goodsTitle.frame = goodsTitleFrame;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:FontSize(16)]};
    NSAttributedString *attributedString = [NSAttributedString emotionAttributedStringFrom:title attributes:attributes];
    self.goodsTitle.attributedText =attributedString;
    //商品简介
    CGSize goodsDescribeSize = CGSizeZero;
    CGRect goodsDescribeFrame = CGRectZero;
    if (desctibe.length != 0) {
        goodsDescribeSize = [desctibe sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(2*(VIEW_WIDTH - 2*kleftSpace), HightScalar(31))];
        goodsDescribeFrame = self.goodsTitle.frame;
        goodsDescribeFrame.origin.y = CGRectGetMaxY(self.goodsTitle.frame);
        if (goodsDescribeSize.width > (VIEW_WIDTH - 2*kleftSpace)) {//描述信息大于一行
            goodsDescribeFrame.size.height = HightScalar(44);
        }
        self.goodsDescribe.frame = goodsDescribeFrame;
        self.goodsDescribe.text = desctibe;
    } else {
        goodsDescribeFrame = self.goodsTitle.frame;
        goodsDescribeFrame.size.height = 0;
        goodsDescribeFrame.origin.y = CGRectGetMaxY(self.goodsTitle.frame);
        self.goodsDescribe.frame = goodsDescribeFrame;
    }
    
    //产品销售量
    CGSize goodsSaleCountSize = [saleCount sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(2*(VIEW_WIDTH), HightScalar(31))];
    CGRect goodsSaleCountFrame = self.goodsSaleCount.frame;
    goodsSaleCountFrame.origin.y = CGRectGetMaxY(self.goodsDescribe.frame);
    goodsSaleCountFrame.size.width = goodsSaleCountSize.width;
    self.goodsSaleCount.frame = goodsSaleCountFrame;
    self.goodsSaleCount.text = saleCount;
    //产品关注
    CGSize goodsMarkCountSize = [markCount sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(2*(VIEW_WIDTH), HightScalar(31))];
    CGRect goodsMarkCountFrame = self.goodsMarkCount.frame;
    goodsMarkCountFrame.origin.y = CGRectGetMinY(self.goodsSaleCount.frame);
    goodsMarkCountFrame.origin.x = CGRectGetMaxX(self.goodsSaleCount.frame) + 12;
    goodsMarkCountFrame.size.width = goodsMarkCountSize.width;
    self.goodsMarkCount.frame = goodsMarkCountFrame;
    self.goodsMarkCount.text = markCount;
     //商品当前价格
    CGSize goodsCurrentPriceSize = [currentPrice sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(2*(VIEW_WIDTH), HightScalar(31))];
    CGRect goodsCurrentPriceFrame = self.goodsCurrentPrice.frame;
    goodsCurrentPriceFrame.origin.y = CGRectGetMaxY(self.goodsSaleCount.frame);
    goodsCurrentPriceFrame.size.width = goodsCurrentPriceSize.width;
    self.goodsCurrentPrice.frame = goodsCurrentPriceFrame;
    self.goodsCurrentPrice.text = currentPrice;
    //产品原价格
    CGSize goodsOldPriceSize = [oldePrice sizeWithFont:[UIFont systemFontOfSize:FontSize(13)] maxSize:CGSizeMake(2*(VIEW_WIDTH), HightScalar(31))];
    CGRect goodsOldPriceFrame = self.goodsOldPrice.frame;
    goodsOldPriceFrame.origin.y = CGRectGetMidY(self.goodsCurrentPrice.frame) - FontSize(13)/2;
    goodsOldPriceFrame.origin.x = CGRectGetMaxX(self.goodsCurrentPrice.frame) + 42;
    goodsOldPriceFrame.size.width = goodsOldPriceSize.width;
    self.goodsOldPrice.frame = goodsOldPriceFrame;
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:oldePrice];
    [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, oldePrice.length)];
    self.goodsOldPrice.attributedText = attrString;
    
    //分割线
    CGRect secondSepLineFrame = self.secondSepLineImageView.frame;
    secondSepLineFrame.origin.y = CGRectGetMaxY(_goodsCurrentPrice.frame) + 9;
    self.secondSepLineImageView.frame = secondSepLineFrame;
    
}

- (void)updateMarkCount:(NSString*)markCount {
    NSString *markCountStr = [NSString stringWithFormat:@"关注%@",markCount];
    CGSize goodsMarkCountSize = [markCountStr sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(2*(VIEW_WIDTH), HightScalar(31))];
    CGRect goodsMarkCountFrame = self.goodsMarkCount.frame;
    goodsMarkCountFrame.size.width = goodsMarkCountSize.width;
    self.goodsMarkCount.frame = goodsMarkCountFrame;
    self.goodsMarkCount.text = markCountStr;
}
//计算实际高度
- (CGFloat)cellFactHight {
    return CGRectGetMaxY(_secondSepLineImageView.frame);
}

@end
