<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">店铺商品信息</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-1 col-lg-11">
				<select id="sState" class="selectpicker pull-left" data-width="10%">
					<option value="0">全部</option>
					<option value="1">在售</option>
					<option value="2">已下架</option>
				</select>
				<select id="sCategoryonid" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/category" data-isfirst="true" data-next="#sCategorytwid" data-key="id:name"></select>
				<select id="sCategorytwid" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/category/{value}" data-next="#sCategorythid" data-key="id:name"></select>
				<select id="sCategorythid" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/category/{value}" data-next="#sCategoryfoid" data-key="id:name"></select>
				<select id="sCategoryfoid" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/category/{value}" data-key="id:name"></select>
				<select id="sType" class="selectpicker pull-left p-l-5" data-width="10%">
					<option value="0">请选择</option>
					<option value="1">普通</option>
					<option value="2">推荐</option>
					<option value="3">限量</option>
					<option value="4">一元购</option>
				</select>
				<select id="sTag" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/goodsTag" data-key="id:name" data-issingle="true" multiple></select>
				<input type="text" id="sKeyword" value="" class="form-control pull-left m-l-5 width200" placeholder="搜索关键字" maxlength="100">
				<button type="button" class="btn btn-success pull-left m-l-5" onclick="doSearch()">查询</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div><p></p></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
							<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
							<a href="${BASE_PATH}/goods/base/index" class="btn btn-link"><span class="fa fa-copy"></span> 复制</a>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-hover">
			            <thead>
			                <tr>
			                	<th width="5%"></th>
								<th>商品名称</th>
								<th width="8%">市场价(元)</th>
								<th width="8%">类型</th>
								<th width="8%">销售价(元)</th>
								<th width="8%">状态</th>
								<th width="10%">记录时间</th>
								<th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="thumbTpl">
<a href="javascript:void(0)" onclick="Kit.photo('{{thumb}}')" class="center-block thumbnail thumbnail-none-margin width50"><img src="{{thumb}}"></a>
</script>
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td><a href="javascript:void(0)" onclick="Kit.photo('${FTP_PATH}{{item.thumb}}')" class="center-block thumbnail thumbnail-none-margin width50"><img src="${FTP_PATH}{{item.thumb}}"></a></td>
		<td>{{item.name}}</td>
		<td>{{item.marketPrice}}</td>
		<td>{{item.type | flagTransform:1,'普通',2,'推荐',3,'限量',4,'一元购'}}</td>
		<td>{{item.sellPrice}}</td>
		<td>{{item.state | flagTransform:1,'在售',2,'下架'}}</td>
		<td>{{item.createDate | dateFormat:'yyyy-MM-dd'}}</td>
		<td>{{if item.state==1}}
			<button type="button" class="btn btn-sm btn-warning btn-simple" onclick="doUpDown('{{item.id}}','x')" title="下架"><span class="fa fa-arrow-down fa-lg"></span></button>
			{{/if}}
			{{if item.state==2}}
			<button type="button" class="btn btn-sm btn-success btn-simple" onclick="doUpDown('{{item.id}}','s')" title="上架"><span class="fa fa-arrow-up fa-lg"></span></button>
			{{/if}}
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-info btn-simple" onclick="toDetailEdit('{{item.id}}')" title="图文编辑"><span class="fa fa-file-text-o fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-success btn-simple" onclick="toUploadImages('{{item.id}}')" title="商品图片"><span class="fa fa-file-image-o fa-lg"></span></button> 
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="doDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button> 
		</td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(doSearch);
function doSearch(){
	var s = $("#sState").val(),c1 = $("#sCategoryonid").val(), c2 = $("#sCategorytwid").val(), c3 = $("#sCategorythid").val(), c4 = $("#sCategoryfoid").val(), t = $("#sType").val(), k = $("#sKeyword").val();
	var tag = $("#sTag").val();
	if(tag&&tag.length>0){
		tag = tag.join(",");
	}
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/goods/page",{s:s,c1:c1,c2:c2,c3:c3,c4:c4,t:t,k:k,tag:tag,"pageSize":10},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/goods/toAdd").open();
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/goods/toEdit/"+id).open();
}
function onEditSuccess(newObject){
	$("#item"+newObject.id).replaceWith(template("dataTpl",{"list":[newObject]}));
}
var detailEditDialog;
function toDetailEdit(id){
	detailEditDialog = Kit.dialog("图文编辑","${BASE_PATH}/goods/toDetailEdit/"+id,{size:'size-wide'}).open();
}
var imagesDialog;
function toUploadImages(id){
	imagesDialog = Kit.dialog("商品图片","${BASE_PATH}/goods/imageIndex/"+id,{closable:true,size:"size-wide"}).open();
}
function onLogoSet(id,thumb){
	$("#item"+id+" a").replaceWith(template("thumbTpl",{"thumb":thumb}));
}
function doDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/goods/doDelete/"+id,function(result){
			if(result.flag==0){
				$("#item"+id).remove();
			}else{
				Kit.alert("删除失败");return;
			}
		});
	});
}
function doUpDown(id,flag){
	var tipmessage;
	if(flag=='s'){
		message="你确定要上架此商品吗？";
	}else{
		message="你确定要下架此商品吗？";
	}
	Kit.confirm("提示",message,function(){
		$.post("${BASE_PATH}/goods/doUpDownGoods",{'id':id,'flag':flag},function(result){
			if(result.flag==0){
				dataPaginator.loadPage(1);
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
</script>
</body>
</html>