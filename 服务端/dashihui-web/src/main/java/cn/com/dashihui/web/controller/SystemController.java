package cn.com.dashihui.web.controller;

import com.jfinal.kit.StrKit;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Join;
import cn.com.dashihui.web.kit.CommonKit;
import cn.com.dashihui.web.kit.ValidateKit;
import cn.com.dashihui.web.service.NoticeService;

public class SystemController extends BaseController{
	private NoticeService noticeService = new NoticeService();
    
    public void index(){
    	//查询公司新闻3条
    	this.setAttr("noticeList1", noticeService.findByPage(1, 1, 5).getList());
        this.render("index.jsp");
    }
    
    /**
     * 产品服务
     */
    public void product(){
    	render("product.jsp");
    }
    
    /**
     * 合作加盟
     */
    public void cooperate(){
    	render("cooperate.jsp");
    }
    
    /**
     * 招聘
     */
    public void welcome(){
    	render("welcome.jsp");
    }
    
    /**
     * 关于我们
     */
    public void about(){
    	render("about.jsp");
    }
    
    /**
     * 联系我们
     */
    public void contact() {
    	render("contact.jsp");
    }
    
    /**
     * 提交留言
     */
    public void doJoin(){
    	String ip = CommonKit.getClientIp(getRequest());
    	long now = System.currentTimeMillis();
    	/*if(!StrKit.isBlank(ip)){
    		long last = 0;
    		if(getSessionAttr(ip)!=null){
    			last = getSessionAttr(ip);
    			if((now-last)/(60*1000)<5){
    				//请求太频繁
    				renderResult(13);
    				return;
    			}
    		}
    		setSessionAttr(ip, now);
    	}*/
    	String name = getPara("name");
    	String msisdn = getPara("msisdn");
    	String qq = getPara("qq");
    	String mail = getPara("mail");
    	String context = getPara("context");
    	if(StrKit.isBlank(name)){
    		renderResult(1);
    	}else if(!ValidateKit.Chinese(name)){
    		renderResult(2);
    	}else if(name.length()>10){
    		renderResult(3);
    	}else if(StrKit.isBlank(msisdn)){
    		renderResult(4);
    	}else if(!ValidateKit.Mobile(msisdn)){
    		renderResult(5);
    	}else if(!StrKit.isBlank(qq)&&!ValidateKit.QQnumber(qq)){
    		renderResult(6);
    	}else if(qq.length()>12){
    		renderResult(7);
    	}else if(!StrKit.isBlank(mail)&&!ValidateKit.Email(mail)){
    		renderResult(8);
    	}else if(mail.length()>30){
    		renderResult(9);
    	}else if(StrKit.isBlank(context)){
    		renderResult(11);
    	}else if(context.length()>200){
    		renderResult(12);
    	}else if(new Join()
    			.set("name", name)
    			.set("msisdn", msisdn)
    			.set("qq", qq)
    			.set("mail", mail)
    			.set("context", context).save()){
    		renderSuccess();
    	}else{
    		renderFailed();
    	}
    }
}
