<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">服务商家</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-4 col-lg-8">
				<select name="sState" class="selectpicker pull-left p-l-5 " data-width="10%">
					<option value="">全部</option>
					<option value="1">上架</option>
					<option value="2">下架</option>
				</select>
				<input type="text"  name="sName" value="" class="form-control pull-left m-l-5 width200" placeholder="商家名称" maxlength="100" />
				<button type="button" class="btn btn-success pull-left m-l-5" onclick="doSearch()">查询</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div><p></p></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
						<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                	<th width="5%"></th>
								<th width="20%">商家名称</th>
								<th width="10%">电话</th>
								<th width="20%">地址</th>
								<th width="5%">状态</th>
								<th width="15%">创建时间</th>
								<th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="{{item.id}}" data-id="{{item.id}}">
		<td><a href="javascript:void(0)" class="thumbnail thumbnail-none-margin width50"><img src="${FTP_PATH}{{item.thumb}}"></a></td>
		<td>{{item.name}}</td>
		<td>{{item.tel}}</td>
		<td>{{item.address}}</td>
		<td>{{item.state | flagTransform:1,'上架',2,'下架'}}</td>
		<td>{{item.createDate | dateFormat:'yyyy-MM-dd hh:mm'}}</td>
		<td>
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toChoiceCategory('{{item.id}}')" title="分类选择"><span class="fa fa-paperclip fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-info btn-simple" onclick="toDetailEdit('{{item.id}}')" title="图文编辑"><span class="fa fa-file-text-o fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-success btn-simple" onclick="toUploadImages('{{item.id}}')" title="商家图片"><span class="fa fa-file-image-o fa-lg"></span></button> 			
			<button type="button" class="btn btn-sm btn-success btn-simple" onclick="toItemIndex('{{item.id}}')" title="服务规格"><span class="fa fa fa-align-justify fa-lg"></span></button> 			
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="doDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button> 
		</td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(doSearch);
function doSearch(){
	var sName = $("input[name='sName']").val(), sState = $("select[name='sState']").val();
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/service/shop/page",{name:sName,state:sState,"pageSize":20},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/service/shop/toAdd").open();
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/service/shop/toEdit/"+id).open();
}
var choiceCategoryDialog;
function toChoiceCategory(id){
	choiceCategoryDialog = Kit.dialog("选择分类","${BASE_PATH}/service/shop/toChoiceCategoryDialog/"+id).open();
}
var detailEditDialog;
function toDetailEdit(id){
	detailEditDialog = Kit.dialog("图文编辑","${BASE_PATH}/service/shop/toDetailEdit/"+id,{size:'size-wide'}).open();
}
var imagesDialog;
function toUploadImages(id){
	imagesDialog = Kit.dialog("商家图片","${BASE_PATH}/service/shop/imageIndex/"+id,{closable:true}).open();
}
var itemDialog;
function toItemIndex(id){
	itemDialog = Kit.dialog("服务项","${BASE_PATH}/service/shop/itemIndex/"+id,{closable:true}).open();
}
function doDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/service/shop/doDelete/"+id,function(result){
			dataPaginator.loadPage(1);
		});
	});
}
</script>
</body>
</html>