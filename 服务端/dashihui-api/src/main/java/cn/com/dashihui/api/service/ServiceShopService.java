package cn.com.dashihui.api.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.common.ServiceOrderCode;
import cn.com.dashihui.api.dao.ServiceCategory;
import cn.com.dashihui.api.dao.ServiceShop;
import cn.com.dashihui.api.dao.ServiceShopItem;

public class ServiceShopService {
	
	/**
	 * 根据店铺ID查询出分类
	 */
	public List<ServiceCategory> getCategoryByStoreid(int storeid){
		String sql = "SELECT DISTINCT SA.id,SA.code,SA.name,SA.icon FROM t_bus_service_category SA"
				+ " WHERE SA.storeid=? AND SA.isShow=1 ORDER BY SA.orderNo";
		return ServiceCategory.me().find(sql, storeid);
	}
	
	/**
	 * 根据店铺ID和分类CODE分页获得商家信息
	 */
	public Page<Record> getPageShop(int pageNum,int pageSize,int storeid,String categoryCode){
		String sqlSelect = "SELECT SS.id,SS.name,SS.thumb,SS.comment,"
				+ "(SELECT COUNT(*) FROM t_bus_service_order_list OL INNER JOIN t_bus_service_order O ON OL.orderNum=O.orderNum AND orderState="+ServiceOrderCode.OrderState.FINISH+" WHERE OL.shopid=SS.id) total";
		StringBuffer sqlExceptSelectBuffer = new StringBuffer();
		List<Object> params = new ArrayList<Object>();
		sqlExceptSelectBuffer.append(" FROM t_bus_service_shop SS");
		if(!StrKit.isBlank(categoryCode)){
			sqlExceptSelectBuffer.append(" INNER JOIN t_bus_service_category_rel SCR ON SCR.shopid=SS.id AND SCR.categorycode=?");
			params.add(categoryCode);
		}
		sqlExceptSelectBuffer.append(" WHERE SS.storeid=? AND SS.state=1");
		params.add(storeid);
		sqlExceptSelectBuffer.append(" ORDER BY SS.createDate DESC");
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlExceptSelectBuffer.toString(), params.toArray());
	}
	
	/**
	 * 获得商家轮播图片
	 */
	public List<Record> getListShopImgByShopid(int shopid){
		String sql = "SELECT thumb from t_bus_service_shop_images where shopid=? ORDER BY orderNo";
		return Db.find(sql,shopid);
	}
	
	/**
	 * 此处SQL中的查询条件storeid被暂时去掉，下一版时要再加上，2016/02/29修改
	 * 根据店铺ID和商家ID 获得商铺描述
	 */
	public ServiceShop getShopById(int id,int storeid){
		String sql = "SELECT SS.id,SS.name,SS.thumb,SS.comment,"
				+ "(CASE WHEN SS.describe IS NULL OR length(SS.describe)<10 THEN 0 ELSE 1 END) hasDescribe,"
				+ "(SELECT COUNT(*) FROM t_bus_service_order_list OL INNER JOIN t_bus_service_order O ON OL.orderNum=O.orderNum AND orderState=? WHERE OL.shopid=?) total"
				+ " FROM t_bus_service_shop SS"
				+ " WHERE SS.ID=? AND SS.state=1";
		return ServiceShop.me().findFirst(sql,ServiceOrderCode.OrderState.FINISH,id,id);
	}
	
	/**
	 * 根据店铺ID和商家ID 获得商铺详情描述
	 */
	public ServiceShop getShopDescribe(int id,int storeid){
		String sql = "SELECT `describe` FROM t_bus_service_shop where id =? AND storeid=?";
		return ServiceShop.me().findFirst(sql,id,storeid);
	}
	
	/**
	 * 根据店铺ID获得服务规格
	 */
	public List<ServiceShopItem> getListShopItemByShopid(int shopid){
		String sql = "SELECT id,title,marketPrice,sellPrice FROM t_bus_service_shop_item"+
					 " WHERE shopid =?"+
					 " ORDER BY sellPrice ASC";
		return ServiceShopItem.me().find(sql,shopid);
	}
}
