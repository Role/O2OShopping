//
//  Strore_productTextAndImageInfoWebController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Strore_productTextAndImageInfoWebController.h"
//untils
#import "UIColor+Hex.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
#import "AppDelegate.h"
//vendor
#import "RDVTabBarController.h"
#import "MTA.h"
@interface Strore_productTextAndImageInfoWebController ()
@property (nonatomic, strong)UIWebView *webView;
@end

@implementation Strore_productTextAndImageInfoWebController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efeeee"];
    [self addBackNavItem];
    [self setupSubView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    [self loadTextAndImageData];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:NO];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"图文详情"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"图文详情"];
}
#pragma mark -- setup view
- (void)setupSubView {
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - 64)];
    _webView.backgroundColor = [UIColor clearColor];
    _webView.scalesPageToFit = NO;
    _webView.delegate = (id<UIWebViewDelegate>)self;
    [self.view addSubview:_webView];
}
#pragma mark request Data
- (void)loadTextAndImageData {
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    XiaoQuAndStoreInfoModel *model = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    NSString *adapterStr = @"";
    if (self.imageAndTextDetailType == kserviceImageAndTextDetail) {
        [parameter setObject:self.goodsId.length == 0 ? @"" : self.goodsId forKey:@"SHOPID"];
        adapterStr = @"service/describe";
    } else if (self.imageAndTextDetailType == kproductImageAndTextDetail) {
        adapterStr = @"goods/detailDescribe";
        [parameter setObject:self.goodsId.length == 0 ? @"" : self.goodsId forKey:@"GOODSID"];
    }
    
    [parameter setObject:model.storeID forKey:@"STOREID"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:adapterStr success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                if (weakSelf.imageAndTextDetailType == kserviceImageAndTextDetail) {
                    [_webView loadHTMLString:[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"DESCRIBE"]]  baseURL:nil];
                } else if (weakSelf.imageAndTextDetailType == kproductImageAndTextDetail) {
                    [_webView loadHTMLString:[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"CONTEXT"]]  baseURL:nil];
                }
                
                
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
        
    } failure:^(bool isFailure) {
        
        
    }];
}

#pragma mark --  UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:webView];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[ManagerGlobeUntil sharedManager] hideHUDFromeView:webView];
    NSString *scriptStr = [NSString stringWithFormat:@"var script = document.createElement('script');"
                           "script.type = 'text/javascript';"
                           "script.text = \"function ResizeImages() { "
                           "var myimg,oldwidth;"
                           "var maxwidth=%f;" //缩放系数
                           "for(i=0;i <document.images.length;i++){"
                           "myimg = document.images[i];"
                           "if(myimg.width > maxwidth){"
                           "oldwidth = myimg.width;"
                           "myimg.width = maxwidth;"
                           "myimg.height = myimg.height * (maxwidth/oldwidth);"
                           "}"
                           "}"
                           "}\";"
                           "document.getElementsByTagName('head')[0].appendChild(script);",self.view.bounds.size.width - 2*8];
    [webView stringByEvaluatingJavaScriptFromString:scriptStr];
   
    
    [webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error {
    [[ManagerGlobeUntil sharedManager] hideHUDFromeView:webView];
}

- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- setter method
- (void)setNavgationTitle:(NSString *)navgationTitle {
    self.title = navgationTitle;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
