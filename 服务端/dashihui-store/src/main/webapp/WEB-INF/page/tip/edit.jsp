<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		messages:{
			content: {required: "请输入通知内容"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						editDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editForm" action="${BASE_PATH}/tip/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">通知</label>
	    <div class="col-lg-9">
        	<textarea name="content" class="form-control" placeholder="请输入通知内容" required maxlength="50">${object.content}</textarea>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">显示</label>
	    <div class="col-lg-9">
	        <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="isShow" value="1" <c:if test="${object.isShow==1}">checked</c:if>> 是</li>
	    		<li><input type="radio" class="iCheck" name="isShow" value="0" <c:if test="${object.isShow==0}">checked</c:if>> 否</li>
	    	</ul>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="tipid" value="${object.id}">
</form>