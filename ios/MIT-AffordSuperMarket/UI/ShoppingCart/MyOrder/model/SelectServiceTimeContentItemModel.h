//
//  SelectServiceTimeContentItemModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SelectServiceTimeContentItemModel
 Created_Date： 20151224
 Created_People：gt
 Function_description： 选择服务时间内容Item model
 ***************************************/

#import <Foundation/Foundation.h>

@interface SelectServiceTimeContentItemModel : NSObject
@property(nonatomic, strong)NSString *time;//时间
@property(nonatomic, strong)NSString *date;//日期
@property(nonatomic)BOOL enable;//是否可点击
@end
