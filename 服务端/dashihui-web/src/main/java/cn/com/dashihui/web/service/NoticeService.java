package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Page;

import cn.com.dashihui.web.dao.Notice;

public class NoticeService {
	
	public Page<Notice> findByPage(int type,int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_web_notice A WHERE type=?");
		sqlExcept.append(" ORDER BY A.createDate DESC");
		return Notice.me().paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(), type);
	}
	
	public Notice findById(int id){
		String sql = "SELECT A.*,B.previd,B.prevTitle,C.nextid,C.nextTitle ";
		sql += " FROM (SELECT * FROM t_web_notice WHERE id = " + id + ") A ";
		sql += " LEFT JOIN ";
		sql += " (SELECT " + id + " currid,id previd,title prevTitle FROM t_web_notice WHERE id=(SELECT max(id) FROM t_web_notice WHERE id < " + id + " ORDER BY id)) B ON A.id=B.currid ";
		sql += " LEFT JOIN ";
		sql += "(SELECT " + id + " currid,id nextid,title nextTitle FROM t_web_notice WHERE id=(SELECT min(id) FROM t_web_notice WHERE id > " + id + " ORDER BY id)) C ON A.id=c.currid";
		return Notice.me().findFirst(sql);
	}
	
}
