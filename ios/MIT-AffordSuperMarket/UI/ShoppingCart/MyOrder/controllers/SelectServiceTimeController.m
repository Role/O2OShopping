//
//  SelectServiceTimeController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SelectServiceTimeController.h"
#define kleftSpace 15
//untils
#import "UIColor+Hex.h"
#import "AppDelegate.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
//vendor
#import "MTA.h"
#import "RDVTabBarController.h"
//view
#import "SelectServiceTimeHeaderView.h"
#import "SelectServiceTimeContentView.h"
//controller
#import "SureOrderController.h"
@interface SelectServiceTimeController ()<SelectServiceTimeHeaderViewDelegate,SelectServiceTimeContentViewDelegate>
{
    SelectServiceTimeContentView *_contentView;
    NSString *_selectedDate;
}
@end

@implementation SelectServiceTimeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"请选择服务时间";
    [self addBackNavItem];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
          [self loadServiceTimeData];
    }
  
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"选择服务时间页面"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"选择服务时间页面"];
}
#pragma mark -- request Data
- (void)loadServiceTimeData {
    //请求参数说明
    //SIGNATURE			设备识别码
    //TOKEN			用户签名
    //ORDERNUM			订单号
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.token] forKey:@"TOKEN"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"service/order/times" success:^(AFHTTPRequestOperation *operation, id responseObject) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [weakSelf setupSubViewWithData:[dataDic objectForKey:@"OBJECT"]];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
        
    } failure:^(bool isFailure) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        
    }];

    
}
#pragma mark -- 初始化视图
- (void)setupSubViewWithData:(NSArray*)dataArr {
    NSMutableArray *titles = [[NSMutableArray alloc]init];
    for (NSDictionary *dataDic in dataArr) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"TITLE"]] forKey:@"title"];
        [dic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"DATE"]] forKey:@"subtitle"];
        [titles addObject:dic];
    }
    SelectServiceTimeHeaderView *headerView = [[SelectServiceTimeHeaderView alloc]initWithFrame:CGRectMake(0
                                                                                                           , 0
                                                                                                           , CGRectGetWidth(self.view.bounds)
                                                                                                           , 66)
                                                                                         titles:[titles copy]];
    headerView.delegate = self;
    [self.view addSubview:headerView];
    
    _contentView = [[SelectServiceTimeContentView alloc]initWithFrame:CGRectMake(0, 80, CGRectGetWidth(self.view.bounds) , 221) withDataArr:dataArr columnNums:4];
    //contentView.backgroundColor = [UIColor redColor];
    _contentView.delegate = self;
    [self.view addSubview:_contentView];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(kleftSpace
                               , CGRectGetHeight(self.view.bounds) - 35 - 44
                               , CGRectGetWidth(self.view.bounds) - 2*kleftSpace
                               , 44);
    UIImage *selectImage = [UIImage imageNamed:@"login_Btn"];
    [sureBtn setTitle:@"确认选择" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
    [sureBtn setBackgroundImage:selectImage forState:UIControlStateNormal];
    [sureBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateDisabled];
    [sureBtn addTarget:self action:@selector(sureSelectServiceTime:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sureBtn];
    
    
}
#pragma mark -- SelectServiceTimeHeaderViewDelegate
- (void)selectedItemWithIndex:(NSInteger)index {
    [_contentView reloadViewWithDateSelectedIndex:index];
}
#pragma mark -- SelectServiceTimeContentViewDelegate
- (void)selectedListTtemAtIndex:(NSInteger)index title:(NSString*)title {
    _selectedDate = title;
}
#pragma mark -- button Action
- (void)sureSelectServiceTime:(id)sender {
    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[SureOrderController class]]) {
            ((SureOrderController*)VC).serviceTime = [NSString stringISNull:_selectedDate];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
