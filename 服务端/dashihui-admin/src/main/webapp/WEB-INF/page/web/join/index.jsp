<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">加盟留言</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
				</div>
				<hr/>
				<div class="container-fluid"><div id="dataList" class="row"></div></div>
				<div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<div id="item{{item.id}}" class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-lg-2"><label>姓名：</label>{{item.name}}</div>
				<div class="col-lg-2"><label>手机号：</label>{{item.msisdn}}</div>
				<div class="col-lg-2"><label>QQ：</label>{{item.qq}}</div>
				<div class="col-lg-2"><label>邮箱：</label>{{item.mail}}</div>
				<div class="col-lg-2"><label>时间：</label>{{item.createDate | dateFormat:'yyyy-MM-dd hh:mm'}}</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body" style="min-height:50px;">{{item.context}}</div>
		<div class="panel-footer">
			<div class="pull-left"><label>地址：</label>{{item.address}}</div>
			<button type="button" class="btn btn-sm btn-simple btn-danger p-none pull-right" onclick="toDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button>
			<div class="clearfix"></div>
		</div>
	</div>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(function(){
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/web/join/page",{"pageSize":10},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
});
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/web/join/doDelete/"+id,function(result){
			$("#item"+id).remove();
		});
	});
}
</script>
</body>
</html>