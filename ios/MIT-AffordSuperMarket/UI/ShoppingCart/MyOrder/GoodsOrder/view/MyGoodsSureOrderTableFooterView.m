//
//  MyGoodsSureOrderTableFooterView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/11.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsSureOrderTableFooterView.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#define ktopSapce 12
#define kleftSpace 15
#define khSpace 10
#define ksepLineHight 0.5
#define kitemHight HightScalar(47)
@interface MyGoodsSureOrderTableFooterView ()
@property(nonatomic, strong)UIButton *markInfoBtn;
@property(nonatomic, strong)UILabel *goodsPricelabel;
@property(nonatomic, strong)UILabel *peiSongPriceLabel;
@end
@implementation MyGoodsSureOrderTableFooterView
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
        [self setupSubView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    UIView *firstSepLineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, ktopSapce)];
    firstSepLineView.backgroundColor = [UIColor clearColor];
    [firstSepLineView.layer setBorderWidth:ksepLineHight];
    [firstSepLineView.layer setBorderColor:[UIColor colorWithHexString:@"#c6c6c6"].CGColor];
    [self addSubview:firstSepLineView];
    //配送备注
    UIView *markView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLineView.frame)
                                                              , CGRectGetMaxY(firstSepLineView.frame)
                                                              , CGRectGetWidth(firstSepLineView.bounds)
                                                               , kitemHight - ksepLineHight)];
    markView.backgroundColor = [UIColor whiteColor];
    [self addSubview:markView];
    NSString *peisongMarkTitle = @"订单备注";
    CGSize peisongMarkTitleSize = [peisongMarkTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                                         maxSize:CGSizeMake(120, 44)];
    UILabel *peisongMarkTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                              ,0
                                                                              , peisongMarkTitleSize.width
                                                                              , CGRectGetHeight(markView.bounds))];
    peisongMarkTitleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    peisongMarkTitleLabel.text = peisongMarkTitle;
    peisongMarkTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [markView addSubview:peisongMarkTitleLabel];
    
    _markInfoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    NSString *markInfoTitle = @"(选填)可输入特殊要求";
    UIImage *markInfoImage = [UIImage imageNamed:@"right_arrow"];
    _markInfoBtn.frame = CGRectMake(CGRectGetMaxX(peisongMarkTitleLabel.frame) + 12
                                    , CGRectGetMinY(peisongMarkTitleLabel.frame)
                                    , CGRectGetWidth(self.bounds) - 2*kleftSpace  - peisongMarkTitleSize.width - 12
                                    , CGRectGetHeight(peisongMarkTitleLabel.bounds));
    [_markInfoBtn setTitle:markInfoTitle forState:UIControlStateNormal];
    [_markInfoBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    [_markInfoBtn setImage:markInfoImage forState:UIControlStateNormal];
    _markInfoBtn.titleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    _markInfoBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    CGSize titleSize = [markInfoTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                           maxSize:CGSizeMake(VIEW_WIDTH - 2*kleftSpace - khSpace - peisongMarkTitleSize.width
                                                                                                   , kitemHight)];
    _markInfoBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -titleSize.width);
    _markInfoBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0,  markInfoImage.size.width + 10);
    [_markInfoBtn addTarget:self action:@selector(addMarkInfoEvent:) forControlEvents:UIControlEventTouchUpInside];
    [markView addSubview:_markInfoBtn];
    
//    UIView *secondSepLineView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(markView.frame)
//                                                                        , CGRectGetMaxY(markView.frame)
//                                                                        , CGRectGetWidth(firstSepLineView.bounds)
//                                                                        , CGRectGetHeight(firstSepLineView.bounds))];
//    secondSepLineView.backgroundColor = [UIColor clearColor];
//    [secondSepLineView.layer setBorderWidth:ksepLineHight];
//    [secondSepLineView.layer setBorderColor:[UIColor colorWithHexString:@"#c6c6c6"].CGColor];
//    [self addSubview:secondSepLineView];
    
//    //价格视图
//    UIView *priceView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(secondSepLineView.frame)
//                                                               , CGRectGetMaxY(secondSepLineView.frame)
//                                                               , CGRectGetWidth(secondSepLineView.bounds)
//                                                                , 2*kitemHight + 2*ksepLineHight)];
//    priceView.backgroundColor = [UIColor whiteColor];
//    [self addSubview:priceView];
//    //商品金额
//    UILabel *priceTitleLabel = [self setLabelWithTitle:@"商品金额" fontSize:FontSize(15) textColor:@"#555555"];
//    priceTitleLabel.frame = CGRectMake(kleftSpace, 0, 120, kitemHight);
//    [priceView addSubview:priceTitleLabel];
//    
//    _goodsPricelabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#555555"];
//    _goodsPricelabel.textAlignment = NSTextAlignmentRight;
//    _goodsPricelabel.frame = CGRectMake(VIEW_WIDTH - kleftSpace - 160
//                                        , CGRectGetMinY(priceTitleLabel.frame)
//                                        , 160
//                                        , CGRectGetHeight(priceTitleLabel.bounds));
//    [priceView addSubview:_goodsPricelabel];
//    
//    UIImage *sepLineImage = [UIImage imageNamed:@"gray_line"];
//    UIImageView *firstSepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
//    firstSepLineImageView.frame = CGRectMake(CGRectGetMinX(priceTitleLabel.frame)
//                                             , CGRectGetMaxY(priceTitleLabel.frame)
//                                             , VIEW_WIDTH - kleftSpace
//                                             , ksepLineHight);
//    [priceView addSubview:firstSepLineImageView];
//    
//    //运费
//    UILabel *peiSongTitleLabel = [self setLabelWithTitle:@"运费" fontSize:FontSize(15) textColor:@"#555555"];
//    peiSongTitleLabel.frame = CGRectMake(CGRectGetMinX(priceTitleLabel.frame)
//                                         , CGRectGetMaxY(firstSepLineImageView.frame)
//                                         , CGRectGetWidth(priceTitleLabel.bounds)
//                                         , CGRectGetHeight(priceTitleLabel.bounds));
//    
//    [priceView addSubview:peiSongTitleLabel];
//    
//    _peiSongPriceLabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#555555"];
//    _peiSongPriceLabel.textAlignment = NSTextAlignmentRight;
//    _peiSongPriceLabel.frame = CGRectMake(CGRectGetMinX(_goodsPricelabel.frame)
//                                        , CGRectGetMinY(peisongMarkTitleLabel.frame)
//                                        , CGRectGetWidth(_goodsPricelabel.bounds)
//                                        , CGRectGetHeight(_goodsPricelabel.bounds));
//    [priceView addSubview:_peiSongPriceLabel];
//    
//    UIImageView *secondSepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
//    secondSepLineImageView.frame = CGRectMake(0
//                                             , CGRectGetMaxY(peiSongTitleLabel.frame)
//                                             , VIEW_WIDTH
//                                             , ksepLineHight);
//    [priceView addSubview:secondSepLineImageView];
    
    UIImage *sepLineImage = [UIImage imageNamed:@"gray_line"];
    UIImageView *firstSepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
    firstSepLineImageView.frame = CGRectMake(0
                                             , CGRectGetMaxY(peisongMarkTitleLabel.frame)
                                             , VIEW_WIDTH
                                             , ksepLineHight);
    [markView addSubview:firstSepLineImageView];
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0
                                                                , CGRectGetMaxY(markView.frame)
                                                                 , VIEW_WIDTH, 49)];
    [self addSubview:bottomView];
}
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
#pragma mark -- button action
- (void)addMarkInfoEvent:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(addOrderMarkInfo)]) {
        [_delegate addOrderMarkInfo];
    }
}
#pragma mark -- setter Method
- (void)setMarkInfo:(NSString *)markInfo {
    UIImage *markInfoImage = [UIImage imageNamed:@"right_arrow"];
    NSString *title = @"";
    _markInfoBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    if (markInfo.length == 0) {
        title = @"可输入特殊要求(选填)";
    } else {
        title = markInfo;
        
    }
    CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(CGRectGetWidth(self.markInfoBtn.bounds), 50)];
    CGFloat maxTitleWidth = CGRectGetWidth(self.markInfoBtn.bounds) - 2*markInfoImage.size.width;
    CGFloat leftSpace = 0;
    if (titleSize.width < maxTitleWidth ) {
        leftSpace = maxTitleWidth - titleSize.width - 10;
    }
    _markInfoBtn.imageEdgeInsets = UIEdgeInsetsMake(0, (CGRectGetWidth(self.markInfoBtn.bounds) - markInfoImage.size.width), 0, 0);
    _markInfoBtn.titleEdgeInsets = UIEdgeInsetsMake(0,leftSpace, 0, markInfoImage.size.width);
    [self.markInfoBtn setTitle:title forState:UIControlStateNormal];
}

- (void)setGoodsPrice:(NSString *)goodsPrice {
    NSString *priceStr = [NSString stringWithFormat:@"￥%@",goodsPrice];
    self.goodsPricelabel.text = priceStr;
}

- (void)setPeiSongPrice:(NSString *)peiSongPrice {
    NSString *peiSongPriceStr = [NSString stringWithFormat:@"￥%@",peiSongPrice];
    self.peiSongPriceLabel.text = peiSongPriceStr;
}
@end
