<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		messages:{
			name: {required: "请输入权限名称"},
			code: {required: "请输入权限代码"},
			url: {required: "请输入Action"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 5:
						Kit.alert("权限代码已经存在");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						onResourceEdited(data.object);
						editDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editForm" action="${BASE_PATH}/auth/resource/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">权限名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" value="${object.name}" class="form-control" placeholder="请输入权限名称" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">权限代码</label>
	    <div class="col-lg-9">
        	<input type="text" name="code" value="${object.code}" class="form-control" placeholder="请输入权限代码" required maxlength="100">
	    </div>
	</div>
	<c:if test="${parent!=null}">
	<div class="form-group">
	    <label class="col-lg-2 control-label">父权限</label>
	    <div class="col-lg-9">
	    	<input type="text" class="form-control" value="${parent.name}" disabled="disabled"/>
	    </div>
		<input type="hidden" id="parentid" name="parentid" value="${parent.id}"/>
	</div>
	</c:if>
	<c:if test="${parent==null}"><input type="hidden" name="parentid" value="0"/></c:if>
	<div class="form-group">
	    <label class="col-lg-2 control-label">类型</label>
	    <div class="col-lg-9">
	    	<c:choose>
	    	<c:when test="${object.type==1}"><input type="text" class="form-control" value="一级菜单" disabled="disabled"/></c:when>
	    	<c:when test="${object.type==2}"><input type="text" class="form-control" value="二级菜单" disabled="disabled"/></c:when>
	    	<c:when test="${object.type==3}"><input type="text" class="form-control" value="三级操作" disabled="disabled"/></c:when>
	    	</c:choose>
	    </div>
	</div>
	<c:if test="${object.type!=1}">
	<div class="form-group">
	    <label class="col-lg-2 control-label">Action</label>
	    <div class="col-lg-9">
        	<input type="text" name="url" value="${object.url}" class="form-control" placeholder="请输入Action" maxlength="200" required>
	    </div>
	</div>
   	</c:if>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="resourceid" value="${object.id}"/>
</form>