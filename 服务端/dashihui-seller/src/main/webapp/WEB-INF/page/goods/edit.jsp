<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.seller.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<title>编辑</title>
	<!-- amazeui -->
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
	<!-- app -->
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/goods.css" />
</head>
<body>
	<header class="am-header am-header-default app-header" data-am-sticky>
		<h1 class="am-header-title">编辑</h1>
		<div class="am-header-left am-header-nav">
			<a href="javascript:history.go(-1);"><i class="am-icon-angle-left am-icon-lg"></i></a>
		</div>
	</header>
	<div style="padding:10px;background-color:white;">
		<form class="am-form">
			<input type="hidden" id="goodsid" name="goodsid" value="${goods.id }"/>
			<div class="am-form-group">
				<label>标题：</label>
				<textarea rows="2" cols="3" id="name" name="name" maxlength="100">${goods.name }</textarea>
			</div>
			<div class="am-form-group">
				<label>市场价：</label>
				<input type="text" id="marketPrice" name="marketPrice" value="${goods.marketPrice }" maxlength="15">
			</div>
			<div class="am-form-group">
				<label>销售价：</label>
				<input type="text" id="sellPrice" name="sellPrice" class="form-control" value="${goods.sellPrice }" maxlength="15">
			</div>
			<div class="am-form-group">
				<label>规格：</label>
				<input type="text" id="spec" name="spec" value="${goods.spec }" maxlength="50">
			</div>
			<div class="am-form-group">
				<label>标签：</label>
				<div>
		    		<c:forEach items="${tagList}" var="tag" varStatus="status">
	    			<div class="am-fl">
    					<input type="checkbox" id="tag" name="tag" value="${tag.id}" class="am-fl" <c:if test="${tag.checked==1}">checked</c:if>>
    					<span class="am-fl app-m-l-5 <c:if test="${status.index!=fn:length(tagList)-1}">app-m-r-10</c:if>">${tag.tagName}</span>
	    				<div class="am-cf"></div>
   					</div>
		    		</c:forEach>
		    		<div class="am-cf"></div>
	    		</div>
			</div>
			<div class="am-form-group">
				<label>商品类型：</label>
				<div>
		    		<div class="am-fl">
						<input type="radio" name="type" value="1" class="am-fl" onclick="doCheckType(this);" <c:if test="${goods.type==1}">checked</c:if>>
						<span class="am-fl app-m-l-5 app-m-r-10">普通</span>
			    		<div class="am-cf"></div>
	    			</div>
					<div class="am-fl">
						<input type="radio" name="type" value="2" class="am-fl" onclick="doCheckType(this);" <c:if test="${goods.type==2}">checked</c:if>>
						<span class="am-fl app-m-l-5 app-m-r-10">推荐</span>
			    		<div class="am-cf"></div>
	    			</div>
					<div class="am-fl">
						<input type="radio" name="type" value="3" class="am-fl" onclick="doCheckType(this);" <c:if test="${goods.type==3}">checked</c:if>>
						<span class="am-fl app-m-l-5 app-m-r-10">限量</span>
			    		<div class="am-cf"></div>
	    			</div>
					<div class="am-fl">
						<input type="radio" name="type" value="4" class="am-fl" onclick="doCheckType(this);" <c:if test="${goods.type==4}">checked</c:if>>
						<span class="am-fl app-m-l-5 app-m-r-10">一元购</span>
			    		<div class="am-cf"></div>
	    			</div>
		    		<div class="am-cf"></div>
	    		</div>
			</div>
			<div class="am-form-group" style="display:none;" id="urvPart">
				<label>限量购：</label>
				<input type="text" class="form-control" id="urv" name="urv" placeholder="请输入限购量" value="${goods.urv}" maxlength="3">
			</div>
			<div class="am-form-group">
				<label>描述（短）：</label>
				<textarea rows="7" cols="3" name="shortInfo" maxlength="120">${goods.shortInfo }</textarea>
			</div>
			
			<div class="app-list-item app-m-t-10">
				<button type="button" class="am-btn am-btn-success" style="width: 100%;" onclick="doSubmit();">保存</button>
			</div>
		</form>
	</div>

	<!-- include -->
	<%@include file="../include.jsp"%>
	<!-- lib -->
	<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
	<script src='${BASE_PATH}/static/lib/layer/layer.js' type='text/javascript'></script>
	<!-- app -->
	<script src='${BASE_PATH}/static/app/js/kit.js' type='text/javascript'></script>
	<script type="text/javascript">
		var sellPrice;
		$(function() {
			sellPrice = $("#sellPrice").val();
			var type = $('input[name="type"]:checked').val();
			if(type == 3 || type == 4) {
				$("#urvPart").show();
			}
		});
		function doSubmit() {
			var goodsid = $("#goodsid").val();
			var name = $("#name").val();
			var marketPrice = $("#marketPrice").val();
			var sellPrice = $("#sellPrice").val();
			var spec = $("#spec").val();
			var tags =[]; 
			$('input[name="tag"]:checked').each(function(){ 
				tags.push($(this).val()); 
			});
			var type = $('input[name="type"]:checked').val();
			var urv = $("#urv").val();
			if(name == '' || name == null) {
				Kit.ui.alert('请输入商品标题!');return;
			}
			if(marketPrice == '' || marketPrice == null) {
				Kit.ui.alert('请输入市场价!');return;
			}
			if(sellPrice == '' || sellPrice == null) {
				Kit.ui.alert('请输入销售价!');return;
			}
			if(spec == '' || spec == null) {
				Kit.ui.alert('请输入规格!');return;
			}
			if(type == 3 || type == 4) {
				if(urv == '' || urv == null) {
					Kit.ui.alert("请输入限量购!");return;
				}
			}
			if(type == '' || type == null) {
				Kit.ui.alert('请选择优惠类型!');return;
			}
			var params = {goodsid:goodsid,name:name,marketPrice:marketPrice,sellPrice:sellPrice,spec:spec,tags:tags,type:type,urv:urv};
			Kit.ajax.post("${BASE_PATH}/goods/doEdit",params,function(result){
				if(result.flag == 0) {
					Kit.ui.alert("修改成功!",function(){
						history.go(-1);
					});
				} else if(result.flag == 1) {
					Kit.ui.toast("请输入商品名称!");
				} else if(result.flag == 3) {
					Kit.ui.toast("请输入规格!");
				} else if(result.flag == 4) {
					Kit.ui.toast("请输入市场价!");
				} else if(result.flag == 5) {
					Kit.ui.toast("请输入销售价!");
				} else if(result.flag == 6) {
					Kit.ui.toast("请输入限量购!");
				} else {
					Kit.ui.toast("修改失败，请重试!");
				}
			});
		}
		function doCheckType(obj) {
			if(obj.value==3){
				$("#sellPrice").val(sellPrice).prop("readonly",false);
				$("#urvPart").show();
				$("#urvPartHr").show();
			}else if(obj.value==4){
				$("#sellPrice").val(1).prop("readonly",true);
				$("#urvPart").show();
				$("#urvPartHr").show();
			}else{
				$("#sellPrice").val(sellPrice);
				$("#sellPrice").prop("readonly",false);
				$("#urvPart").hide();
				$("#urvPartHr").hide();
			}
		}
		
	</script>
</body>
</html>