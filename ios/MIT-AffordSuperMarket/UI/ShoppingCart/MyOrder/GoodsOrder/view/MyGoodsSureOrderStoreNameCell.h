//
//  MyGoodsSureOrderStoreNameCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/13.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsSureOrderStoreNameCell
 Created_Date： 20160413
 Created_People：GT
 Function_description： 商品提交订单table便利店名字cell
 ***************************************/

#import <UIKit/UIKit.h>

@interface MyGoodsSureOrderStoreNameCell : UITableViewCell
- (void)setupViewWithData:(NSDictionary*)dataDic;
@end
