//
//  UserInfo_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： UserInfo_Controller
 Created_Date： 20151110
 Created_People： GT
 Function_description： 个人信息页面
 ***************************************/

#import "BaseViewController.h"

@interface UserInfo_Controller : BaseViewController
@property(nonatomic, strong)UIImage *userImage;
@property(nonatomic, strong)NSString *userName;
@property(nonatomic, strong)NSString *userPhoneNum;
@end
