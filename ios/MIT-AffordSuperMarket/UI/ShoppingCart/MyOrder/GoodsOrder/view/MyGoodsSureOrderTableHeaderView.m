//
//  MyGoodsSureOrderTableHeaderView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/8.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsSureOrderTableHeaderView.h"
#define ktopSpace 12
#define kleftSpace 15
#define kvSpace 13 //有地址子视图对父视图的垂直间隙
#define khSpace 7 //有地址子视图之间的水平间隙
#define kmodifiedAddressBtnWidth HightScalar(44) //修改地址宽度
#define kitemHight HightScalar(47) //每个cell高度
#define ksepLineHight 0.5f //分割线高度
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "NSString+TextSize.h"
#import "FL_Button.h"
//view
#import "CheckBoxView.h"
@interface MyGoodsSureOrderTableHeaderView ()<UIGestureRecognizerDelegate>
//无地址视图
@property(nonatomic, strong)UIView *noAddressView;
@property(nonatomic, strong)UIButton *selectXiaoQuBtn;//选择小区
@property(nonatomic, strong)UILabel *xiaoQuAddressLabel;//显示小区label
//有地址视图
@property(nonatomic, strong)UIView *hadAddressView;
@property(nonatomic, strong)UILabel *hadAddressNameLabel;
@property(nonatomic, strong)UILabel *hadAddressAddressLabel;
//公共视图
@property(nonatomic, strong)FL_Button *payTypeBtn;//选择支付类型
@property(nonatomic, strong)FL_Button *peiSongTypeBtn;//选择配送方式类型
@property(nonatomic,strong)UILabel *peiSongIntroduceLabel;//营业说明/配送说明
@property(nonatomic, strong)UIView *commonView;//公共视图
@end
@implementation MyGoodsSureOrderTableHeaderView
- (instancetype)initWithFrame:(CGRect)frame hadOrdersAddressStatus:(BOOL)isHadOrdersAddress selectedGoodsType:(SelectedShoppingCartGoodsType)selectedGoodsType {
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefaultDataWithGoogsType:selectedGoodsType];
        [self setupSubViewWithHadOrdersAddressStatus:isHadOrdersAddress selectedGoodsType:selectedGoodsType];
        //隐藏键盘手势
        UITapGestureRecognizer *tabGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
        tabGesture.delegate = self;
        [self addGestureRecognizer:tabGesture];
    }
    return self;
}
//设置默认值
- (void)setDefaultDataWithGoogsType:(SelectedShoppingCartGoodsType)goodsType {
    if (goodsType == konlySelectedSelfGoods) {
        self.storeGoodsPayType = @"";
        self.storeGoodsSendProductType = @"";
        self.selfGoodsPayType = @"1";
        self.selfGoodsSendProductType = @"1";
    } else if (goodsType == konlySelectedStoreGoods) {
        self.storeGoodsPayType = @"在线支付";
        self.storeGoodsSendProductType = @"门店配送";
        self.selfGoodsPayType = @"";
        self.selfGoodsSendProductType = @"";
    } else if (goodsType == kselectedStoreGoodsAndSelfGoods) {
        self.storeGoodsPayType = @"在线支付";
        self.storeGoodsSendProductType = @"门店配送";
        self.selfGoodsPayType = @"1";
        self.selfGoodsSendProductType = @"1";
    }
    self.sexType = @"男";
}

#pragma mark -- 初始化视图
- (void)setupSubViewWithHadOrdersAddressStatus:(BOOL)isHadOrdersAddress selectedGoodsType:(SelectedShoppingCartGoodsType)selectedGoodsType{
    if (isHadOrdersAddress) {//有地址
        [self setupHadAddressView];
    } else {//无地址
        [self setupNoAddressView];
    }
    if (selectedGoodsType != konlySelectedSelfGoods) {//当商品包含便利店商品的时候才显示公共视图
        [self setupCommonViewWithHadOrdersAddressStatus:isHadOrdersAddress];
    } else {
        [self setAddressViewSepLineWhenOnlySelfGoodsWithIsHadAddress:isHadOrdersAddress];
    }
}
//有地址视图
- (void)setupHadAddressView {
    
    
    _hadAddressView = [[UIView alloc]init];
    _hadAddressView.frame = CGRectMake(0
                                       , ktopSpace
                                       , CGRectGetWidth(self.bounds)
                                       , HightScalar(88));
    _hadAddressView.backgroundColor = [UIColor colorWithHexString:@"#faeee6"];
    [self addSubview:_hadAddressView];
    //背景图片
    UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0
                                                                                , 0
                                                                                , CGRectGetWidth(_hadAddressView.bounds)
                                                                                , CGRectGetHeight(_hadAddressView.bounds))];
    backgroundImage.image = [UIImage imageNamed:@"shouHuoAddress_bg"];
    [_hadAddressView addSubview:backgroundImage];
    // name
    _hadAddressNameLabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#555555"];
    _hadAddressNameLabel.frame = CGRectMake(kleftSpace
                                            , HightScalar(14)
                                            , CGRectGetWidth(self.bounds) - kleftSpace - kmodifiedAddressBtnWidth
                                            , (HightScalar(88) - 2*kvSpace)/2);
    _hadAddressNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.hadAddressView addSubview:_hadAddressNameLabel];
    //address
    UIImage *locationImage = [UIImage imageNamed:@"dingweiImage"];
    UIImageView *locationImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_hadAddressNameLabel.frame)
                                                                                  , CGRectGetMaxY(_hadAddressNameLabel.frame) + (CGRectGetHeight(_hadAddressNameLabel.bounds) - locationImage.size.height)/2
                                                                                  , locationImage.size.width
                                                                                  , locationImage.size.height ) ];
    locationImageView.image = locationImage;
    [self.hadAddressView addSubview:locationImageView];
    
    _hadAddressAddressLabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#555555"];
    _hadAddressAddressLabel.frame = CGRectMake(CGRectGetMaxX(locationImageView.frame) + khSpace
                                               , CGRectGetMaxY(_hadAddressNameLabel.frame)
                                               , CGRectGetWidth(_hadAddressNameLabel.bounds) - khSpace
                                               , CGRectGetHeight(_hadAddressNameLabel.bounds));
    [self.hadAddressView addSubview:_hadAddressAddressLabel];
    
    //修改地址button
    UIButton *moditAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    moditAddressBtn.frame = CGRectMake(CGRectGetWidth(_hadAddressView.bounds) - kmodifiedAddressBtnWidth
                                       , (HightScalar(88) - kmodifiedAddressBtnWidth)/2
                                       , kmodifiedAddressBtnWidth
                                       , kmodifiedAddressBtnWidth);
    [moditAddressBtn setImage:[UIImage imageNamed:@"icon_eidt"] forState:UIControlStateNormal];
    [moditAddressBtn addTarget:self action:@selector(modifiedAddressEvent) forControlEvents:UIControlEventTouchUpInside];
    [self.hadAddressView addSubview:moditAddressBtn];
    
}
//无地址视图
- (void)setupNoAddressView {
    //父视图
    _noAddressView = [[UIView alloc]initWithFrame:CGRectMake(0
                                                             , ktopSpace
                                                             , CGRectGetWidth(self.bounds)
                                                             , kitemHight*4 + ksepLineHight*5)];
    _noAddressView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    [_noAddressView.layer setBorderWidth:ksepLineHight];
    [_noAddressView.layer setBorderColor:[UIColor colorWithHexString:@"#c6c6c6"].CGColor];
    [self addSubview:_noAddressView];
    
    UIImage *sepImage = [UIImage imageNamed:@"gray_line"];
    NSString *usreNameTitle = @"联系人：";
    CGSize userNameTitleLabelSize = [usreNameTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                                        maxSize:CGSizeMake(120, kitemHight)];
    UILabel *userNameTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                           , 0
                                                                           , userNameTitleLabelSize.width
                                                                           , kitemHight)];
    userNameTitleLabel.text = usreNameTitle;
    userNameTitleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    userNameTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [_noAddressView addSubview:userNameTitleLabel];
    
    _userNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userNameTitleLabel.frame)
                                                                      , CGRectGetMinY(userNameTitleLabel.frame)
                                                                      , CGRectGetWidth(self.bounds) - 2*kleftSpace - CGRectGetWidth(userNameTitleLabel.bounds)
                                                                      , CGRectGetHeight(userNameTitleLabel.bounds))];
    _userNameTextField.font = [UIFont systemFontOfSize:FontSize(15)];
    _userNameTextField.placeholder = @"请输入联系人姓名";
    _userNameTextField.textColor = [UIColor colorWithHexString:@"#555555"];
    [_noAddressView addSubview:_userNameTextField];
    
    UIImageView *firstSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                             , CGRectGetMaxY(_userNameTextField.frame)
                                                                             , CGRectGetWidth(self.bounds) - kleftSpace
                                                                             , ksepLineHight)];
    firstSepLine.image = sepImage;
    [_noAddressView addSubview:firstSepLine];
    
    //性别
    CheckBoxView *sexCheckBoxView = [[CheckBoxView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLine.frame)
                                                                                  , CGRectGetMaxY(firstSepLine.frame)
                                                                                  , CGRectGetWidth(firstSepLine.frame)
                                                                                  , CGRectGetHeight(_userNameTextField.bounds))
                                                           withiTitles:@[@"男士",@"女士"]
                                                           normalImage:[UIImage imageNamed:@"checkBoxNormalImage"]
                                                          slectedImage:[UIImage imageNamed:@"checkBoxSelectdImage"]];
    sexCheckBoxView.tag = 100;
    sexCheckBoxView.delegate = (id<CheckBoxViewDelegate>)self;
    [_noAddressView addSubview:sexCheckBoxView];
    
    UIImageView *secondSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLine.frame)
                                                                              , CGRectGetMaxY(sexCheckBoxView.frame)
                                                                              , CGRectGetWidth(firstSepLine.bounds)
                                                                              , ksepLineHight)];
    secondSepLine.image = sepImage;
    [_noAddressView addSubview:secondSepLine];
    
    //电话
    NSString *userTelePhoneTitle = @"电话：";
    CGSize userTelePhoneTitleSize = [userTelePhoneTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                                             maxSize:CGSizeMake(120, kitemHight)];
    UILabel *userTelePhoneTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(userNameTitleLabel.frame)
                                                                                , CGRectGetMaxY(secondSepLine.frame)
                                                                                , userTelePhoneTitleSize.width
                                                                                , CGRectGetHeight(userNameTitleLabel.bounds))];
    userTelePhoneTitleLabel.text = userTelePhoneTitle;
    userTelePhoneTitleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    userTelePhoneTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [_noAddressView addSubview:userTelePhoneTitleLabel];
    
    _userTelePhoneTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userNameTitleLabel.frame)
                                                                           , CGRectGetMinY(userTelePhoneTitleLabel.frame)
                                                                           , CGRectGetWidth(self.bounds) - 2*kleftSpace - CGRectGetWidth(userNameTitleLabel.bounds)
                                                                           , CGRectGetHeight(userTelePhoneTitleLabel.bounds))];
    _userTelePhoneTextField.font = [UIFont systemFontOfSize:FontSize(15)];
    // 获取用户电话
    UserInfoModel *model = [[ManagerGlobeUntil sharedManager] getUserInfo];
    _userTelePhoneTextField.text =model.userName;
    _userTelePhoneTextField.placeholder = @"请输入联系人电话";
    _userTelePhoneTextField.textColor = [UIColor colorWithHexString:@"#555555"];
    [_noAddressView addSubview:_userTelePhoneTextField];
    
    UIImageView *thridSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(secondSepLine.frame)
                                                                             , CGRectGetMaxY(_userTelePhoneTextField.frame)
                                                                             , CGRectGetWidth(secondSepLine.bounds)
                                                                             , CGRectGetHeight(secondSepLine.bounds))];
     thridSepLine.image = sepImage;
    [_noAddressView addSubview:thridSepLine];
    
    //详细地址
    NSString *userAddressTitle = @"详细地址：";
    CGSize userAddressTitleSize = [userAddressTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                                         maxSize:CGSizeMake(120, kitemHight)];
    UILabel *userAddressTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(userNameTitleLabel.frame)
                                                                              , CGRectGetMaxY(thridSepLine.frame)
                                                                              , userAddressTitleSize.width
                                                                              , CGRectGetHeight(userNameTitleLabel.bounds))];
    userAddressTitleLabel.text = userAddressTitle;
    userAddressTitleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    userAddressTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [_noAddressView addSubview:userAddressTitleLabel];
    
    _userAddressTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userAddressTitleLabel.frame)
                                                                         , CGRectGetMinY(userAddressTitleLabel.frame)
                                                                         , VIEW_WIDTH - 2*kleftSpace - CGRectGetWidth(userAddressTitleLabel.bounds)
                                                                         , CGRectGetHeight(userAddressTitleLabel.bounds))];
    _userAddressTextField.font = [UIFont systemFontOfSize:FontSize(15)];
    _userAddressTextField.placeholder = @"街道/小区/门牌号等详细信息";
    _userAddressTextField.textColor = [UIColor colorWithHexString:@"#555555"];
    [_noAddressView addSubview:_userAddressTextField];
    
    
    
}

//公共视图
- (void)setupCommonViewWithHadOrdersAddressStatus:(BOOL)isHadOrdersAddress {
    CGFloat y = 0;
    if (isHadOrdersAddress) {
        y = CGRectGetMaxY(self.hadAddressView.frame) + ktopSpace;
    } else {
        y = CGRectGetMaxY(self.noAddressView.frame) + ktopSpace;
    }
    _commonView = [[UIView alloc]initWithFrame:CGRectMake(0
                                                             , y
                                                             , CGRectGetWidth(self.bounds)
                                                             , kitemHight*3 + ksepLineHight*3)];
    _commonView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    [self addSubview:_commonView];
    
    UIImage *sepImage = [UIImage imageNamed:@"gray_line"];
    UIImageView *firstSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(0
                                                                             , 0
                                                                             , CGRectGetWidth(self.bounds)
                                                                             , ksepLineHight)];
    firstSepLine.image = sepImage;
    [_commonView addSubview:firstSepLine];
    //支付方式
    UILabel *payTypeTitleLabel = [self setLabelWithTitle:@"支付方式" fontSize:FontSize(15) textColor:@"#555555"];
    payTypeTitleLabel.frame = CGRectMake(kleftSpace, CGRectGetMaxY(firstSepLine.frame), 120, kitemHight);
    [_commonView addSubview:payTypeTitleLabel];
    
    _payTypeBtn = [[FL_Button alloc]initWithAlignmentStatus:FLAlignmentStatusRight];
    _payTypeBtn.frame =CGRectMake(CGRectGetWidth(self.bounds) - kleftSpace - 160
                             , CGRectGetMinY(payTypeTitleLabel.frame),
                             160
                             , CGRectGetHeight(payTypeTitleLabel.bounds));
    [_payTypeBtn setTitle:@"在线支付" forState:UIControlStateNormal];
    [_payTypeBtn setImage:[UIImage imageNamed:@"interface---arrow-right"] forState:UIControlStateNormal];
    _payTypeBtn.titleLabel.font =[UIFont systemFontOfSize:FontSize(15)];
    [_payTypeBtn addTarget:self action:@selector(selectedPayTypeEvent:) forControlEvents:UIControlEventTouchUpInside];
    [_payTypeBtn setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
    [_commonView addSubview:_payTypeBtn];
    //配送方式
    UIImageView *secondSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                             , CGRectGetMaxY(payTypeTitleLabel.frame)
                                                                             , CGRectGetWidth(self.bounds) - kleftSpace
                                                                             , ksepLineHight)];
    secondSepLine.image = sepImage;
    [_commonView addSubview:secondSepLine];
    
    UILabel *peiSongTitleLabel = [self setLabelWithTitle:@"配送方式" fontSize:FontSize(15) textColor:@"#555555"];
    peiSongTitleLabel.frame = CGRectMake(CGRectGetMinX(payTypeTitleLabel.frame)
                                         , CGRectGetMaxY(secondSepLine.frame)
                                         , CGRectGetWidth(payTypeTitleLabel.bounds)
                                         , CGRectGetHeight(payTypeTitleLabel.bounds));
    [_commonView addSubview:peiSongTitleLabel];
    
    _peiSongTypeBtn = [[FL_Button alloc]initWithAlignmentStatus:FLAlignmentStatusRight];
    _peiSongTypeBtn.frame =CGRectMake(CGRectGetMinX(self.payTypeBtn.frame)
                                  , CGRectGetMinY(peiSongTitleLabel.frame),
                                    CGRectGetWidth(self.payTypeBtn.bounds)
                                  , CGRectGetHeight(self.payTypeBtn.bounds));
    [_peiSongTypeBtn setTitle:@"门店配送" forState:UIControlStateNormal];
    [_peiSongTypeBtn setImage:[UIImage imageNamed:@"interface---arrow-right"] forState:UIControlStateNormal];
    _peiSongTypeBtn.titleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    [_peiSongTypeBtn addTarget:self action:@selector(selectPeiSongTypeEvent:) forControlEvents:UIControlEventTouchUpInside];
    [_peiSongTypeBtn setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
    [_commonView addSubview:_peiSongTypeBtn];
    //配送时间
    UIImageView *thridSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                              , CGRectGetMaxY(peiSongTitleLabel.frame)
                                                                              , CGRectGetWidth(secondSepLine.bounds)
                                                                              , CGRectGetHeight(secondSepLine.bounds))];
    thridSepLine.image = sepImage;
    [_commonView addSubview:thridSepLine];
    XiaoQuAndStoreInfoModel *model = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    CGSize storeNameSize = [model.storeName sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(160, kitemHight)];
    UILabel *storeNameLabel = [self setLabelWithTitle:model.storeName fontSize:FontSize(15) textColor:@"#555555"];
    storeNameLabel.frame = CGRectMake(CGRectGetMinX(thridSepLine.frame)
                                      , CGRectGetMaxY(thridSepLine.frame)
                                      , storeNameSize.width
                                      , CGRectGetHeight(peiSongTitleLabel.bounds));
    [_commonView addSubview:storeNameLabel];
    
    _peiSongIntroduceLabel = [self setLabelWithTitle:model.storePeiSongIntroduce fontSize:FontSize(14) textColor:@"#e73f18"];
    _peiSongIntroduceLabel.frame = CGRectMake(CGRectGetMaxX(storeNameLabel.frame) + khSpace
                                              , CGRectGetMinY(storeNameLabel.frame)
                                              , CGRectGetWidth(self.bounds) - CGRectGetMaxX(storeNameLabel.frame) - kvSpace
                                              , CGRectGetHeight(storeNameLabel.bounds));
    [_commonView addSubview:_peiSongIntroduceLabel];
    
    UIImageView *fouthSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                             , CGRectGetMaxY(_peiSongIntroduceLabel.frame)
                                                                             , CGRectGetWidth(thridSepLine.bounds)
                                                                             , CGRectGetHeight(thridSepLine.bounds))];
    fouthSepLine.image = sepImage;
    [_commonView addSubview:fouthSepLine];

}
//如果只包含自营商品，地址下面区间需要加分割线
- (void)setAddressViewSepLineWhenOnlySelfGoodsWithIsHadAddress:(BOOL)isHadOrdersAddress {
    UIImage *sepImage = [UIImage imageNamed:@"gray_line"];
    UIImageView *sepLineImageView = [[UIImageView alloc]initWithImage:sepImage];
    CGFloat y = 0;
    if (isHadOrdersAddress) {
        y = CGRectGetMaxY(_hadAddressView.frame) + 12 - ksepLineHight;
    } else {
        y = CGRectGetMaxY(_noAddressView.frame) + 12 - ksepLineHight;
    }
    sepLineImageView.frame = CGRectMake(0,y, VIEW_WIDTH, ksepLineHight);
    [self addSubview:sepLineImageView];
}
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
- (void)updateViewWithReceivedAddressStatus:(BOOL)isHadReceivedAddress {
    
}
- (void)updataWithName:(NSString*)name telNumber:(NSString*)telNumber address:(NSString*)address {
    NSString *nameAndTelNumStr = [NSString stringWithFormat:@"%@    %@",name,telNumber];
    self.hadAddressNameLabel.text = nameAndTelNumStr;
    self.hadAddressAddressLabel.text = address;
}

#pragma mark -- UIGestureRecognizerDelegate
- (void)hideKeyboard {
    if ([self.userNameTextField isFirstResponder])
    {
        [self.userNameTextField resignFirstResponder];
    }
    if ([self.userTelePhoneTextField isFirstResponder])
    {
        [self.userTelePhoneTextField resignFirstResponder];
    }
    if ([self.userAddressTextField isFirstResponder]) {
        [self.userAddressTextField resignFirstResponder];
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.userNameTextField.isFirstResponder || self.userTelePhoneTextField.isFirstResponder || self.userAddressTextField.isFirstResponder) {
        return YES;
    }
    return NO;
}
#pragma mark -- CheckBoxViewDelegate
- (void)selectedItemWithIndex:(NSInteger)buttonTag withCheckBoxView:(CheckBoxView*)checkboxView {
    if (checkboxView.tag == 100) {//性别选择
        if (buttonTag == 0) {//男
            self.sexType = @"男";
        } else {//女
            self.sexType = @"女";
        }
        
    }
}
#pragma mark -- button Action
- (void)modifiedAddressEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(reSetMyGoodsReceivedAddress)]) {
        [_delegate reSetMyGoodsReceivedAddress];
    }
}
- (void)selectedPayTypeEvent:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(selectPayTypeOrSendGoodsTypeWithType:)]) {
        [_delegate selectPayTypeOrSendGoodsTypeWithType:kselectPayType];
    }
}
- (void)selectPeiSongTypeEvent:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(selectPayTypeOrSendGoodsTypeWithType:)]) {
        [_delegate selectPayTypeOrSendGoodsTypeWithType:kselectSendGoodsType];
    }
}
#pragma mark -- setter method
- (void)setStoreGoodsPayType:(NSString *)storeGoodsPayType {
    [self.payTypeBtn setTitle:storeGoodsPayType forState:UIControlStateNormal];
    if ([storeGoodsPayType isEqualToString:@"在线支付"]) {
        _storeGoodsPayType = @"1";
    } else if ([storeGoodsPayType isEqualToString:@"货到付款"]) {
        _storeGoodsPayType = @"2";
    } else {
        _storeGoodsPayType = @"";
    }
}

- (void)setStoreGoodsSendProductType:(NSString *)storeGoodsSendProductType {
    [self.peiSongTypeBtn setTitle:storeGoodsSendProductType forState:UIControlStateNormal];
    if ([storeGoodsSendProductType isEqualToString:@"门店配送"]) {
         _storeGoodsSendProductType = @"1";
    } else if ([storeGoodsSendProductType isEqualToString:@"上门自取"]) {
         _storeGoodsSendProductType = @"2";
    } else {
        _storeGoodsSendProductType = @"";
    }
}

- (void)setSelfGoodsPayType:(NSString *)selfGoodsPayType {
    _selfGoodsPayType = selfGoodsPayType;
}

- (void)setSelfGoodsSendProductType:(NSString *)selfGoodsSendProductType {
    _selfGoodsSendProductType = selfGoodsSendProductType;
}
- (void)setPeiSongIntroduceMessage:(NSString *)peiSongIntroduceMessage {
    _peiSongIntroduceMessage = peiSongIntroduceMessage;
    self.peiSongIntroduceLabel.text = peiSongIntroduceMessage;
}
@end
