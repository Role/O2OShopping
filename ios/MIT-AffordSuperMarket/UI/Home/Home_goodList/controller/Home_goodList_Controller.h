//
//  Home_goodList_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "BaseViewController.h"

@interface Home_goodList_Controller : BaseViewController
@property(nonatomic,strong)NSString *listTag;
@property(nonatomic,strong)NSString *listName;
@end
