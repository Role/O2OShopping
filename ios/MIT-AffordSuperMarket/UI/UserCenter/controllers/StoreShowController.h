//
//  StoreShowController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： StoreShowController
 Created_Date： 20151210
 Created_People： GT
 Function_description： 门店展示
 ***************************************/

#import "BaseViewController.h"

@interface StoreShowController : BaseViewController

@end
