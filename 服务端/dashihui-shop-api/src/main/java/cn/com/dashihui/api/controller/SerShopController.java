package cn.com.dashihui.api.controller;

import com.jfinal.kit.StrKit;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.CacheHolder;
import cn.com.dashihui.api.common.CacheKey;
import cn.com.dashihui.api.common.ResultMap;
import cn.com.dashihui.api.dao.SerFeedback;
import cn.com.dashihui.api.dao.SerShop;
import cn.com.dashihui.api.service.SerShopService;
import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.kit.ValidateKit;

public class SerShopController extends BaseController {
	
	private SerShopService shopService = new SerShopService();
	
	/**
	 * 店铺人员登录
	 * @param PHONE 用户名称
	 * @param PASSWORD 
	 */
	public void login(){
		String phone = getPara("PHONE");
		String password = getPara("PASSWORD");
		if(StrKit.isBlank(phone)){
			renderFailed("参数PHONE不能为空");
			return;
		}else if(StrKit.isBlank(password)){
			renderFailed("参数PASSWORD不能为空");
			return;
		}else{
			//登录验证
			SerShop shop = shopService.getShopByPhone(phone);
			if(shop == null){
				renderFailed("用户名或密码错误");
				return;
			}else if(!CommonKit.passwordsMatch(password,shop.getStr("password"))){
				renderFailed("用户名或密码错误");
				return;
			}else if(shop.getInt("enabled")!=1){
				renderFailed("当前帐号不可用");
				return;
			}else{
				//创建用户与Client之间的session，即登录操作
	    		//String sessionid = CommonKit.getUUID().concat(CommonKit.randomNum(4));
	    		//CacheHolder.cache(sessionid, sessionid);
	    		//renderSuccess(ResultMap.newInstance().put("TOKEN", sessionid).put("SHOP", shop));
				renderSuccess(ResultMap.newInstance().put("SHOP", shop)); 
	    		return;
			}
		}
	}
	
	/**
	 * 店铺详情
	 * @param ID 店铺ID
	 */
	public void detail(){
		String id= getPara("ID");
		if(StrKit.isBlank(id)){
			renderFailed("参数ID不能为空");
			return;
		}else{
			SerShop shop = shopService.getShopById(Integer.valueOf(id).intValue());
			if(shop != null){
				renderSuccess(shop);
				return;
			}else{
				renderFailed("商铺信息不存在");
				return;
			}
		}
	}

	/**
	 * 改变商家店铺的营业状态
	 * @param ID 店铺ID
	 * @param FLAG 1：营业中，  0：休息中
	 */
	public void changeWork(){
		String id= getPara("ID");
		String flag = getPara("FLAG");
		if(StrKit.isBlank(id)){
			renderFailed("参数ID不能为空");
			return;
		}else if(StrKit.isBlank(flag)){
			renderFailed("参数FLAG不能为空");
			return;
		}else{
			if(shopService.changeWork(Integer.valueOf(id).intValue(),Integer.valueOf(flag).intValue())){
				renderSuccess();
				return;
			}else{
				renderFailed("修改营业状态发生错误");
				return;
			}
		}
	}
	/**
	 * 重置密码
     * @param SIGNATURE 设备识别码
	 * @param PHONE 手机号码
	 * @param CODE 验证码
	 * @param PWD 新密码
	 * @param VERIFYPWD 确认密码
	 */
	public void resetPwd(){
    	String phone = getPara("PHONE");
    	String code = getPara("CODE");
    	String pwd = getPara("PWD");
    	String verifypwd = getPara("CONFIRMPWD");
    	if(StrKit.isBlank(phone)){
    		renderFailed("参数PHONE不能为空");
    		return;
    	}else if(StrKit.isBlank(code)){
    		renderFailed("参数CODE不能为空");
    		return;
    	}else if(shopService.getShopByPhone(phone)==null){
    		renderFailed("手机号未注册");
    		return;
    	}else if(StrKit.isBlank(pwd)){
    		renderFailed("密码为空");
    		return;
    	}else if(ValidateKit.Password_reg(pwd)){
    		renderFailed("密码只能填写数字、字母、下划线组合");
    		return;
    	}else if(pwd.length()<6||pwd.length()>18){
    		renderFailed("请保持密码长度在6-18之间");
    		return;
    	}else if(StrKit.isBlank(verifypwd)){
    		renderFailed("确认密码为空");
    		return;
    	}else if(!pwd.equals(verifypwd)){
    		renderFailed("两次密码输入不一致");
    		return;
    	}
		String cacheKey = CacheHolder.key(CacheKey.CACHE_RESETPWD_SMS_VALID_CODE_KEY,phone);
		if(!CacheHolder.has(cacheKey)){
			renderFailed("未获取短信验证码或验证码已超时，请重新获取");
			return;
		}else if(!CacheHolder.getStr(cacheKey).equals(code)){
			renderFailed("验证码不正确");
			return;
		}
		//移除缓存中的验证码
		CacheHolder.remove(cacheKey);
		if(shopService.updateShopPwd(phone,CommonKit.encryptPassword(pwd))){
			renderSuccess();
		}else{
			renderFailed("重置密码失败");
		}
	}
	/**
	 * 用户反馈
	 * @param SHOPID 商家ID
     * @param CONTEXT 反馈内容
	 */
	public void feedback(){
		String  shopid = getPara("SHOPID");
    	String  context = getPara("CONTEXT");
    	if(StrKit.isBlank(shopid)){
    		renderFailed("参数SHOPID不能为空");
    		return;
    	}else if(StrKit.isBlank(context)){
    		renderFailed("参数CONTEXT不能为空");
    		return;
    	}else{
    		SerFeedback feedback = new SerFeedback();
    		feedback.set("shopid", Integer.valueOf(shopid).intValue())
    		        .set("context",context);
    		if(shopService.saveFeedback(feedback)){
    			renderSuccess();
    			return;
    		}
    	}
    	renderFailed();
	}
}