//
//  CheckBoxView.h
//  BooksListTest
//
//  Created by apple on 15/11/12.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： CheckBoxView
 Created_Date： 20151108
 Created_People： GT
 Function_description： 单选框视图
 ***************************************/

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, CheckBoxType) {
    kleftImageAndRightTitle = 0,//左边image右边title
    kleftTitleAndRightImage //左边标题右边image
};

@class CheckBoxView;
@protocol CheckBoxViewDelegate <NSObject>
- (void)selectedItemWithIndex:(NSInteger)buttonTag withCheckBoxView:(CheckBoxView*)checkboxView;
@end
@interface CheckBoxView : UIView

@property(nonatomic)NSInteger selectedItemIndex;
@property(nonatomic)CheckBoxType checkBoxType;
@property(nonatomic, unsafe_unretained)id<CheckBoxViewDelegate>delegate;
- (instancetype)initWithFrame:(CGRect)frame withTitles:(NSArray*)titles withCheckBoxType:(CheckBoxType)boxType normalImage:(UIImage*)normalImage slectedImage:(UIImage*)selectedImage;
- (instancetype)initWithFrame:(CGRect)frame withiTitles:(NSArray*)titles normalImage:(UIImage*)normalImage slectedImage:(UIImage*)selectedImage;


@end
