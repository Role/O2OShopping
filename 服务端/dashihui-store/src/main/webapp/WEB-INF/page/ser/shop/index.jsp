<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">服务商家</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-4 col-lg-5">
				<input type="text" id="sTitle" value="" class="form-control pull-left width200" placeholder="商家名称" maxlength="50" />
				<button type="button" class="btn btn-success pull-left m-l-5" onclick="doSearch()">查询</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div><p></p></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
						<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
								<th width="25%">商家名称</th>
								<th width="10%">用户名</th>
								<th width="30%">地址</th>
								<th width="10%">电话</th>
								<th width="5%">营业中</th>
								<th width="5%">状态</th>
								<th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="{{item.id}}" data-id="{{item.id}}">
		<td>{{item.name}}</td>
		<td>{{item.username}}</td>
		<td>{{item.address}}</td>
		<td>{{item.tel}}</td>
		{{if item.isWork==1}}
		<td><span class="fa fa-check text-success"></span></td>
		{{else if item.isWork==0}}
		<td><span class="fa fa-ban"></span></td>
		{{/if}}
		{{if item.enabled==1}}
		<td><span class="fa fa-check text-success"></span></td>
		{{else if item.enabled==0}}
		<td><span class="fa fa-ban"></span></td>
		{{/if}}
		<td>
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toEditPwd('{{item.id}}')" title="修改密码"><span class="fa fa-lock fa-lg"></span></button> 
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="doDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button> 
		</td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(doSearch);
function doSearch(){
	var t = $("#title").val();
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/ser/shop/page",{t:t,"pageSize":20},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var addDialog;
function toAdd(p,c,a){
	addDialog = Kit.dialog("添加","${BASE_PATH}/ser/shop/toAdd").open();
}
var editDialog;
function toEdit(id,a){
	editDialog = Kit.dialog("修改","${BASE_PATH}/ser/shop/toEdit/"+id).open();
}
var editPwdDialog;
function toEditPwd(id){
	editPwdDialog = Kit.dialog("修改密码","${BASE_PATH}/ser/shop/toEditPwd/"+id).open();
}
function doDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/ser/shop/doDelete/"+id,function(result){
			dataPaginator.loadPage(1);
		});
	});
}
</script>
</body>
</html>