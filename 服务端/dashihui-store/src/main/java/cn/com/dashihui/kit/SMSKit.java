package cn.com.dashihui.kit;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jfinal.core.Const;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Prop;

import net.sf.json.JSONObject;

/**
 * 发短信工具类，调用的是“聚合数据”网接口（http://www.juhe.cn/）
 * 需要事先在“聚合”网上添加相应的短信模板，并通过审核才可使用
 * @author duxikun
 */
public class SMSKit {
	private static Logger logger = Logger.getLogger(SMSKit.class);
	
	private static boolean isDebug(){
		Prop prop = new Prop("realtime.properties",Const.DEFAULT_ENCODING);
		return prop.getBoolean("debug.sms", true);
	}
	
	/**
	 * （家政）店铺接单后，向用户发送短信通知
	 * @param msisdn 手机号
	 * @param serTime 预约时间
	 * @param cusAddress 预约地点
	 */
	public static boolean onStoreAcceptToCustomer(String msisdn, String serTime, String cusAddress){
		if(!isDebug()){
			Map<String, String> params = new HashMap<String, String>();
			params.put("mobile", msisdn);
			//模板ID
			params.put("tpl_id", "10875");
			params.put("tpl_value", "#serTime#="+serTime+"&#cusAddress#="+cusAddress);
			params.put("key", "195f4ee17e4ea9ce49270dae01f34e88");
			params.put("dtype", "json");
			logger.info("（家政）店铺接单后，向用户"+msisdn+"发送短信通知");
			String resultStr = HttpKit.get("http://v.juhe.cn/sms/send",params);
			JSONObject result = JSONObject.fromObject(resultStr);
			logger.info("（家政）店铺接单后，向用户"+msisdn+"发送短信通知返回结果："+resultStr);
			return result.getInt("error_code")==0;
		}else{
			logger.info("（家政）店铺接单后，向用户"+msisdn+"发送短信通知");
			return true;
		}
	}
	
	/**
	 * （其他）店铺确认派单后，向商家发送短信通知
	 * @param msisdn 手机号
	 * @param cusName 用户名称
	 * @param cusTel 用户电话
	 * @param serItem 服务项
	 * @param serTime 预约时间
	 * @param cusAddress 预约地点
	 */
	public static boolean onStoreDispatchToShop(String msisdn, String cusName, String cusTel, String serItem, String serTime, String cusAddress){
		if(!isDebug()){
			Map<String, String> params = new HashMap<String, String>();
			params.put("mobile", msisdn);
			//模板ID
			params.put("tpl_id", "10877");
			params.put("tpl_value", "#cusName#="+cusName+"&#cusTel#="+cusTel+"&#serItem#="+serItem+"&#serTime#="+serTime+"&#cusAddress#="+cusAddress);
			params.put("key", "195f4ee17e4ea9ce49270dae01f34e88");
			params.put("dtype", "json");
			logger.info("（其他）店铺确认派单后，向商家"+msisdn+"发送短信通知");
			String resultStr = HttpKit.get("http://v.juhe.cn/sms/send",params);
			JSONObject result = JSONObject.fromObject(resultStr);
			logger.info("（其他）店铺确认派单后，向商家"+msisdn+"发送短信通知返回结果："+resultStr);
			return result.getInt("error_code")==0;
		}else{
			logger.info("（其他）店铺确认派单后，向商家"+msisdn+"发送短信通知");
			return true;
		}
	}
	
	/**
	 * （其他）店铺确认派单后，向用户发送短信通知
	 * @param msisdn 手机号
	 * @param serName 服务商家名称
	 * @param serTel 预约时间
	 */
	public static boolean onStoreDispatchToCustomer(String msisdn, String serName, String serTel){
		if(!isDebug()){
			Map<String, String> params = new HashMap<String, String>();
			params.put("mobile", msisdn);
			//模板ID
			params.put("tpl_id", "10876");
			params.put("tpl_value", "#serName#="+serName+"&#serTel#="+serTel);
			params.put("key", "195f4ee17e4ea9ce49270dae01f34e88");
			params.put("dtype", "json");
			logger.info("（其他）服务商家接单后，向用户"+msisdn+"发送短信通知");
			String resultStr = HttpKit.get("http://v.juhe.cn/sms/send",params);
			JSONObject result = JSONObject.fromObject(resultStr);
			logger.info("（其他）服务商家接单后，向用户"+msisdn+"发送短信通知返回结果："+resultStr);
			return result.getInt("error_code")==0;
		}else{
			logger.info("（其他）服务商家接单后，向用户"+msisdn+"发送短信通知");
			return true;
		}
	}
}
