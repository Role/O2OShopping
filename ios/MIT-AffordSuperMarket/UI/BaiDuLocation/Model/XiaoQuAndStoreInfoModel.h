//
//  XiaoQuAndStoreInfoModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/28.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XiaoQuAndStoreInfoModel : NSObject<NSCoding>
@property(nonatomic, strong)NSString *xiaoQuAddress;
@property(nonatomic, strong)NSString *xiaoQuID;
@property(nonatomic, strong)NSString *xiaoQuName;
@property(nonatomic, strong)NSString *storeID;
@property(nonatomic, strong)NSString *storeName;
@property(nonatomic, strong)NSString *storeAddress;
@property(nonatomic, strong)NSString *storePhone;//门店电话
@property(nonatomic, strong)NSString *storeYingYeTime;//营业时间
@property(nonatomic, strong)NSString *storePeiSongIntroduce;//配送介绍;
@end
