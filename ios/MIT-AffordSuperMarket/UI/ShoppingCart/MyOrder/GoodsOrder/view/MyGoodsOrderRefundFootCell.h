//
//  MyGoodsOrderRefundFootCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/24.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyGoodSOrderRefundModel.h"
@interface MyGoodsOrderRefundFootCell : UITableViewCell
- (void)updateViewWithData:(MyGoodSOrderRefundModel*)refundModel stateMent:(NSString *)stateMent;
- (CGFloat)cellFactHight;
@end
