//
//  ShoppingCart_RootCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
#define KCellHigth HightScalar(147)
#define KRightSpacing 15
#import "ShoppingCart_RootCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
@interface  ShoppingCart_RootCell()
//选中按钮
@property (retain,nonatomic)UIButton *choiceBtn;
//商品图片
@property (retain,nonatomic)UIImageView * productLogo;
//商品名称
@property (retain,nonatomic)UILabel *productName;
//商品价格
@property (retain,nonatomic)UILabel *productMoney;
//商品原价格
@property (retain,nonatomic)UILabel *productMoneyOriginalPrice;
//左减号
@property (retain,nonatomic)UIButton *leftBtn;
//右加号
@property (retain,nonatomic)UIButton *rightBtn;
//Quantity 商品数量
@property (retain,nonatomic)UILabel* productQuantity;

@property (retain,nonatomic)UILabel *subtotalMoney;



@end


@implementation ShoppingCart_RootCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        self.backgroundColor =[UIColor whiteColor];
        [self setUpSuberView];
    }
    return self;
}

#pragma  mark--初始化控件
-(void)setUpSuberView
{
      //选中按钮
    _choiceBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    self.choiceBtn.frame=CGRectMake(0, (KCellHigth-HightScalar(40))/2, HightScalar(40), HightScalar(40));
    [_choiceBtn setImage:[UIImage imageNamed:@"checkBoxNormalImage"] forState:UIControlStateNormal];
    [_choiceBtn setImage:[UIImage imageNamed:@"checkBoxSelectdImage"] forState:UIControlStateSelected];
    [_choiceBtn addTarget:self action:@selector(choiceBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    _choiceBtn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    
    [self.contentView addSubview:_choiceBtn];
     //商品图片
    _productLogo=[[UIImageView alloc]init];
    self.productLogo.frame=CGRectMake(CGRectGetMaxX(self.choiceBtn.frame)+10, (KCellHigth-HightScalar(106))/2,HightScalar(106), HightScalar(106));
    [_productLogo.layer setBorderWidth:0.7];
    [_productLogo.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    [_productLogo.layer setMasksToBounds:YES];
    [_productLogo.layer setCornerRadius:4];
    [self.contentView addSubview:_productLogo];
    //商品名称
    _productName=[[UILabel alloc]init];
    self.productName.frame=CGRectMake(CGRectGetMaxX(self.productLogo.frame)+10,CGRectGetMinY(self.productLogo.frame),VIEW_WIDTH-CGRectGetWidth(self.choiceBtn.bounds)-CGRectGetWidth(self.productLogo.bounds)-KRightSpacing-10,HightScalar(50));
    [self.contentView addSubview:_productName];
    _productName.numberOfLines = 0;
    _productName.textColor=[UIColor colorWithHexString:@"#555555"];
    _productName.font =[UIFont systemFontOfSize:FontSize(15)];
    
    //现价
    _productMoney=[[UILabel alloc]init];
    self.productMoney.frame =CGRectMake(CGRectGetMinX(self.productName.frame), CGRectGetMaxY(self.productName.frame)+5, CGRectGetWidth(self.productLogo.bounds), HightScalar(20));
    [self.contentView addSubview:_productMoney];
    _productMoney.textColor=[UIColor colorWithHexString:@"#ff0000"];
    _productMoney.font =[UIFont systemFontOfSize:FontSize(16)];
    
    //原价
    _productMoneyOriginalPrice =[[UILabel alloc]init];
    self.productMoneyOriginalPrice.frame=CGRectMake(CGRectGetMinX(self.productMoney.frame)
                                                    , CGRectGetMaxY(self.productMoney.frame)
                                                    , CGRectGetWidth(self.productMoney.bounds)
                                                    , HightScalar(20));
    [self.contentView addSubview:_productMoneyOriginalPrice];
    _productMoneyOriginalPrice.textColor=[UIColor colorWithHexString:@"#999999"];
    _productMoneyOriginalPrice.font =[UIFont systemFontOfSize:FontSize(12)];
    
    //右边加按钮
    _rightBtn =[[UIButton alloc]init];
    self.rightBtn.frame =CGRectMake(VIEW_WIDTH - 45 - 10
                                    , CGRectGetMaxY(self.productName.frame)
                                    , 45, 45);
    [self.contentView addSubview:_rightBtn];
    [_rightBtn addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBtn setImage:[UIImage imageNamed:@"btn_cart_normal_jia"] forState:UIControlStateNormal];
    [_rightBtn setImage:[UIImage imageNamed:@"btn_cart_press_jia"] forState:UIControlStateHighlighted];
    _rightBtn.tag=12;
    //购买数量
    _productQuantity =[[UILabel alloc]init];
    self.productQuantity.frame=CGRectMake(CGRectGetMinX(self.rightBtn.frame) - 30
                                          , CGRectGetMinY(self.rightBtn.frame)
                                          , 30
                                          , CGRectGetHeight(self.rightBtn.bounds));
    [self.contentView addSubview:_productQuantity];
    _productQuantity.textColor=[UIColor colorWithHexString:@"#555555"];
    
    _productQuantity.textAlignment=NSTextAlignmentCenter;
    _productQuantity.font=[UIFont systemFontOfSize:HightScalar(14)];
    
     //左边减按钮
    _leftBtn =[[UIButton alloc]init];
    self.leftBtn.frame =CGRectMake(CGRectGetMinX(self.productQuantity.frame) - CGRectGetWidth(self.rightBtn.bounds)
                                   , CGRectGetMinY(self.productQuantity.frame)
                                   , CGRectGetWidth(self.rightBtn.bounds)
                                   , CGRectGetHeight(self.rightBtn.bounds));
    [self.contentView addSubview:_leftBtn];
    [_leftBtn setImage:[UIImage imageNamed:@"btn_cart_normal_jian"] forState:UIControlStateNormal];
    [_leftBtn setImage:[UIImage imageNamed:@"btn_cart_press_jian"] forState:UIControlStateHighlighted];
    [_leftBtn addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
    _leftBtn.tag=11;
    
    //小计内容
    _subtotalMoney =[[UILabel alloc]init];
    self.subtotalMoney.frame=CGRectMake(VIEW_WIDTH - VIEW_WIDTH/3*2 - 15-20
                                        ,CGRectGetMaxY(self.productMoneyOriginalPrice.frame)
                                        , VIEW_WIDTH/3*2
                                        , 20);
    [self.contentView addSubview:_subtotalMoney];
    _subtotalMoney.font=[UIFont systemFontOfSize:HightScalar(14)];
    _subtotalMoney.textColor=[UIColor colorWithHexString:@"#000000"];
    _subtotalMoney.textAlignment =NSTextAlignmentRight;
    _subtotalMoney.textAlignment=NSTextAlignmentRight;
}
//点击选中按钮事件
-(void)choiceBtnClick:(UIButton *)sender
{
    UIButton *button = (UIButton*)sender;
    button.selected = !button.selected;
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItemAtIndexPath: selected:)]) {
        [_delegate selectedItemAtIndexPath:self.indexPath selected:button.selected];
    }
    
}
#pragma mark ---点击加减按钮事件
-(void)btnClike:(id)sender
{
    UIButton *btn =(UIButton*)sender;
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItemAtIndexPath:tag:)]) {
        [_delegate selectedItemAtIndexPath:self.indexPath  tag:btn.tag];
    }
}
-(void)settingDataWithdicData:(NSDictionary *)dicData

{
    if ([[dicData objectForKey:@"ischoose"] isEqualToString:@"1"]) {
         self.choiceBtn.selected=YES;
    }else{
        self.choiceBtn.selected=NO;
    }
    self.productLogo.image=[UIImage imageWithData:[dicData objectForKey:@"picture"]];
    
    self.productName.text = [dicData objectForKey:@"name"];
    
    self.productMoney.text =[NSString stringWithFormat:@"￥%@",[dicData objectForKey:@"price"]];
    
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%@",[dicData objectForKey:@"oldprice"]] attributes:attribtDic];
    
    self.productMoneyOriginalPrice.attributedText = attribtStr;
    
    CGFloat totalPric = [[dicData objectForKey:@"buynum"] floatValue]*[[dicData objectForKey:@"price"] floatValue];
    //设置小计 后半段为红色
    NSString* strr=[NSString stringWithFormat:@"小计:￥%.2f",totalPric];
    self.subtotalMoney.text =strr;
    
    
    self.productQuantity.text= [dicData objectForKey:@"buynum"];
    
}

@end
