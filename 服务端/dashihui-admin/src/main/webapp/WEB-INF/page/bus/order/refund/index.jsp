<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.web.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>退款单管理</title>
    <jsp:include page="../../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">退款单</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-2 col-lg-9">
				<label class="search-label">退款单编号：</label><input type="text" id="sOrderRefundNum" value="" class="form-control search-input width200" maxlength="21">
				<label class="search-label">订单编号：</label><input type="text" id="sOrderNum" value="" class="form-control search-input width200" maxlength="21">
				<label class="search-label">下单时间：</label><input type="text" id="sBeginDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" maxlength="10">
				<label class="search-label">至</label><input type="text" id="sEndDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" maxlength="10">
				<div class="clearfix"></div>
			</div>
			<div class="col-lg-offset-2 col-lg-9 m-t-10">
				<label class="search-label">订单状态：</label>
				<div class="search-input"><select id="sState" class="selectpicker" data-width="150">
					<option value="0">全部</option>
					<option value="1">待处理</option>
					<option value="2">审核中</option>
					<option value="3">审核通过</option>
					<option value="4">审核不通过</option>
				</select></div>
				<button class="btn btn-success search-btn" onclick="query();">确定</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-hover">
			            <thead>
			                <tr>
			                    <th width="15%">退款单单号</th>
								<th width="15%">原订单号</th>
								<th width="10%">类型</th>
								<th width="10%">支付渠道</th>
								<th width="7%">订单金额</th>
								<th width="7%">退款金额</th>
								<th width="6%">状态</th>
								<th width="15%">申请时间</th>
								<th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item index}}
	<tr id="item{{item.refundNum}}" data-id="{{item.refundNum}}">
		<td>{{item.refundNum}}</td>
		<td>{{item.orderNum}}</td>
		<td>{{item.type | flagTransform:<%=OrderCode.RefundType.CANCEL_ORDER%>,'订单取消',<%=OrderCode.RefundType.CANCEL_SINGLE%>,'商品取消',<%=OrderCode.RefundType.CHANGE_REFUND%>,'找零'}}</td>
		<td>{{item.payMethod | flagTransform:<%=OrderCode.OrderPayMethod.ALIPAY%>,'支付宝',<%=OrderCode.OrderPayMethod.WEIXIN%>,'微信'}}</td>
		<td>{{item.orderAmount}}</td>
		<td>{{item.amount}}</td>
		<td>
			{{item.state | flagTransform:<%=OrderCode.RefundState.VERIFYING%>,'审核中',<%=OrderCode.RefundState.VERIFIED%>,'通过',<%=OrderCode.RefundState.VERIFY_FAILED%>,'不通过',<%=OrderCode.RefundState.REFUNDED%>,'已退款'}}
			{{if item.state==<%=OrderCode.RefundState.VERIFY_FAILED%> && !isBlank(item.refuseReason)}}<br/>{{item.refuseReason}}{{/if}}
		</td>
		<td>{{item.createDate | dateFormat:'yyyy-MM-dd hh:mm'}}</td>
		<td>
			{{if item.state==<%=OrderCode.RefundState.VERIFYING%>}}
			<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doAgree('{{item.refundNum}}')">通过</button>
			<button type="button" class="btn btn-sm btn-success m-t-5" onclick="doRefuse('{{item.refundNum}}')">不通过</button>
			{{else if item.state==<%=OrderCode.RefundState.VERIFIED%>}}
			<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doRefund('{{item.refundNum}}','{{item.payMethod}}')">退款</button>
			{{else if item.state==<%=OrderCode.RefundState.VERIFY_FAILED%>}}
			{{else if item.state==<%=OrderCode.RefundState.REFUNDED%>}}
			{{/if}}
		</td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(query);
function query(){
	var params = {
		pageSize:10,
		refundNum:$("#sRefundNum").val(),
		orderNum:$("#sOrderNum").val(),
		beginDate:$("#sBeginDate").val(),
		endDate:$("#sEndDate").val(),
		state:$("#sState").val()
	};
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/bus/order/refund/page",params,function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var showOrderDialog;
function showOrder(orderNum){
	showOrderDialog = Kit.dialog("查看订单","${BASE_PATH}/bus/order/refund/detail?orderNum="+orderNum,{size:'size-wide',closable:true}).open();
}
function doAgree(refundNum){
	Kit.confirm("提示","确定要审核通过？",function(){
		$.post("${BASE_PATH}/bus/order/refund/doAgree",{'refundNum':refundNum},function(result){
			if(result.flag==0){
				$("#item"+refundNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
		});
	});
}
function doRefuse(refundNum){
	Kit.confirm("提示","审核不通过？<input type='text' id='refuseReason' class='form-control m-t-5' placeholder='请输入不通过的原因'>",function(ref){
		var reason = $("#refuseReason").val();
		$.post("${BASE_PATH}/bus/order/refund/doRefuse",{'refundNum':refundNum,"reason":reason},function(result){
			if(result.flag==0){
				$("#item"+refundNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
		});
	});
}
var alipayRefundDialog;
function doRefund(refundNum,payMethod){
	Kit.confirm("提示","确定要进行退款？",function(){
		if(payMethod==<%=OrderCode.OrderPayMethod.WEIXIN%>){
			$.post("${BASE_PATH}/bus/order/refund/doWXRefund",{'refundNum':refundNum},function(result){
				//1：退款单号为空，2：退款单不存在，3：状态不正确，4：微信支付申请退款成功，5：微信支付申请失败
				switch(result.flag){
					case 3:
						Kit.alert("退款失败");
						break;
					case 4:
						Kit.alert("退款成功");
						$("#item"+refundNum).replaceWith(template("dataTpl",{"list":[result.object]}));
						break;
					case 5:
						Kit.alert("退款失败");
						break;
				}
			});
		}else if(payMethod==<%=OrderCode.OrderPayMethod.ALIPAY%>){
			alipayRefundDialog = layer.open({
				type: 2,
				shadeClose: true,
				shade: false,
				maxmin: true, //开启最大化最小化按钮
				area: ['1000px', '730px'],
				content: '${BASE_PATH}/bus/order/refund/toAlipayPrepayRefund?refundNum='+refundNum
		    });
		}
	});
}
</script>
</body>
</html>