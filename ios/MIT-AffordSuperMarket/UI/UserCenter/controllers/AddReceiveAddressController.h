//
//  AddReceiveAddressController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： AddReceiveAddressController
 Created_Date： 20151117
 Created_People： GT
 Function_description： 添加收货地址页面
 ***************************************/

#import "BaseViewController.h"
#import "ReceivedAddressOBJECT.h"
typedef NS_ENUM(NSInteger, EditReceiveAddressType) {
    kaddReceiveAddressType = 0,//添加地址
    kmodifyReceiveAddressType //编辑地址
};
typedef NS_ENUM(NSInteger,AddReceiveAddressControllerSupperControllerType) {
    kUserCenterPushToAddReceiveAddressController = 0,//从个人中心push
    kSureOrderPushToAddReceiveAddressController //从确认订单push
};
@interface AddReceiveAddressController : BaseViewController
@property(nonatomic, strong)ReceivedAddressOBJECT *receivedAddressDatas;//收货地址model
@property(nonatomic)EditReceiveAddressType editAddressType;
@property(nonatomic)AddReceiveAddressControllerSupperControllerType supperControllerType;
@end
