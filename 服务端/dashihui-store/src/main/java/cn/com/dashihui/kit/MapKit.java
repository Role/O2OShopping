package cn.com.dashihui.kit;

import java.util.HashMap;
import java.util.Map;

/**
 * 一个简易的Map封装，可以实现new Map().put().put()....的设值形式，省事省代码
 * 用法举例：MapKit.newInstance().put(key,value).put(key.value)....getAttrs();
 */
public class MapKit {
	private Map<String,Object> attrs = new HashMap<String,Object>();
	
	private MapKit() {
	}
	
	public static MapKit newInstance(){
		return new MapKit();
	}
	
	public MapKit put(String key, Object value){
		this.attrs.put(key, value);
		return this;
	}
	
	public Map<String,Object> getAttrs(){
		return attrs;
	}
}
