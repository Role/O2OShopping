//
//  XiaoQuAndStoreInfoModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/28.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "XiaoQuAndStoreInfoModel.h"

@implementation XiaoQuAndStoreInfoModel
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.xiaoQuID forKey:@"xiaoQuID"];
    [aCoder encodeObject:self.xiaoQuName forKey:@"xiaoQuName"];
    [aCoder encodeObject:self.xiaoQuAddress forKey:@"xiaoQuAddress"];
    [aCoder encodeObject:self.storeID forKey:@"storeID"];
    [aCoder encodeObject:self.storeName forKey:@"storeName"];
    [aCoder encodeObject:self.storeAddress forKey:@"storeAddress"];
    [aCoder encodeObject:self.storePhone forKey:@"storePhone"];
    [aCoder encodeObject:self.storeYingYeTime forKey:@"storeYingYeTime"];
    [aCoder encodeObject:self.storePeiSongIntroduce forKey:@"storePeiSongIntroduce"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.xiaoQuName = [aDecoder decodeObjectForKey:@"xiaoQuName"];
        self.xiaoQuID = [aDecoder decodeObjectForKey:@"xiaoQuID"];
        self.xiaoQuAddress = [aDecoder decodeObjectForKey:@"xiaoQuAddress"];
        self.storeID = [aDecoder decodeObjectForKey:@"storeID"];
        self.storeName = [aDecoder decodeObjectForKey:@"storeName"];
        self.storeAddress = [aDecoder decodeObjectForKey:@"storeAddress"];
        self.storePhone =[aDecoder decodeObjectForKey:@"storePhone"];
        self.storeYingYeTime =[aDecoder decodeObjectForKey:@"storeYingYeTime"];
        self.storePeiSongIntroduce = [aDecoder decodeObjectForKey:@"storePeiSongIntroduce"];
    }
    return self;
}
@end
