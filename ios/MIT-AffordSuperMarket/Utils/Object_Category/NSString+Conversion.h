//
//  NSString+Conversion.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/28.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Conversion)
//把空字符串转换成 @“”
+ (NSString*)stringISNull:(NSString*)_str;
//判断字符串是否可用(可能是整型)
+ (NSString*)stringTransformObject:(id)object;
//判断字符串最后两位是否是0
+ (NSString*)marketLastTwoByteOfStringIsZero:(NSString*)_str;
//根据标题获取模式
+ (NSString*)productTypeWithItemTitle:(NSString*)title;
//拼接图片url
+ (NSString*)appendImageUrlWithServerUrl:(NSString*)urlStr;
//根据模式获取标题
+ (NSString*)itemTitleWithProductType:(NSString*)ProductType;
//支付类型
+ (NSString*)transitionPayType:(NSString*)payType;
//配送类型
+ (NSString*)transitionPeiSongType:(NSString*)peiSongType;
@end
