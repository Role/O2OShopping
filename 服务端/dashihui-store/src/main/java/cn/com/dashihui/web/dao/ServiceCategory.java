package cn.com.dashihui.web.dao;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class ServiceCategory extends Model<ServiceCategory>{
	
	private static final long serialVersionUID = 1L;
	private static ServiceCategory me = new ServiceCategory();
	public static ServiceCategory me(){
		return me;
	}
	//子级分类
	private boolean hasChildren = false;
	public List<ServiceCategory> getChildren() {
		return get("children");
	}
	public void setChildren(List<ServiceCategory> children) {
		put("children", children);
		hasChildren = true;
	}
	public boolean hasChildren(){
		return hasChildren;
	}
	public void addChild(ServiceCategory child){
		List<ServiceCategory> children = getChildren();
		if(children==null){
			children = new ArrayList<ServiceCategory>();
		}
		children.add(child);
		setChildren(children);
		hasChildren = true;
	}
}
