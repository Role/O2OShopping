//
//  UserCenter_MoreButtonCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
#define kleftSpace 15
#import "Global.h"
#import "UIColor+Hex.h"
#import "UserCenter_MoreButtonCell.h"
#import "UIImage+ColorToImage.h"
#import "NSString+Conversion.h"
@interface UserCenter_MoreButtonCell ()
@property(nonatomic, strong)NSMutableArray *buttons;
@property(nonatomic, strong)NSMutableArray *borderLabels;

@end
@implementation UserCenter_MoreButtonCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style titles:(NSArray*)titles images:(NSArray*)images tag:(NSArray*)tag reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _buttons = [[NSMutableArray alloc]init];
        _borderLabels = [[NSMutableArray alloc]init];
        [self setupSubViewWithTitles:titles images:images tag:tag];
    }
    return self;
}

#pragma mark -- 初始化视图
- (void)setupSubViewWithTitles:(NSArray*)titles images:(NSArray*)images tag:(NSArray*)tag
{
    CGFloat itemWidth = (VIEW_WIDTH - 2*kleftSpace)/titles.count;
    UIImage *backImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#eeeeee"]
                                                 frame:CGRectMake(0, 0, itemWidth, HightScalar(75))];
    for (NSInteger i = 0; i < [titles count]; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(kleftSpace + itemWidth*i, 0, itemWidth, HightScalar(75));
        [button setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
        [button setImage:[images objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
        [button setBackgroundImage:backImage forState:UIControlStateHighlighted];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

        NSString *str=[tag objectAtIndex:i];
        button.tag=str.floatValue;

        button.titleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        button.imageEdgeInsets = UIEdgeInsetsMake((CGRectGetHeight(button.bounds) - (CGRectGetHeight(button.imageView.bounds) + 10 + CGRectGetHeight(button.titleLabel.bounds)))/2
                                                  , (CGRectGetWidth(button.bounds) - CGRectGetWidth(button.imageView.bounds))/2
                                                  , 0
                                                  , 0);
        button.titleEdgeInsets = UIEdgeInsetsMake(CGRectGetMaxY(button.imageView.frame) + 10
                                                  , - CGRectGetWidth(button.imageView.bounds) +(CGRectGetWidth(button.bounds) - CGRectGetWidth(button.titleLabel.bounds))/2
                                                  , 0
                                                  , 0);
        [button addTarget:self action:@selector(jumpToPage:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:button];
        UILabel *boderlabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(button.imageView.frame) - 10
                                                                      , CGRectGetMinY(button.imageView.frame) - 4
                                                                       , HightScalar(16), HightScalar(16))];
        [boderlabel setBackgroundColor:[UIColor redColor]];
        boderlabel.font = [UIFont systemFontOfSize:FontSize(11)];
        boderlabel.textColor = [UIColor whiteColor];
        boderlabel.textAlignment = NSTextAlignmentCenter;
        [boderlabel.layer setMasksToBounds:YES];
        [boderlabel.layer setCornerRadius:8];
        boderlabel.hidden = YES;
        [button addSubview:boderlabel];
        [self.buttons addObject:button];
        [self.borderLabels addObject:boderlabel];
        
    }
}


#pragma mark -- buttonAction
- (void)jumpToPage:(id)sender {
    UIButton *button = (UIButton*)sender;
    if (_moreButtonCellDelegate && [_moreButtonCellDelegate respondsToSelector:@selector(selectMoreButtonCellItemWithIndex:)]) {
        [_moreButtonCellDelegate selectMoreButtonCellItemWithIndex:button.tag];
    }
}
- (void)updateBorderStatus:(NSDictionary*)dataDic orderType:(NSString *)orderType {
    if (dataDic.count == 0) {
        for (UILabel *label in self.borderLabels) {
            label.hidden = YES;
        }
    } else {
        if (dataDic.count != 0) {
            for (NSInteger i = 0; i < self.borderLabels.count; i++) {
                UILabel *lable = [self.borderLabels objectAtIndex:i];
                if (i == 0) {
                    if ([orderType isEqualToString:@"1"]) {
                        lable.text = [NSString stringTransformObject:[dataDic objectForKey:@"NOPAY"]];
                    }else{
                        lable.text =[NSString stringTransformObject:[dataDic objectForKey:@"SERNOPAY"]];
                    }
                    
                } else if (i == 1) {
                    if ([orderType isEqualToString:@"1"]) {
                        lable.text = [NSString stringTransformObject:[dataDic objectForKey:@"NOTAKE"]];
                    }else{
                        lable.text =[NSString stringTransformObject:[dataDic objectForKey:@"SERNOSERVICE"]];
                    }
                   
                } else if (i == 2) {
                    if ([orderType isEqualToString:@"1"]) {
                         lable.text = [NSString stringTransformObject:[dataDic objectForKey:@"NOEVAL"]];
                    }else{
                        lable.text = @"0";
                    }
                   
                }else if (i == 3) {
                    if ([orderType isEqualToString:@"1"]) {
                        lable.text = @"0";
                    }else{
                        lable.text = @"0";
                    }
                    
                }
                if ([lable.text isEqualToString:@"0"]) {
                    lable.hidden = YES;
                } else {
                    lable.hidden = NO;
                }
            }
        }
    }
    
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
