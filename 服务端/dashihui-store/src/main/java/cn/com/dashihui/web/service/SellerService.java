package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Seller;

public class SellerService {
	
	public boolean addSeller(Seller newObject){
		return newObject.save();
	}
	
	public boolean delSeller(int id){
		return Seller.me().deleteById(id);
	}
	
	public boolean editSeller(Seller object){
		return object.update();
	}
	
	public Seller findById(int id){
		return Seller.me().findFirst("SELECT * FROM t_dict_store_seller WHERE id=?",id);
	}
	
	public Page<Record> findByPage(int storeid, int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_dict_store_seller WHERE storeid=?");
		sqlExcept.append(" ORDER BY createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT * ", sqlExcept.toString(), storeid);
	}
	
	public boolean findExistsByUsername(String username){
		return Seller.me().findFirst("SELECT * FROM t_dict_store_seller WHERE username=?",username)!=null;
	}
	
	public boolean findExistsByUsernameWithout(String username,int userid){
		return Seller.me().findFirst("SELECT * FROM t_dict_store_seller WHERE username=? AND id!=?",username,userid)!=null;
	}
}
