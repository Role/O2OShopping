<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">服务分类</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
							<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
							<button type="button" class="btn btn-link" onclick="doSort()"><span class="fa fa-save"></span> 保存排序</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<form id="sortForm" action="<c:url value="/service/category/doSort"/>" method="post">
					<input type="hidden" name="sortKey" value="orderNo">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                    <th width="50"></th>
			                    <th>分类名</th>
			                    <th width="5%">代码</th>
			                    <th width="10%">排序</th>
								<th width="5%">显示</th>
								<th width="5%">可用</th>
			                    <th width="10%">添加时间</th>
			                    <th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
			        </form>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td><img src="${FTP_PATH}{{item.icon}}" width="40"/></td>
		<td>{{item.name}}</td>
		<td>{{item.code}}</td>
		<td><input name="orderNo{{item.id}}" value="{{item.orderNo}}" size="3" title="排序" class="order form-control width50 pull-left" required></td>
		{{if item.isShow==1}}
		<td><span class="fa fa-eye text-success" title="显示"></span></td>
		{{else if item.isShow==0}}
		<td><span class="fa fa-eye-slash" title="不显示"></span></td>
		{{/if}}
		{{if item.enabled==1}}
		<td><span class="fa fa-check text-success" title="可用"></span></td>
		{{else if item.enabled==0}}
		<td><span class="fa fa-ban" title="不可用"></span></td>
		{{/if}}
		<td>{{item.createDate | dateFormat:'yyyy-MM-dd'}}</td>
		<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button> <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{item.id}}','{{item.code}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button></td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(function(){
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/service/category/page",{"pageSize":10},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
});
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/service/category/toAdd").open();
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/service/category/toEdit/"+id).open();
}
function onEditSuccess(id,newObject){
	$("#item"+id).replaceWith(template("list-tpl",{"list":[newObject]}));
}
function toDelete(id,code){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/service/category/doDelete/"+code,function(result){
			$("#item"+id).remove();
		});
	});
}
function doSort(){
	$("#sortForm").ajaxSubmit({
		success:function(data){
			switch(data.flag){
			case -1:
				Kit.alert("系统异常，请重试");return;
			case 0:
				window.location.reload();
			}
		}
	});
}
</script>
</body>
</html>