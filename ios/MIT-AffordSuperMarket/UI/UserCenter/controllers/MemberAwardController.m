//
//  MemberAwardController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
#import <JavaScriptCore/JavaScriptCore.h>
#import "MemberAwardController.h"
//controller
#import "MembersShareController.h"
#import "Login_Controller.h"
//vendor
#import "RDVTabBarController.h"
#import "ManagerHttpBase.h"
//untils
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "AppDelegate.h"
#import "NSString+Conversion.h"
#import "RDVTabBarController.h"
#import "MTA.h"
#import "Global.h"
#import "MBProgressHUD.h"
#import "ManagerGlobeUntil.h"
@interface MemberAwardController ()<UIWebViewDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UIWebView *webView;
@property (nonatomic, strong)JSContext *jsContext;
@property(nonatomic,strong)NSString *userLevel;
@end

@implementation MemberAwardController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    self.title =@"会员奖励";
    [self addBackNavItem];
    [self setUpSubView];
    self.jsContext = [[JSContext alloc] init];
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."inView:self.view];
    if ([ManagerGlobeUntil sharedManager].transactionLogin) {
    [self loadUserBaseInfoData];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setWebViewData];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"会员奖励"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"会员奖励"];
}
//获取用户的基本信息
- (void)loadUserBaseInfoData {
    __weak typeof(self)weakSelf = self;
    //请求参数说明： SIGNATURE  设备识别码
    UserInfoModel *model = [[ManagerGlobeUntil sharedManager]getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:model.token forKey:@"TOKEN"];
    [manager parameters:parameter customPOST:@"user/info" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                
                _userLevel =[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"LEVEL"]];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
    } failure:^(bool isFailure) {
    }];
}

#pragma mark--获取WEB数据
-(void)setWebViewData
{
    //添加设备识别码
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *URLStr =[NSString stringWithFormat:@"%@user/rule?SIGNATURE=%@",baseServerUrl,[userDefaults objectForKey: Phone_Signature]];
   
//    URLStr = [NSString stringWithFormat:@"%@ser/order/daily",baseServerUrl];
    NSURL  *url =[[NSURL alloc]initWithString:URLStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
    
}
#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[ManagerGlobeUntil sharedManager] hideHUDFromeView:self.view];
    self.jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    [_webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
//    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.zoom=0.34"];
    self.jsContext.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        context.exception = exceptionValue;
        NSLog(@"异常信息：%@", exceptionValue);
    };
}
#pragma mark-- buttonClick
-(void)shareJumpClick
{
    if ([ManagerGlobeUntil sharedManager].transactionLogin) {
            if ([self.userLevel isEqualToString:@"2"]) {
            MembersShareController  *shareVc = [[MembersShareController alloc]init];
            [self.navigationController pushViewController:shareVc animated:YES];
        }else{
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"抱歉，您暂不满足条件" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
    } else {//跳转登录页面
        Login_Controller *loginVC = [[Login_Controller alloc]init];
        loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [self presentViewController:nav animated:YES completion:nil];
    }
   
}

-(void)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark--初始化控件
-(void)setUpSubView
{
    //初始化webView
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,VIEW_WIDTH , VIEW_HEIGHT-64-49)];
    _webView.scalesPageToFit = YES;
    _webView.delegate = self;
    _webView.backgroundColor=[UIColor whiteColor];
    _webView.scalesPageToFit = NO;
    [self.view addSubview:_webView];
    
    //底部按钮
    UIButton *shareJump=[UIButton buttonWithType:UIButtonTypeCustom];
    shareJump.frame=CGRectMake(0,  CGRectGetMaxY(self.webView.frame), VIEW_WIDTH,49);
    [shareJump setTitle:@"查看分享页，邀请好友注册" forState:UIControlStateNormal];
    shareJump.titleLabel.font=[UIFont systemFontOfSize:15];
    UIImage *shareJumpImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#c52720"]
                                                         frame:CGRectMake(0, 0, CGRectGetWidth(shareJump.bounds), CGRectGetHeight(shareJump.bounds))];
    [shareJump setBackgroundImage:shareJumpImage forState:UIControlStateNormal];
    [shareJump addTarget:self action:@selector(shareJumpClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shareJump];
   
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
