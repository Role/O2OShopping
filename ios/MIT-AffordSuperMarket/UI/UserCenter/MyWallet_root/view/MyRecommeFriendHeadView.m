//
//  MyRecommeFriendHeadView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyRecommeFriendHeadView.h"

#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
#import "Global.h"
@interface MyRecommeFriendHeadView()
@property (nonatomic, strong)UILabel *friendNum;
@property (nonatomic, strong)UILabel *details;
@property (nonatomic, strong)UIButton *shareToFriend;
@end
@implementation MyRecommeFriendHeadView
-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if(self){
        [self setSubView];
        self.backgroundColor =[UIColor colorWithHexString:@"#343333"];
    }
    return self;
}
-(void)setSubViewDataWithFirendNum:(NSString *)firendNum
{
    NSString *firendNumText =[NSString stringWithFormat:@"%ld个 好友",(long)[firendNum integerValue]];
    CGSize firendNumSize =[firendNumText sizeWithFont:[UIFont systemFontOfSize:FontSize(50)] maxSize:CGSizeMake(VIEW_WIDTH, HightScalar(60))];
    _friendNum.frame= CGRectMake(30, 16, firendNumSize.width,  firendNumSize.height);
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:firendNumText];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize(50)] range:NSMakeRange(0, firendNum.length+1)];
    _friendNum.attributedText =str;
    
    NSString *detailsText =@"推荐的好友越多，奖励越多，轻松理财";
    CGSize detailsSize =[detailsText sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(20))];
    _details.frame = CGRectMake(CGRectGetMinX(_friendNum.frame), CGRectGetMaxY(_friendNum.frame)+5, detailsSize.width,  detailsSize.height);
    _details.text = detailsText;
    
    
}
-(void)shareToFriendClick
{
    if (_delegate && [_delegate respondsToSelector:@selector(shareToFriendJump)]) {
        [_delegate shareToFriendJump];
    }
}
#pragma mark --  初始化视图
-(void)setSubView
{
    //好友个数
    _friendNum = [[UILabel alloc]initWithFrame:CGRectZero];
    _friendNum.font = [UIFont systemFontOfSize:FontSize(16)];
    _friendNum.textColor = [UIColor whiteColor];
    _friendNum.text=@"0";
    [self addSubview:_friendNum];
    //描述
    _details = [[UILabel alloc]initWithFrame:CGRectZero];
    _details.font = [UIFont systemFontOfSize:FontSize(16)];
    _details.textColor = [UIColor whiteColor];
    [self addSubview:_details];
    
    //分享好友
    _shareToFriend=[UIButton buttonWithType:UIButtonTypeCustom];
    _shareToFriend.frame=CGRectMake(VIEW_WIDTH-90,30, 75, HightScalar(30));
    UIImage *goPayBackgroundImage = [UIImage createImageWithColor:[UIColor whiteColor]
                                                            frame:CGRectMake(0, 0, CGRectGetWidth(_shareToFriend.bounds), CGRectGetHeight(_shareToFriend.bounds))];
    [_shareToFriend setBackgroundImage:goPayBackgroundImage forState:UIControlStateNormal];
    [_shareToFriend setTitle:@"分享好友" forState:UIControlStateNormal];
    [_shareToFriend.layer setBorderWidth:0.5];
    [_shareToFriend.layer setBorderColor:[UIColor colorWithHexString:@"#000000"].CGColor];
    [_shareToFriend setTitleColor:[UIColor colorWithHexString:@"#000000"] forState:UIControlStateNormal];
    //    _goPay.titleLabel.textColor=[UIColor whiteColor];
    _shareToFriend.titleLabel.font=[UIFont systemFontOfSize:FontSize(15)];
    [_shareToFriend addTarget:self action:@selector(shareToFriendClick) forControlEvents:UIControlEventTouchUpInside];
    
    [_shareToFriend.layer setMasksToBounds:YES];
    [_shareToFriend.layer setCornerRadius:4.0];
    [self addSubview:_shareToFriend];

    
}


@end

