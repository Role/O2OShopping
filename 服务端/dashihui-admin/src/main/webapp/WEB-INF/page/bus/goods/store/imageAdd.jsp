<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#imagesForm").validate({
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 1:
						Kit.alert("请上传图片");return;
					case 2:
						Kit.alert("上传图片失败");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						onAddImagesSuccess(data.object);
						addImagesDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="imagesForm" action="${BASE_PATH}/bus/goods/store/doImageAdd" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">
		<label class="col-lg-2 control-label">图片</label>
		<div class="col-lg-9">
			<input type="file" id="thumb" name="thumb" accept="image/jpg" title="选择文件" data-show-caption="true">
		</div>
		<div class="col-lg-offset-2 p-t-5 col-lg-9"><span class="text-success">*用于商品详情页展示，最佳尺寸860*480</span></div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addImagesDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="goodsid" value="${goodsid}">
</form>