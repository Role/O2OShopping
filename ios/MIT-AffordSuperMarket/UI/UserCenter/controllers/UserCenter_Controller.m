//
//  UserCenter_Controller.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/9.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "UserCenter_Controller.h"
#define DATA_DIC_ICONS_KEY @"data_dic_icons_key"
#define DATA_DIC_TITLES_KEY @"data_dic_titles_key"
#define DATA_DIC_SUBTITLE_KEY @"data_dic_subtitle_key"
#define DATA_DIC_JIANTOU_KEY @"data_dic_jiantou_key"
#define DATA_DIC_BUTTON_TAG @"data_dic_button_tag"
//vendor
#import "Masonry.h"
#import "HYActivityView.h"
#import "UIImageView+AFNetworking.h"
#import "SendMessageToWXReq+requestWithTextOrMediaMessage.h"
#import "WXMediaMessage+messageConstruct.h"
#import "RDVTabBarController.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
#import "FMDBManager.h"
#import "AppDelegate.h"
#import "WXApi.h"
//views
#import "UserInfoShowView.h"
#import "UserCenter_MoreButtonCell.h"
//controllers
#import "Login_Controller.h"
#import "RegisterOneStepViewController.h"
#import "UserInfo_Controller.h"
#import "UserCenter_MarkAndHistoryController.h"
#import "MyOrderController.h"
#import "SettingViewController.h"
#import "UserCenter_FeedbackAndSuggestionController.h"
#import "ManageReceivedAddressController.h"
#import "StoreShowController.h"
#import "MTA.h"
#import "MyGoodsOrderController.h"
#import "MyGoodsOrderRefundController.h"
#import "MyWalletViewController.h"
#import "MemberAwardController.h"
@interface UserCenter_Controller ()
{
    /** 分享视图 */
    HYActivityView *_activityView;//更多弹出框
    ButtonView *_wxWeibo;//微信朋友圈
    ButtonView *_wxFriends;//微信好友
}
@property(nonatomic, strong)UserInfoShowView *headerView;//头部视图
@property(nonatomic, strong)UITableView *tableview;
@property (nonatomic,strong)NSMutableArray *listDatasArr;
@property (nonatomic,strong)NSMutableDictionary *userInfoDict;
@property(nonatomic, strong)NSMutableDictionary *orderStateDic;//订单状态信息
@property(nonatomic,strong)NSString *userLevel;
@end

@implementation UserCenter_Controller

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    self.title = @"我的";
    [self addRightNavItemWith:[UIImage imageNamed:@"setting_bg"]];
    _userInfoDict = [[NSMutableDictionary alloc]init];
    _listDatasArr = [[NSMutableArray alloc]init];
    _orderStateDic = [[NSMutableDictionary alloc]init];
    [self initDataSource];
    
    [self setupSubview];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setHeaderViewWithTransactionLonin];
    [self setHeaderViewWithHistoryCount];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"个人中心"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"个人中心"];
}
#pragma mark -- request Data
//获取用户的基本信息
- (void)loadUserBaseInfoData {
    __weak typeof(self)weakSelf = self;
    //请求参数说明： SIGNATURE  设备识别码
    UserInfoModel *model = [[ManagerGlobeUntil sharedManager]getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:model.token forKey:@"TOKEN"];
    [manager parameters:parameter customPOST:@"user/info" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                
                weakSelf.headerView.title = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"NICKNAME"]];
                _userLevel =[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"LEVEL"]];
                weakSelf.headerView.membersBtnTitle = [_userLevel isEqualToString:@"2"]?@"白金会员":@"普通用户";
                 weakSelf.headerView.CrownLogo.hidden = [_userLevel isEqualToString:@"2"]?NO:YES;
                if ([[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"AVATOR"]] length] != 0) {
                    [weakSelf.headerView.userImageView setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"AVATOR"]]]]
                                                      placeholderImage:[UIImage imageNamed:@"icon_headportrait"]];
                }
                
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
    } failure:^(bool isFailure) {
        [[ManagerGlobeUntil sharedManager] clearUserInfoData];
        weakSelf.headerView.title = @"登录/注册";
        [weakSelf.headerView showUserInfoWithLoginStatus:NO];
        _headerView.guanZhuCountText = @"0";
    }];
}
//查询用户订单状态查询
- (void)loadOrderStateData {
    __weak typeof(self)weakSelf = self;
    //请求参数说明： SIGNATURE  设备识别码
    UserInfoModel *userInfoMode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    XiaoQuAndStoreInfoModel *xiaoQuAndStoreModel = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:userInfoMode.token forKey:@"TOKEN"];
    [parameter setObject:xiaoQuAndStoreModel.storeID forKey:@"STOREID"];
    [parameter setObject:serviceVersionNumber forKey:@"APIVERSION"];
    [manager parameters:parameter customPOST:@"user/state" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([[dataDic objectForKey:@"OBJECT"] isKindOfClass:[NSDictionary class]]) {
                weakSelf.headerView.guanZhuCountText = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"COLLECTED"]];
            }
            
            [weakSelf.orderStateDic removeAllObjects];
            if ([state isEqualToString:@"0"]) {//请求成功
                [weakSelf.orderStateDic addEntriesFromDictionary:[dataDic objectForKey:@"OBJECT"]];
                [_tableview reloadData];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
       
        
    } failure:^(bool isFailure) {
        
    }];
}
#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.listDatasArr count];
}
- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *arr = [self.listDatasArr objectAtIndex:section];
    return [arr count];
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dataDic = [[self.listDatasArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if (indexPath.section == 0 || indexPath.section == 1) {//订单不同状态
        if (indexPath.row == 1) {
            static NSString *CellIdentifier = @"userCenterCell";
            UserCenter_MoreButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UserCenter_MoreButtonCell alloc]initWithStyle:UITableViewCellStyleDefault
                                                                titles:[dataDic objectForKey:DATA_DIC_TITLES_KEY]
                                                                images:[dataDic objectForKey:DATA_DIC_ICONS_KEY] tag: [dataDic objectForKey: DATA_DIC_BUTTON_TAG]
                                                       reuseIdentifier:CellIdentifier];
              
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.moreButtonCellDelegate = (id< UserCenter_MoreButtonCellDelegate>)self;
                
            }
            if (indexPath.section == 0) {
                 [cell updateBorderStatus:self.orderStateDic orderType:@"1"];
            } else if(indexPath.section ==1)
            {
                 [cell updateBorderStatus:self.orderStateDic orderType:@"0"];
            }
            return cell;
        } else {//我的订单 或 服务订单
            static NSString *OrderCellIdentifier = @"OrderCellIdentifier";
            UITableViewCell *orderCell = [tableView dequeueReusableCellWithIdentifier:OrderCellIdentifier];
            if (orderCell == nil) {
                orderCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:OrderCellIdentifier];
                orderCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                UILabel *subTitleLabe = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_WIDTH - 44 - 110, 0, 110, HightScalar(54))];
                subTitleLabe.backgroundColor = [UIColor clearColor];
                subTitleLabe.textAlignment = NSTextAlignmentRight;
                subTitleLabe.font = [UIFont systemFontOfSize:FontSize(14)];
                subTitleLabe.textColor=[UIColor colorWithHexString:@"#999999"];
                subTitleLabe.tag = 100;
                [orderCell.contentView addSubview:subTitleLabe];
                
            }
            
            orderCell.imageView.image = [[dataDic objectForKey:DATA_DIC_ICONS_KEY] firstObject];
            orderCell.textLabel.text = [[dataDic objectForKey:DATA_DIC_TITLES_KEY] firstObject];
            orderCell.textLabel.font=[UIFont systemFontOfSize:FontSize(16)];
            UILabel *subTitleLabel = [orderCell viewWithTag:100];
            subTitleLabel.text = [dataDic objectForKey:DATA_DIC_SUBTITLE_KEY];
            
            return orderCell;
        }
        
    }
        static NSString *CommonCellIdentifier = @"CommonCellIdentifier";
        UITableViewCell *commonCell = [tableView dequeueReusableCellWithIdentifier:CommonCellIdentifier];
        if (commonCell == nil) {
            commonCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CommonCellIdentifier];
            commonCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        commonCell.imageView.image = [[dataDic objectForKey:DATA_DIC_ICONS_KEY] firstObject];
        commonCell.textLabel.text = [[dataDic objectForKey:DATA_DIC_TITLES_KEY] firstObject];
         commonCell.textLabel.textColor=[UIColor colorWithHexString:@"#555555"];
       commonCell.textLabel.font=[UIFont systemFontOfSize:FontSize(16)];
        return commonCell;
}

#pragma mark -- UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 0) {//商品订单  全部订单
        if ([ManagerGlobeUntil sharedManager].transactionLogin) { //requestDataType  商品订单 是1 服务订单是 0
            MyGoodsOrderController *myGoodOrderVC = [[MyGoodsOrderController alloc]init];
            myGoodOrderVC.myGoodOrderType = kGoodWholeType;
            myGoodOrderVC.selectedIndex =0;
            [self.navigationController pushViewController:myGoodOrderVC animated:YES];
            
        } else {
            Login_Controller *loginVC = [[Login_Controller alloc]init];
            loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
            [self presentViewController:nav animated:YES completion:nil];
        }
        
    }else if(indexPath.section == 1 && indexPath.row == 0){//服务订单 全部订单
            if ([ManagerGlobeUntil sharedManager].transactionLogin) { //requestDataType  商品订单 是1 服务订单是 0
            MyOrderController *myOrderVC = [[MyOrderController alloc]init];
            myOrderVC.myOrderType = kallOrderType;
            myOrderVC.requestDataType=@"0";
            myOrderVC.homeMakeType =@"1";
            [self.navigationController pushViewController:myOrderVC animated:YES];

        } else {
            Login_Controller *loginVC = [[Login_Controller alloc]init];
            loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
            [self presentViewController:nav animated:YES completion:nil];
        }
        
    }else if (indexPath.section == 2 && indexPath.row == 0) {//我的钱包
        if ([ManagerGlobeUntil sharedManager].transactionLogin) { //requestDataType  商品订单 是1 服务订单是 0
            MyWalletViewController *Vc =[[MyWalletViewController alloc]init];
            Vc.userLevel =_userLevel;
            [self.navigationController pushViewController:Vc animated:YES];
        } else {
            Login_Controller *loginVC = [[Login_Controller alloc]init];
            loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
            [self presentViewController:nav animated:YES completion:nil];
        }
  
    }else if (indexPath.section == 3 && indexPath.row == 0) {//我的门店
        StoreShowController *storeShowVC = [[StoreShowController alloc]init];
        [self.navigationController pushViewController:storeShowVC animated:YES];
    } else if(indexPath.section ==4 && indexPath.row==0){//反馈建议
        if ([ManagerGlobeUntil sharedManager].transactionLogin) {
            UserCenter_FeedbackAndSuggestionController *FeedbackVc=[[UserCenter_FeedbackAndSuggestionController alloc]init];
            FeedbackVc.feedbackOrRemarksType=kFeedbackType;
            [self.navigationController pushViewController:FeedbackVc animated:YES];
        } else {
            Login_Controller *loginVC = [[Login_Controller alloc]init];
            loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
            [self presentViewController:nav animated:YES completion:nil];
 
        }
        
    } else if (indexPath.section == 4 && indexPath.row == 1) {//分享视图
        if (!_activityView) {
            _activityView = [[HYActivityView alloc]initWithTitle:@"更多" referView:self.view];
            _activityView.bgColor = [UIColor colorWithHexString:@"e9e9e9"];
            _activityView.numberOfButtonPerLine = 3;
            
            //横屏会变成一行6个, 竖屏无法一行同时显示6个, 会自动使用默认一行4个的设置
            [_activityView addButtonView:_wxWeibo];
            [_activityView addButtonView:_wxFriends];
            
           
        }
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate.tabBarController setTabBarHidden:YES animated:YES];
        [_activityView show];
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 || indexPath.section == 1) {//我的订单和服务订单状态cell高度
        if (indexPath.row == 1) {
            return HightScalar(75);
        }
    }
    return HightScalar(54);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if (section == 0) {
//        return 5;
//    }
    return HightScalar(12);
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 3) {
        return 0.5;
    }
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *views = [[UIView alloc]init];
    [views.layer setBorderWidth:0.7];
    [views.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    views.backgroundColor = [UIColor clearColor];
    return views;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *views = [[UIView alloc]init];
    [views.layer setBorderWidth:0.7];
    [views.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    views.backgroundColor = [UIColor clearColor];
    return views;
    
}

#pragma mark -- UserCenter_MoreButtonCellDelegate 待评价等按钮
 //requestDataType  商品订单 是1 服务订单是 0
- (void)selectMoreButtonCellItemWithIndex:(NSInteger)index {
    if ([ManagerGlobeUntil sharedManager].transactionLogin) {
        MyGoodsOrderController *myGoodOrderVC = [[MyGoodsOrderController alloc]init];

        MyOrderController *myOrderVC = [[MyOrderController alloc]init];
        if (index == 100) {//待付款
            myGoodOrderVC.myGoodOrderType = kGoodNoPayMoneyType;
            myGoodOrderVC.selectedIndex =1;
            [self.navigationController pushViewController:myGoodOrderVC animated:YES];
        } else if (index == 101) {//待收货
            myGoodOrderVC.myGoodOrderType = kGoodNoReceivingType;
            myGoodOrderVC.selectedIndex =2;
            [self.navigationController pushViewController:myGoodOrderVC animated:YES];
        } else if (index == 102) {//待评价
            myGoodOrderVC.myGoodOrderType = kGoodNoEvaluateType;
            myGoodOrderVC.selectedIndex =3;
            [self.navigationController pushViewController:myGoodOrderVC animated:YES];
        }else if (index == 106) {//退款
            MyGoodsOrderRefundController *myGoodRefundOrderVC = [[MyGoodsOrderRefundController alloc]init];
            [self.navigationController pushViewController:myGoodRefundOrderVC animated:YES];

        }else if(index==103){//服务订单 三个按钮//待付款
            myOrderVC.requestDataType=@"0";
            myOrderVC.myOrderType = kwaitPayMoneyType;
             myOrderVC.homeMakeType =@"1";
         [self.navigationController pushViewController:myOrderVC animated:YES];
        }else if (index==104) {//待服务
            myOrderVC.requestDataType=@"0";
            myOrderVC.myOrderType = kwaitSendProductType;
            myOrderVC.homeMakeType =@"1";
         [self.navigationController pushViewController:myOrderVC animated:YES];
        }else{//待评价
            myOrderVC.requestDataType=@"0";
            myOrderVC.myOrderType = kwaitGetProductType;
            myOrderVC.homeMakeType =@"1";
        [self.navigationController pushViewController:myOrderVC animated:YES];
        }
        
    } else {
        Login_Controller *loginVC = [[Login_Controller alloc]init];
        loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [self presentViewController:nav animated:YES completion:nil];
    }

   
    
}
#pragma mark -- UserInfoShowViewDelegate
- (void)userUnLogin {
    Login_Controller *loginVC = [[Login_Controller alloc]init];
    loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:nil];
}
- (void)manageUserInfo {
    UserInfo_Controller *userInfoVC = [[UserInfo_Controller alloc]init];
    userInfoVC.userImage = self.headerView.userImageView.image;
    userInfoVC.userName = self.headerView.title;
    userInfoVC.userPhoneNum = self.headerView.title;
    [self.navigationController pushViewController:userInfoVC animated:YES];
}
-(void)membersJump
{
    MemberAwardController *memberAwardVC = [[MemberAwardController alloc]init];
    [self.navigationController pushViewController:memberAwardVC animated:YES];
}
- (void)manageUserRecivedAddress {
    ManageReceivedAddressController *manageReceiveAddressVC = [[ManageReceivedAddressController alloc]init];
    manageReceiveAddressVC.supperControllerType = kUserCenterPushToManageReceivedAddressController;
    [self.navigationController pushViewController:manageReceiveAddressVC animated:YES];
}

#pragma mark --查看关注页面
- (void)lookGuanZhun {
    if ([ManagerGlobeUntil sharedManager].transactionLogin) {
        UserCenter_MarkAndHistoryController *markVC = [[UserCenter_MarkAndHistoryController alloc]init];
        markVC.navItemTitle = @"我的关注";
        markVC.markOrHistoyType = kmarkType;
        
        [self.navigationController pushViewController:markVC animated:YES];
    } else {//跳转登录页面
        Login_Controller *loginVC = [[Login_Controller alloc]init];
        loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [self presentViewController:nav animated:YES completion:nil];
    }
}
#pragma mark -- 查看浏览历史页面
- (void)lookLiuLanHistory {
    if ([ManagerGlobeUntil sharedManager].transactionLogin) {
        UserCenter_MarkAndHistoryController *historyVC = [[UserCenter_MarkAndHistoryController alloc]init];
        historyVC.navItemTitle = @"浏览历史";
        historyVC.markOrHistoyType = khistoryType;
        [self.navigationController pushViewController:historyVC animated:YES];
    } else {//跳转登录页面
        Login_Controller *loginVC = [[Login_Controller alloc]init];
        loginVC.supperControllerType = kUserCenterPushToLogin_ControllerSupperController;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [self presentViewController:nav animated:YES completion:nil];
    }

   
}
#pragma mark -- 初始化数据
- (void)initDataSource {
    //section-0
    NSMutableArray *sectionArr = [[NSMutableArray alloc]init];
    //我的订单
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@[[UIImage imageNamed:@"icon_myorder"]] forKey:DATA_DIC_ICONS_KEY];
    [dic setObject:@[@"我的订单"] forKey:DATA_DIC_TITLES_KEY];
    [dic setObject:@"查看全部订单" forKey:DATA_DIC_SUBTITLE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"" forKey:DATA_DIC_BUTTON_TAG];
    [sectionArr addObject:dic];
    
    //我的订单状态
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@[[UIImage imageNamed:@"icon_gopay"],[UIImage imageNamed:@"icon_goodsreceipt"],[UIImage imageNamed:@"icon_evaluation"],[UIImage imageNamed:@"btn_refund"]] forKey:DATA_DIC_ICONS_KEY];
    [dic setObject:@[@"待付款",@"待发货",@"待评价",@"退款"] forKey:DATA_DIC_TITLES_KEY];
    [dic setObject:@"" forKey:DATA_DIC_SUBTITLE_KEY];
    [dic setObject:@"NO" forKey:DATA_DIC_JIANTOU_KEY];
    NSArray* arrayone=[[NSArray alloc]initWithObjects:@"100",@"101",@"102",@"106" ,nil];
    [dic setObject:arrayone forKey:DATA_DIC_BUTTON_TAG];
    [sectionArr addObject:dic];
    [_listDatasArr addObject:sectionArr];
    
    //section - 1
    sectionArr = [[NSMutableArray alloc]init];
    //服务订单
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@[[UIImage imageNamed:@"icon_order"]] forKey:DATA_DIC_ICONS_KEY];
    [dic setObject:@[@"服务订单"] forKey:DATA_DIC_TITLES_KEY];
    [dic setObject:@"查看全部订单" forKey:DATA_DIC_SUBTITLE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"" forKey:DATA_DIC_BUTTON_TAG];
    [sectionArr addObject:dic];
    
    //服务订单状态
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@[[UIImage imageNamed:@"icon_gopay"],[UIImage imageNamed:@"icon_service"],[UIImage imageNamed:@"btn_finished"]] forKey:DATA_DIC_ICONS_KEY];
    [dic setObject:@[@"待付款",@"待服务",@"已完成"] forKey:DATA_DIC_TITLES_KEY];
    [dic setObject:@"" forKey:DATA_DIC_SUBTITLE_KEY];
    NSArray* arraytwo=[[NSArray alloc]initWithObjects:@"103",@"104",@"105", nil];
    [dic setObject:@"NO" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:arraytwo forKey:DATA_DIC_BUTTON_TAG];
    [sectionArr addObject:dic];
    [_listDatasArr addObject:sectionArr];
    //section - 2

    //我的钱包
    sectionArr = [[NSMutableArray alloc]init];
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@[[UIImage imageNamed:@"img_mywallet"]] forKey:DATA_DIC_ICONS_KEY];
    [dic setObject:@[@"我的钱包"] forKey:DATA_DIC_TITLES_KEY];
    [dic setObject:@"" forKey:DATA_DIC_SUBTITLE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"" forKey:DATA_DIC_BUTTON_TAG];
    [sectionArr addObject:dic];
    [_listDatasArr addObject:sectionArr];

    //section - 3
      //我的门店
    sectionArr = [[NSMutableArray alloc]init];
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@[[UIImage imageNamed:@"Myaccount_icon_store"]] forKey:DATA_DIC_ICONS_KEY];
    [dic setObject:@[@"我的门店"] forKey:DATA_DIC_TITLES_KEY];
    [dic setObject:@"" forKey:DATA_DIC_SUBTITLE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"" forKey:DATA_DIC_BUTTON_TAG];
    [sectionArr addObject:dic];
    [_listDatasArr addObject:sectionArr];
    
     //section - 3
    //反馈建议
    sectionArr = [[NSMutableArray alloc]init];
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@[[UIImage imageNamed:@"icon_advice"]] forKey:DATA_DIC_ICONS_KEY];
    [dic setObject:@[@"反馈与建议"] forKey:DATA_DIC_TITLES_KEY];
    [dic setObject:@"" forKey:DATA_DIC_SUBTITLE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"" forKey:DATA_DIC_BUTTON_TAG];
    [sectionArr addObject:dic];
    //把大实惠告诉朋友
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@[[UIImage imageNamed:@"icon_share"]] forKey:DATA_DIC_ICONS_KEY];
    [dic setObject:@[@"把大实惠告诉朋友"] forKey:DATA_DIC_TITLES_KEY];
    [dic setObject:@"" forKey:DATA_DIC_SUBTITLE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"" forKey:DATA_DIC_BUTTON_TAG];
    [sectionArr addObject:dic];
     [_listDatasArr addObject:sectionArr];
}
#pragma  mark--获取历史总个数
- (void)setHeaderViewWithHistoryCount
{
    //获取历史总个数
    XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSString *shopID=[NSString stringISNull:mode.storeID];
    NSArray *numary=[[FMDBManager sharedManager]HistoryQueryDataWithShopID:shopID];
    self.headerView.historyCountText=[NSString stringWithFormat:@"%lu",(unsigned long)numary.count];
    
}
#pragma mark -- //根据登录状态 切换头部信息
- (void)setHeaderViewWithTransactionLonin
{
    if ([ManagerGlobeUntil sharedManager].transactionLogin) {
        [self.headerView showUserInfoWithLoginStatus:YES];
        [self loadUserBaseInfoData];
        
        [self loadOrderStateData];
        
    } else {
        self.headerView.title = @"登录/注册";
        [self.headerView showUserInfoWithLoginStatus:NO];
        _headerView.guanZhuCountText = @"";
        [self.orderStateDic removeAllObjects];
        [self.tableview reloadData];
    }
}
#pragma mark -- 初始化视图
- (void)setupSubview {
    [self setupHeaderView];
    [self setupTableView];
    [self setupShareView];
}
//头部视图
- (void)setupHeaderView {
    _headerView = [[UserInfoShowView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), HightScalar(180))];
    _headerView.delegate = (id<UserInfoShowViewDelegate>)self;
    _headerView.backImage=[UIImage imageNamed:@"bg_head_userconter"];
    _headerView.userImageView.image = [UIImage imageNamed:@"icon_headportrait"];
    _headerView.title = @"登录/注册";
}
//列表视图
- (void)setupTableView {
    _tableview = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableview.frame = CGRectMake(0, 0, self.view.bounds.size.width , self.view.bounds.size.height - 64 - 49);
    _tableview.delegate = (id<UITableViewDelegate>)self;
    _tableview.dataSource = (id<UITableViewDataSource>)self;
    _tableview.backgroundColor = [UIColor colorWithHexString:@"#f3f3f3"];
    _tableview.showsVerticalScrollIndicator = NO;
    _tableview.tableHeaderView = self.headerView;
    [self.view addSubview:_tableview];

}

- (void)setupShareView {
    /*********************************************微信朋友圈分享****************************************/
    _wxWeibo = [[ButtonView alloc]initWithText:@"微信朋友圈" image:[UIImage imageNamed:@"friendsicon@2x"] handler:^(ButtonView *buttonView){
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate.tabBarController setTabBarHidden:NO animated:NO];
        NSLog(@"微信朋友圈");
        //网页数据
        if ([WXApi isWXAppInstalled]) {//已安装微信
            WXWebpageObject *ext = [WXWebpageObject object];
            ext.webpageUrl = @"http://91dashihui.com/share/";
            WXMediaMessage *message = [WXMediaMessage messageWithTitle:@"下载大实惠APP，即享蔬菜水果、酒水饮料、生活百货0元配送，25分钟到家，快来试试吧~"
                                                           Description:@"大惠云商服务平台是您家门口的社区E站，不仅提供蔬菜水果、酒水饮料、生活百货、粮油调料、营养早餐的在线购买和线下配送服务，还为小区业主提供家政、洗衣、丽人、洗车、开锁、维修等便捷的到家服务。旨在开启全新的社区便利生活！"
                                                                Object:ext
                                                            MessageExt:nil
                                                         MessageAction:nil
                                                            ThumbImage:[UIImage imageNamed:@"icon-60"]
                                                              MediaTag:@""];
            
            SendMessageToWXReq* req = [SendMessageToWXReq requestWithText:nil
                                                           OrMediaMessage:message
                                                                    bText:NO
                                                                  InScene:WXSceneTimeline];
            [WXApi sendReq:req];
        } else {//未安装微信
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"您还未安装微信" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
        }
        
    }];
    [_wxWeibo.imageButton setImage:[UIImage imageNamed:@"friendsiconpressdown@2x"] forState:UIControlStateHighlighted];
    
    
    /*********************************************微信好友分享****************************************/
    
    _wxFriends = [[ButtonView alloc]initWithText:@"微信好友" image:[UIImage imageNamed:@"weixinicon@2x"] handler:^(ButtonView *buttonView){
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate.tabBarController setTabBarHidden:NO animated:NO];
        if ([WXApi isWXAppInstalled]) {//已安装微信
            WXWebpageObject *ext = [WXWebpageObject object];
            ext.webpageUrl = @"http://91dashihui.com/share/";
            WXMediaMessage *message = [WXMediaMessage messageWithTitle:@"下载大实惠APP，即享蔬菜水果、酒水饮料、生活百货0元配送，25分钟到家，快来试试吧~"
                                                           Description:@"大惠云商服务平台是您家门口的社区E站，不仅提供蔬菜水果、酒水饮料、生活百货、粮油调料、营养早餐的在线购买和线下配送服务，还为小区业主提供家政、洗衣、丽人、洗车、开锁、维修等便捷的到家服务。旨在开启全新的社区便利生活！"
                                                                Object:ext
                                                            MessageExt:nil
                                                         MessageAction:nil
                                                            ThumbImage:[UIImage imageNamed:@"icon-60"]
                                                              MediaTag:@""];
            
            SendMessageToWXReq* req = [SendMessageToWXReq requestWithText:nil
                                                           OrMediaMessage:message
                                                                    bText:NO
                                                                  InScene:WXSceneSession];
            [WXApi sendReq:req];
        } else {//未安装微信
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"您还未安装微信" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
        }
       
        
    }];
    
    [_wxFriends.imageButton setImage:[UIImage imageNamed:@"weixiniconpressdown@2x"] forState:UIControlStateHighlighted];
}

#pragma mark -- button Action
- (void)rigthBtnAction:(id)sender {
    SettingViewController *settingVC = [[SettingViewController alloc]init];
    [self.navigationController pushViewController:settingVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
