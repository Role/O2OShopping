//
//  MyGoodsOrderCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/3.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MyGoodsOrderCellDelegate<NSObject>
//去支付 或 再次购买
-(void)selectedFirstItemWithTag:(NSInteger)tag title:(NSString *)title;
//删除订单
-(void)selectedSecondItemWithTag:(NSInteger)tag title:(NSString *)title;
@end

@interface MyGoodsOrderCell : UITableViewCell

@property(nonatomic,unsafe_unretained)id<MyGoodsOrderCellDelegate>delegate;
- (void)updateViewwithData:(NSDictionary*)dataDic;
// 删除订单
@property (nonatomic,strong)UIButton *deleteOrder;
//去支付
@property (nonatomic,strong)UIButton *goPay;
@property (nonatomic,strong)UIView *views;
@end
