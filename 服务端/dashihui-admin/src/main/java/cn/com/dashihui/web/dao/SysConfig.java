package cn.com.dashihui.web.dao;

import com.jfinal.plugin.activerecord.Model;

public class SysConfig extends Model<SysConfig>{
	private static final long serialVersionUID = 1L;
	private static SysConfig me = new SysConfig();
	public static SysConfig me(){
		return me;
	}
	
	public String getTrueName(){
		return getStr("trueName");
	}
}
