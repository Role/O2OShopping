<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		rules: {
			marketPrice: {isMoney: true},
			sellPrice: {isMoney: true},
			urv: {digits: true}
		},
		messages:{
			name: {required: "请输入商品标题"},
			marketPrice: {required: "请输入商品市场价",isMoney: "请填写正确的金额，最多保留两位小数位"},
			sellPrice: {required: "请输入商品销售价",isMoney: "请填写正确的金额，最多保留两位小数位"},
			urv: {required: "请填写限购量",digits: "请填写正确的数量",maxlength: "长度不能超过3位"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 2:
						Kit.alert("请选择商品分类");return;
					case 6:
						Kit.alert("请填写限购量");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						onEditSuccess(data.object);
						editDialog.close();
						return;
					}
				}
			});
		}
	});
});
function onTypeSelected(v){
	if(v==3){
		//恢复备份的销售价格
		var sellPriceOld = $("#sellPrice").data("old");
		var sellPrice = $("#sellPrice").val();
		if(sellPrice==1){
			$("#sellPrice").val(sellPriceOld);
		}
		$("#sellPrice").data("old",sellPriceBak);
		$("#sellPrice").prop("readonly",false);
		$(".onType34").show();
		$("#urv").rules("add",{required:true});
	}else if(v==4){
		//备份销售价格
		var sellPriceBak = $("#sellPrice").val();
		var sellPriceOld = $("#sellPrice").data("old");
		$("#sellPrice").data("old",sellPriceBak);
		//修改销售价格为1元
		$("#sellPrice").val(1).prop("readonly",true);
		$(".onType34").show();
		$("#urv").rules("add",{required:true});
	}else{
		//恢复备份的销售价格
		var sellPriceOld = $("#sellPrice").data("old");
		var sellPrice = $("#sellPrice").val();
		if(sellPrice==1){
			$("#sellPrice").val(sellPriceOld);
		}
		$("#sellPrice").prop("readonly",false);
		$(".onType34").hide();
		$("#urv").rules("remove");
	}
}
</script>
<form id="editForm" action="${BASE_PATH}/goods/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">标题</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" class="form-control" placeholder="请输入商品标题" value="${object.name}" required maxlength="100">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">分类</label>
	    <div class="col-lg-9">
    		<select id="categoryonid" name="categoryonid" class="selectpicker" data-width="24%" data-url="${BASE_PATH}/api/category" data-val="${object.categoryonid}" data-isfirst="true" data-next="#categorytwid" data-key="id:name"></select>
			<select id="categorytwid" name="categorytwid" class="selectpicker" data-width="24%" data-url="${BASE_PATH}/api/category/{value}" data-val="${object.categorytwid}" data-next="#categorythid" data-key="id:name"></select>
			<select id="categorythid" name="categorythid" class="selectpicker" data-width="24%" data-url="${BASE_PATH}/api/category/{value}" data-val="${object.categorythid}" data-next="#categoryfoid" data-key="id:name"></select>
			<select id="categoryfoid" name="categoryfoid" class="selectpicker" data-width="24%" data-url="${BASE_PATH}/api/category/{value}" data-val="${object.categoryfoid}" data-key="id:name"></select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">是否上架</label>
	    <div class="col-lg-9">
	        <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="state" value="1" <c:if test="${object.state==1}">checked</c:if>> 上架</li>
	    		<li><input type="radio" class="iCheck" name="state" value="2" <c:if test="${object.state==2}">checked</c:if>> 下架</li>
	    	</ul>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">品牌</label>
	    <div class="col-lg-9">
    		<select name="brandid" class="selectpicker form-control" data-url="${BASE_PATH}/api/brand" data-issingle="true" data-val="${object.brandid}" data-key="id:name"></select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">规格</label>
	    <div class="col-lg-9">
	        <input type="text" name="spec" class="form-control" placeholder="请输入商品规格" value="${object.spec}" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">描述</label>
	    <div class="col-lg-9">
        	<textarea name="shortInfo" class="form-control" placeholder="请输入商品短简介" maxlength="120">${object.shortInfo}</textarea>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">市场价</label>
	    <div class="col-lg-9">
        	<input type="text" name="marketPrice" class="form-control" placeholder="请输入商品市场价" value="${object.marketPrice}" required>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">销售价</label>
	    <div class="col-lg-9">
        	<input type="text" id="sellPrice" name="sellPrice" class="form-control" placeholder="请输入商品销售价" value="${object.sellPrice}" required>
	    </div>
	</div>
	<c:if test="${tagList!=null&&fn:length(tagList)!=0}">
	<div class="form-group">
	    <label class="col-lg-2 control-label">标签</label>
	    <div class="col-lg-9">
	    	<ul class="iCheckList">
	    		<c:forEach items="${tagList}" var="tag">
	    		<li><input type="checkbox" class="iCheck" name="tag" value="${tag.id}" <c:if test="${tag.checked==1}">checked</c:if>> ${tag.tagName}</li>
	    		</c:forEach>
	    	</ul>
	    </div>
	</div>
	</c:if>
	<div class="form-group">
	    <label class="col-lg-2 control-label">类型</label>
	    <div class="col-lg-9">
	        <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="type" value="1" data-callback="onTypeSelected" <c:if test="${object.type==1}">checked</c:if>> 普通</li>
	    		<li><input type="radio" class="iCheck" name="type" value="2" data-callback="onTypeSelected" <c:if test="${object.type==2}">checked</c:if>> 推荐</li>
	    		<li><input type="radio" class="iCheck" name="type" value="3" data-callback="onTypeSelected" <c:if test="${object.type==3}">checked</c:if>> 限量</li>
	    		<li><input type="radio" class="iCheck" name="type" value="4" data-callback="onTypeSelected" <c:if test="${object.type==4}">checked</c:if>> 一元购</li>
	    	</ul>
	    </div>
	</div>
	<div class="onType34 form-group" style="display:none;">
	    <label class="col-lg-2 control-label">限购量</label>
	    <div class="col-lg-9">
			<input type="text" class="form-control" id="urv" name="urv" placeholder="请输入限购量" value="${object.urv}" maxlength="3">
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left"  type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="goodsid" value="${object.id}">
</form>