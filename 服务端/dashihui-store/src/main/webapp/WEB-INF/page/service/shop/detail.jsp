<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">信息</a></li>
	<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">图片</a></li>
</ul>
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active container-fluid" id="tab1" style="padding-top:15px;min-height:250px;">
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label class="text-label">名称：</label>
		   </div>
		   <div class="col-lg-10">
			    <div class="text-content">${object.name}</div>
			</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label class="text-label">地址：</label>
		   </div>
		   <div class="col-lg-10">
			    <div class="text-content">${object.address}</div>
			</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label class="text-label">电话：</label>
		   </div>
		   <div class="col-lg-10">
			    <div class="text-content">${object.tel}</div>
			</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label class="text-label">简介：</label>
		   </div>
		   <div class="col-lg-10">
			    <div class="text-content">${object.comment}</div>
			</div>
		</div>
		<hr class="hr-line"/>
	</div>
	<div role="tabpanel" class="tab-pane container-fluid" id="tab2" style="padding-top:15px;min-height:250px;">
		<div class="row">
		   <div class="col-lg-12">
			    <div class="thumbnail">
					<a href="#"><img src="${FTP_PATH}${object.thumb}"/></a>
				</div>
			</div>
		</div>
	</div>
</div>