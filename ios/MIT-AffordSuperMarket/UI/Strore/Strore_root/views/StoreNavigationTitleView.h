//
//  StoreNavigationTitleView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/22.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： StoreNavigationTitleView
 Created_Date： 20160422
 Created_People： GT
 Function_description： 便利店导航栏标题视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol StoreNavigationTitleViewDelegate <NSObject>
- (void)beatNavigationTitleViewToSearchView;
@end
@interface StoreNavigationTitleView : UIView
@property(nonatomic, unsafe_unretained)id<StoreNavigationTitleViewDelegate>delegate;
@end
