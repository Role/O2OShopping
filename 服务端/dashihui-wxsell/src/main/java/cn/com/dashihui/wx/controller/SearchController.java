package cn.com.dashihui.wx.controller;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.wx.dao.User;
import cn.com.dashihui.wx.kit.LuceneKit;
import cn.com.dashihui.wx.service.SearchService;

public class SearchController extends BaseController{
	private SearchService service = new SearchService();
	
	/**
	 * 跳转至搜索页面
	 */
	public void index(){
		//查询指定数量的关键字列表
		setAttr("keywords",service.findAllKeyword(getCurrentStoreid(),10));
		setAttr("keyword",getPara("keyword"));
		render("index.jsp");
	}
	
	/**
	 * 根据关键字分页搜索商品列表
	 * @param keywordid 关键字ID
	 * @param keyword 关键字
	 * @param pageNum 页码
	 * @param pageSize 数量
	 */
	public void page(){
		int isSelf = getParaToInt("isSelf");
		int orderBy = getParaToInt("orderBy");
		String keywordidStr = getPara("keywordid");
		String keyword = getPara("keyword");
		int pageNum = getParaToInt("pageNum",1);
		int pageSize = getParaToInt("pageSize",PropKit.getInt("constants.pageSize"));
		if(StrKit.isBlank(keyword)){
    		renderFailed("参数KEYWORD不能为空");
    		return;
		}else{
			if(!StrKit.isBlank(keywordidStr)){
				//如果传的有关键字ID参数，则说明用户是点击系统推荐的关键字进行的搜索，此时要修改对应关键字点击量加1
				service.updateKeywordAmount(Integer.valueOf(keywordidStr));
			}else{
				//相反则为用户自定义搜索关键字，此时要进行记录，如果用户登录了，则同时记录用户ID
				User user = getCurrentUser();
				service.logKeyword(getCurrentStoreid(),user==null?0:user.getInt("id"), keyword);
			}
			renderSuccess(LuceneKit.search(getCurrentStoreid(),isSelf, keyword, pageNum, pageSize, orderBy));
		}
	}
}
