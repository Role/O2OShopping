//
//  UserCenter_ListCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/9.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "UserCenter_ListCell.h"
#import "Global.h"
//vendor
#import "Masonry.h"
//untils
#import "UIColor+Hex.h"
@implementation UserCenter_ListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubview];
    }
    return self;
}

#pragma mark -- setupSubview
- (void)setupSubview {
    //背景图片
    _backImgView = [[UIImageView alloc]init];
    _backImgView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_backImgView];
    
    //分割线
    _lineImgView = [[UIImageView alloc]init];
    _lineImgView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_lineImgView];
    
    //指示箭头
    _jiantouImgView = [[UIImageView alloc]init];
    _jiantouImgView.backgroundColor = [UIColor clearColor];
    _jiantouImgView.image = [UIImage imageNamed:@"userCenter_jiantou"];
    [self.contentView addSubview:_jiantouImgView];
    
    //图片
    _iconImgView = [[UIImageView alloc]init];
    _iconImgView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_iconImgView];
    
    //标题
    _contentTextLabel = [[UILabel alloc]init];
    _contentTextLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    _contentTextLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    _contentTextLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_contentTextLabel];
    
    //设置约束
    [_backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.mas_equalTo(0);
        make.right.and.bottom.mas_equalTo(0);
    }];
    [_lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(-0.5);
        make.right.mas_equalTo(-10);
    }];
    [_jiantouImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.right.mas_equalTo(-20);
        make.width.mas_equalTo(7);
        make.height.mas_equalTo(12);
    }];
    [_iconImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.left.mas_equalTo(15);
        make.width.and.height.mas_equalTo(20);
    }];
    
    [_contentTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.left.equalTo(_iconImgView.mas_right).offset(15);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(-10);
    }];
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
