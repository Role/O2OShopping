//
//  Strort_BreakFast_RightCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/25.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

//添加代理，用于按钮加减的实现
@protocol Strort_Breakfast_RightCellDelegate <NSObject>

- (void)selectedItemAtIndexPath:(NSIndexPath *)indexPath tag:(NSInteger )tag;
@end


@interface Strort_Breakfast_RightCell : UITableViewCell

-(void)retSubViewWithData:(NSDictionary *)dictionary;

//Quantity 商品数量
@property (retain,nonatomic)UILabel* productQuantity;

@property(nonatomic, strong)NSIndexPath *indexPath;
@property(assign,nonatomic)id<Strort_Breakfast_RightCellDelegate>delegate;
@end
