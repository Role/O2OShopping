//
//  Strore_detailController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Strore_detailController
 Created_Date： 20151106
 Created_People： JSQ
 Function_description： 宝贝详情页面
 ***************************************/

#import "BaseViewController.h"

@interface Strore_detailController : BaseViewController

@property(nonatomic,strong)NSString *goodsID;
@property(nonatomic,strong)NSString *isSelf;
@end
