//
//  Service_listCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Service_listCell : UITableViewCell
-(void)setSuberViewDataWithDictionary:(NSDictionary *)dictionary;
@end
