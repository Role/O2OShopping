//
//  ReceivedAddressOBJECT.m
//
//  Created by apple  on 16/1/25
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ReceivedAddressOBJECT.h"


NSString *const kReceivedAddressOBJECTLINKNAME = @"LINKNAME";
NSString *const kReceivedAddressOBJECTSTOREID = @"STOREID";
NSString *const kReceivedAddressOBJECTADDRESS = @"ADDRESS";
NSString *const kReceivedAddressOBJECTTEL = @"TEL";
NSString *const kReceivedAddressOBJECTSEX = @"SEX";
NSString *const kReceivedAddressOBJECTISDEFAULT = @"ISDEFAULT";
NSString *const kReceivedAddressOBJECTSTORETITLE = @"STORETITLE";
NSString *const kReceivedAddressOBJECTID = @"ID";


@interface ReceivedAddressOBJECT ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ReceivedAddressOBJECT

@synthesize lINKNAME = _lINKNAME;
@synthesize sTOREID = _sTOREID;
@synthesize aDDRESS = _aDDRESS;
@synthesize tEL = _tEL;
@synthesize sEX = _sEX;
@synthesize iSDEFAULT = _iSDEFAULT;
@synthesize sTORETITLE = _sTORETITLE;
@synthesize iDProperty = _iDProperty;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.lINKNAME = [self objectOrNilForKey:kReceivedAddressOBJECTLINKNAME fromDictionary:dict];
            self.sTOREID = [[self objectOrNilForKey:kReceivedAddressOBJECTSTOREID fromDictionary:dict] doubleValue];
            self.aDDRESS = [self objectOrNilForKey:kReceivedAddressOBJECTADDRESS fromDictionary:dict];
            self.tEL = [self objectOrNilForKey:kReceivedAddressOBJECTTEL fromDictionary:dict];
            self.sEX = [[self objectOrNilForKey:kReceivedAddressOBJECTSEX fromDictionary:dict] doubleValue];
            self.iSDEFAULT = [[self objectOrNilForKey:kReceivedAddressOBJECTISDEFAULT fromDictionary:dict] doubleValue];
            self.sTORETITLE = [self objectOrNilForKey:kReceivedAddressOBJECTSTORETITLE fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kReceivedAddressOBJECTID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.lINKNAME forKey:kReceivedAddressOBJECTLINKNAME];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sTOREID] forKey:kReceivedAddressOBJECTSTOREID];
    [mutableDict setValue:self.aDDRESS forKey:kReceivedAddressOBJECTADDRESS];
    [mutableDict setValue:self.tEL forKey:kReceivedAddressOBJECTTEL];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sEX] forKey:kReceivedAddressOBJECTSEX];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iSDEFAULT] forKey:kReceivedAddressOBJECTISDEFAULT];
    [mutableDict setValue:self.sTORETITLE forKey:kReceivedAddressOBJECTSTORETITLE];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kReceivedAddressOBJECTID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.lINKNAME = [aDecoder decodeObjectForKey:kReceivedAddressOBJECTLINKNAME];
    self.sTOREID = [aDecoder decodeDoubleForKey:kReceivedAddressOBJECTSTOREID];
    self.aDDRESS = [aDecoder decodeObjectForKey:kReceivedAddressOBJECTADDRESS];
    self.tEL = [aDecoder decodeObjectForKey:kReceivedAddressOBJECTTEL];
    self.sEX = [aDecoder decodeDoubleForKey:kReceivedAddressOBJECTSEX];
    self.iSDEFAULT = [aDecoder decodeDoubleForKey:kReceivedAddressOBJECTISDEFAULT];
    self.sTORETITLE = [aDecoder decodeObjectForKey:kReceivedAddressOBJECTSTORETITLE];
    self.iDProperty = [aDecoder decodeDoubleForKey:kReceivedAddressOBJECTID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_lINKNAME forKey:kReceivedAddressOBJECTLINKNAME];
    [aCoder encodeDouble:_sTOREID forKey:kReceivedAddressOBJECTSTOREID];
    [aCoder encodeObject:_aDDRESS forKey:kReceivedAddressOBJECTADDRESS];
    [aCoder encodeObject:_tEL forKey:kReceivedAddressOBJECTTEL];
    [aCoder encodeDouble:_sEX forKey:kReceivedAddressOBJECTSEX];
    [aCoder encodeDouble:_iSDEFAULT forKey:kReceivedAddressOBJECTISDEFAULT];
    [aCoder encodeObject:_sTORETITLE forKey:kReceivedAddressOBJECTSTORETITLE];
    [aCoder encodeDouble:_iDProperty forKey:kReceivedAddressOBJECTID];
}

- (id)copyWithZone:(NSZone *)zone
{
    ReceivedAddressOBJECT *copy = [[ReceivedAddressOBJECT alloc] init];
    
    if (copy) {

        copy.lINKNAME = [self.lINKNAME copyWithZone:zone];
        copy.sTOREID = self.sTOREID;
        copy.aDDRESS = [self.aDDRESS copyWithZone:zone];
        copy.tEL = [self.tEL copyWithZone:zone];
        copy.sEX = self.sEX;
        copy.iSDEFAULT = self.iSDEFAULT;
        copy.sTORETITLE = [self.sTORETITLE copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
    }
    
    return copy;
}


@end
