<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">成交情况</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-4 col-lg-8">
				<label class="search-label">日期：</label><input type="text" id="sBeginDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" data-end-date="${today}" maxlength="10">
				<label class="search-label">至</label><input type="text" id="sEndDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" data-end-date="${today}" maxlength="10">
				<button type="button" class="btn btn-success pull-left m-l-5" onclick="doSearch()">查询</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div><p></p></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="table-responsive">
					<table id="dataTable" class="table table-bordered">
			            <thead>
			                <tr>
			                	<th width="10%">日期</th>
								<th width="30%">成交金额</th>
								<th width="30%">成交笔数</th>
								<th width="30%">成交客户数</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
		{{if item.date}}
		<tr>
			<td>{{item.date}}</td>
			<td>{{item.amount}}</td>
			<td>{{item.orderCount}}</td>
			<td>{{item.userCount}}</td>
		</tr>
		{{else}}
		<tr>
			<td>合计</td>
			<td>{{item.amount}}</td>
			<td>{{item.orderCount}}</td>
			<td>{{item.userCount}}</td>
		</tr>
		{{/if}}
	{{/each}}
</script>
<script type="text/javascript">
$(doSearch);
function doSearch(){
	var params = {
		pageSize:10,
		beginDate:$("#sBeginDate").val(),
		endDate:$("#sEndDate").val()
	};
	$.getJSON("${BASE_PATH}/report/orderCountPage",params,function(result){
		if(result.flag==0){
			//根据模板渲染数据并填充
			$("#dataList").empty().append(template("dataTpl",{"list":result.object}));
		}
	});
}
</script>
</body>
</html>