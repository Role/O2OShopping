//
//  MyOrderModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOrderModel : NSObject
//订单状态
@property (nonatomic,strong)NSString *orderState;
//商品logo
@property (nonatomic,strong)NSString *goodsImage;
//商品名称
@property (nonatomic,strong)NSString *goodsName;
//实际付款价格
@property (nonatomic,strong)NSString *goodsMoney;

@property (nonatomic,strong)NSArray *goodimages;
//隐藏删除订单
@property (nonatomic)BOOL hideDeleteBtn;


-(instancetype)initWithDict:(NSDictionary *)dict;
+(instancetype)OrderStateModelWithDict:(NSDictionary *)dict;

@end
