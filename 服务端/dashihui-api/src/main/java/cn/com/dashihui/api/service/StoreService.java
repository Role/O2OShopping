package cn.com.dashihui.api.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.dao.Store;

public class StoreService {
	/**
	 * 查询指定店铺的详细信息
	 */
	public Store findById(int storeid){
		String sql = "SELECT * FROM t_dict_store ds"
				+ " LEFT JOIN"
				+ " (SELECT"
				+ " OE.storeid,"
				+ " CEIL(SUM(OE.eval1)/COUNT(OE.eval1)) eval1,"
				+ " CEIL(SUM(OE.eval2)/COUNT(OE.eval2)) eval2,"
				+ " CEIL(SUM(OE.eval3)/COUNT(OE.eval3)) eval3"
				+ " FROM (SELECT boe.eval1,boe.eval2,boe.eval3,bo.storeid FROM t_bus_order_eval boe INNER JOIN t_bus_order bo ON boe.orderNum=bo.orderNum WHERE bo.storeid=?) OE"
				+ " GROUP BY OE.storeid) OER ON OER.storeid=ds.id"
				+ " WHERE ds.id=?";
		return Store.me().findFirst(sql,storeid,storeid);
	}
	
	/**
	 * 查询指定店铺的最新一条通知
	 */
	public Record findLastTip(int storeid){
		return Db.findFirst("SELECT content FROM t_dict_store_tip WHERE storeid=? ORDER BY orderNo",storeid);
	}
}
