//
//  MyGoodsSureOredrShiHuiBiCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MyGoodsSureOredrShiHuiBiCellDelegate<NSObject>
- (void)setSHiHuiBiSwitchState:(BOOL)state;
@end
@interface MyGoodsSureOredrShiHuiBiCell : UITableViewCell
- (void)setUpViewWithShiHuiBi:(NSString *)shiHuiBi;
@property(nonatomic,assign)id<MyGoodsSureOredrShiHuiBiCellDelegate>delegate;
@end
