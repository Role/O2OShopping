//
//  RegisterTwoStepViewController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "BaseViewController.h"
//判断第二步是注册还是修改密码（页面判断）
#import "Global.h"
typedef NS_ENUM(NSInteger, TwoStepIsRegisterOrModifiedPassword) {
    twoStepIsRegister = 0,//注册
    twoStepIsModifiedPassword = 1,//修改密码
    twoStepIsFindTheSecret = 2,//找回密码
    TwoStepIsModifiedPhoneNumber //变更手机号
};
@interface RegisterTwoStepViewController : BaseViewController
@property (nonatomic) TwoStepIsRegisterOrModifiedPassword typeOfView;//页面判断
@property (strong, nonatomic) NSString *checkCode;//验证码
@property (strong, nonatomic) NSString *phoneNum;//手机号
@property (strong, nonatomic) NSString *oldPassword;//原密码
@end
