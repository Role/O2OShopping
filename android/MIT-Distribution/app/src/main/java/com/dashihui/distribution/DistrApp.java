package com.dashihui.distribution;

import android.app.Application;

/**
 * Created by NiuFC on 2016/3/29.
 */
public class DistrApp extends Application {

    private static DistrApp mApp;
    @Override
    public void onCreate() {
        super.onCreate();
        mApp = new DistrApp();
    }


    public static DistrApp getApplication(){
        if (mApp ==null){
            mApp = new DistrApp();
        }
        return mApp;
    }
    @Override
    public void onTerminate() {
        // 应用结束 在此保存数据
        // ...在此写代码
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        // 系统内存过低，在此部署一些内存回收策略
        super.onLowMemory();
    }

}
