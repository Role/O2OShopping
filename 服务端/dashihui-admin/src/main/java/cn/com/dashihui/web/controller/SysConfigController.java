package cn.com.dashihui.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.SysConfig;
import cn.com.dashihui.web.service.SysConfigService;

@RequiresAuthentication
public class SysConfigController extends BaseController{
	private static final Logger logger = Logger.getLogger(SysConfigController.class);
	private SysConfigService service = new SysConfigService();
    
    public void index(){
    	setAttr("configList", service.findAllConfig());
        render("index.jsp");
    }
    
    public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		service.sortConfig(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }

	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(pageNum, pageSize));
	}
    
	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：代码为空，2：代码过长，3：代码已经存在，4：值为空，5：值过长，6：描述过长
	 */
	public void doAdd(){
		//代码
		String code = getPara("code");
		//值
		String value = getPara("value");
		//描述
		String describe = getPara("describe");
		if(StrKit.isBlank(code)){
			renderResult(1);
			return;
		}else if(code.length()>5){
			renderResult(2);
			return;
		}else if(service.findByCode(code)!=null){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(value)){
			renderResult(4);
			return;
		}else if(value.length()>10){
			renderResult(5);
			return;
		}else if(!StrKit.isBlank(describe)&&describe.length()>100){
			renderResult(6);
			return;
		}else{
			//保存
			SysConfig config = new SysConfig()
				.set("code", code)
				.set("value", value)
				.set("describe", describe);
			if(service.addConfig(config)){
				renderSuccess(service.findById(config.getInt("id")));
				return;
			}
		}
		renderFailed();
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：值为空，2：值过长，3：描述过长
	 */
	public void doEdit(){
		//id
		String configid = getPara("configid");
		//值
		String value = getPara("value");
		//描述
		String describe = getPara("describe");
		if(StrKit.isBlank(configid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(value)){
			renderResult(1);
			return;
		}else if(value.length()>10){
			renderResult(2);
			return;
		}else if(!StrKit.isBlank(describe)&&describe.length()>100){
			renderResult(3);
			return;
		}else{
			//更新
			SysConfig config = new SysConfig()
				.set("id", configid)
				.set("value", value)
				.set("describe", describe);
			if(service.editConfig(config)){
				renderSuccess(service.findById(Integer.valueOf(configid)));
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delConfig(id)){
			logger.info("删除系统配置【"+id+"】");
			renderSuccess();
			return;
		}renderFailed();
	}
}
