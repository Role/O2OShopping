<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="mtag" uri="mytag"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>大实惠云商服务平台 社区o2o平台</title>
	<meta name="Keywords" content="河南大实惠,大实惠云商,社区O2O,O2O创业，智慧社区,社区便利店,便利店加盟,连锁便利店,河南便利店" />
	<meta name="Description" content="河南大实惠电子商务有限公司是一家突破型电商平台，通过社区O2O模式，为小区业主提供更便捷、更安全、更舒适的生活服务体验，大实惠云商用户通过大实惠云商APP其他社区业主沟通和分享、发起交易、组织社区活动等，全面成为社区居民的智慧生活管家！" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript">window.BASE_PATH = '${BASE_PATH}';</script>
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/css.css">
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/banner.css">
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/foot.css">
	<script type="text/javascript" src="${BASE_PATH }/static/js/common.js"></script>
	<script type="text/javascript" src="${BASE_PATH }/static/js/jquery.min.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/plugins/layer/layer.js"></script>
	<link rel="alternate icon" type="images/png" href="${BASE_PATH }/static/img/bsh_logo1.png">
	<style>
		.curr a{color:#fff !important;}
		.ndIntro{text-align: left;}
		.ndIntro p{text-indent: 2em;}
		.nqBody{border: 1px solid #ddd;margin-bottom: 1.5%;}
		footer{width: 61%;margin-left: 23%;}
	</style>
</head>
<body>
	<center>
		<header class="header" style="height: 83px;">
			<div class="header_left">
				<a name="coop_new"><img src="${BASE_PATH }/static/img/bsh_logo.png" /></a>
			</div>
			<div class="header_right">
				<ul>
					<li><a href="${BASE_PATH }/" class="button wobble-horizontal">网站首页</a></li>
					<li><a href="${BASE_PATH }/cooperate" class="button wobble-horizontal">合作加盟</a></li>
					<li><a href="${BASE_PATH }/about" class="button wobble-horizontal">关于我们</a></li>
					<li class="nav-active"><a class="button wobble-horizontal">新闻动态</a></li>
					<li><a href="${BASE_PATH }/contact" class="button wobble-horizontal">联系我们</a></li>
				</ul>
			</div>
		</header>
		<div class="banner">		
			    <img class="moving-bg" src="${BASE_PATH }/static/img/banner_d.jpg" alt="deepengine">
			    <div class="banner-mask"></div>
			    <div class="banner-box">
			        <h2></h2>
			        <p class="info"></p>		        
			        <a href="#" class="btn btn-curve">		        
			        
			        </a>
				</div>
	    	</div>
		<section class="news_list">
			<div class="nqBody">
				<div class="ndContent">
					<h4 class="ndTitle">${detail.title }</h4>
					<div class="nqlDate">
						<span class="nqldDay"><fmt:formatDate value="${detail.createDate }" pattern="dd" /></span>
						<span class="nqldMy"><fmt:formatDate value="${detail.createDate }" pattern="yyyy-MM" /></span>
					</div>
					<div class="ndInfo">
						发布日期：<span class="ndili"><fmt:formatDate value="${detail.createDate }" pattern="yyyy-MM-dd HH:mm:ss" /></span>
					</div>
					<div class="ndIntro">
						<p>
							<span style="font-size: 16px;">${detail.context }</span>
						</p>

					</div>

					<ul class="prevAnext">
						<c:if test="${!empty detail.previd }">
							<li>上一篇：<a href="${BASE_PATH}/notice/detail/${detail.previd}">${detail.prevTitle}</a></li>
						</c:if>
						<c:if test="${!empty detail.nextid }">
							<li>下一篇：<a href="${BASE_PATH}/notice/detail/${detail.nextid}">${detail.nextTitle}</a></li>
						</c:if>
					</ul>
				</div>
			</div>
		</section>
		<!--在线留言-->
		<footer style="height: 450px;">
			<div style="width:55%; float: left;margin: 1% 0 0 2%;">
			<h1 style="">在线留言</h1>
			<h3>请您填写以下相关信息</h3>
			</div>
			<div style="width:40%;float: left;margin-top: 3%;">
				<img src="${BASE_PATH }/static/img/images/c_34.png" style="width:60% ;" />
			</div>
			<div class="wbox  business margin100 clrfix">
				<div class="left">
					<form
						action="http://www.91dashihui.com/cooperate;jsessionid=546C1F14B3C702877DA1F8B841AEFF93#">
						<dl>
							<dt>
								<input type="text" id="name" name="name" maxlength="8" placeholder="请输入您的姓名" class="text"/>
							</dt>
							<dd>
								<input type="text" id="msisdn" name="msisdn" maxlength="11" placeholder="请输入您的手机号" class="text"/>
							</dd>
							<dt>
								<input type="text" id="mail" name="mail" maxlength="30" placeholder="请输入您的邮箱" class="text"/>
							</dt>
							<dd>
								<input type="text" id="qq" maxlength="10" name="qq" placeholder="请输入您的QQ" class="text" maxlength="10"/>
							</dd>
							<dd class="height178">
								<textarea id="context" name="context" maxlength="200" placeholder="请输入您的留言" class="text long height178"></textarea>
							</dd>
							<dd>
								<input type="button" id="save" class="submit" value="提交" onclick="javascript:doJoin();">
							</dd>
						</dl>
					</form>
				</div>
				<div class="right">
					<font>河南大实惠电子商务有限公司<br />联系电话：400-0079661/0371-86563519<br />电子邮箱：dashihui2015@163.com<br />公司地址：郑东新区绿地之窗尚峰座5层<br />
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/wscm.png" />
							</dt>
							<dd>微商城订阅号</dd>
						</dl>
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/a.png" />
							</dt>
							<dd>客户端二维码</dd>
						</dl>
					</font>
				</div>
			</div>
			<p class="big_foot_span" style="bottom:0;">
	  		    <span>Copyright©2015-2015.All Rights Reserved</span>
				河南大实惠电子商务有限公司版权所有&nbsp;&nbsp;豫ICP备15031706号 
			</p>
		</footer>
	</center>
	<div style="background: url(${BASE_PATH }/static/img/images/c_33.png) repeat-x;width: 100%; float: left;">
		<div style="width:188px;margin:0 auto;">
			<a href="#coop_new" style="display:block;">
				<img src="${BASE_PATH }/static/img/images/c_32.png" style="margin:0;float: left;" />
			</a>
		</div>
	</div>
</body>
</html>