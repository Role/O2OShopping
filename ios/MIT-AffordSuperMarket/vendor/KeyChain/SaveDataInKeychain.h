//
//  SaveDataInKeychain.h
//  MIT_itrade
//
//  Created by apple on 14-4-19.
//  Copyright (c) 2014年 ___ZhengDaXinXi___. All rights reserved.
/*********************************
 文件名: SaveDataInKeychain
 功能描述: 把登录用户名保存到钥匙串
 创建人: 郭涛
 修改日期: 20140419
 *********************************/

#import <Foundation/Foundation.h>

@interface SaveDataInKeychain : NSObject
+ (void)save:(NSString *)service data:(id)data;
+ (id)show:(NSString *)service;
+ (void)delete:(NSString *)service;
@end
