package cn.com.dashihui.seller.interceptor;

import com.jfinal.aop.Invocation;

import cn.com.dashihui.seller.common.Constants;
import cn.com.dashihui.seller.controller.BaseController;
import cn.com.dashihui.seller.dao.Seller;

/**
 * 拦截用户请求，判断如果是首次访问，则重定向至微信用户授权
 */
public class TestInterceptor extends AjaxInterceptor{
	
	@Override
	public void onAjax(Invocation inv) {
		onAjaxNope(inv);
	}
	
	@Override
	public void onAjaxNope(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		Object inited = c.getSessionAttr("inited");
		if(inited==null){
			c.setSessionAttr(Constants.USER, Seller.me().findFirst("SELECT * FROM t_dict_store_seller WHERE id=1"));
		}
		inv.invoke();
	}
}
