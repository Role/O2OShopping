package cn.com.dashihui.web.controller;

import org.apache.shiro.authc.credential.DefaultPasswordService;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.kit.ValidateKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.SerShop;
import cn.com.dashihui.web.service.SerShopService;

public class SerShopController extends BaseController{
	private static final Logger logger = Logger.getLogger(SerShopController.class);
	private SerShopService service = new SerShopService();
    
    public void index(){
        render("index.jsp");
    }
    /**
     * 分页数据
     */
    public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		String name = getPara("t");
		renderResult(0,service.findByPage(pageNum, pageSize, name));
	}

	/**
	 * 转到增加页面
	 */
	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：用户名为空，2：用户名过长，3：用户已经存在，4：密码为空，5：密码过长，6：商家名为空，7：商家名过长，8：地址过长，9：电话不正确
	 */
	public void doAdd(){
		//用户名
		String username = getPara("username");
		//密码
		String password = getPara("password");
		//名称
		String name = getPara("name");
		//地址
		String address = getPara("address");
		//电话
		String tel = getPara("tel");
		//是否营业中
		int isWork = getParaToInt("isWork",1);
		//是否可用
		int enabled = getParaToInt("enabled",0);
		if(StrKit.isBlank(username)){
			renderResult(1);
			return;
		}else if(username.length()>20){
			renderResult(2);
			return;
		}else if(service.findExistsByUsername(username)){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(password)){
			renderResult(4);
			return;
		}else if(password.length()>100){
			renderResult(5);
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(6);
			return;
		}else if(name.length()>100){
			renderResult(7);
			return;
		}else if(!StrKit.isBlank(address) && address.length()>100){
			renderResult(8);
			return;
		}else if(!ValidateKit.Tel(tel) && !ValidateKit.Mobile(tel)){
			renderResult(9);
			return;
		}else{
			//保存
			SerShop shop = new SerShop()
				.set("storeid", getCurrentUser().get("id"))
				.set("name", name)
				.set("username", username)
				.set("password", new DefaultPasswordService().encryptPassword(password))
				.set("address", address)
				.set("tel", tel)
				.set("isWork", isWork)
				.set("enabled", enabled);
			if(service.addShop(shop)){
				renderSuccess(service.findById(shop.getInt("id")));
				return;
			}
		}
		renderFailed();
	}
	

	
	/**
	  * 转到商家信息展示页面
	  */
	public void detail(){
		int id = getParaToInt(0,0);
		if(id != 0){
			setAttr("object", service.findById(id));
		}
		render("detail.jsp");
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：商家名为空，2：商家名过长，3：地址过长，4：电话不正确
	 */
	public void doEdit(){
		//店铺id
		String id =  getPara("shopid");
		//名称
		String name = getPara("name");
		//地址
		String address = getPara("address");
		//电话
		String tel = getPara("tel");
		//是否营业中
		int isWork = getParaToInt("isWork",1);
		//是否可用
		int enabled = getParaToInt("enabled",0);
		if(StrKit.isBlank(id)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>100){
			renderResult(2);
			return;
		}else if(!StrKit.isBlank(address) && address.length()>100){
			renderResult(3);
			return;
		}else if(!ValidateKit.Tel(tel) && !ValidateKit.Mobile(tel)){
			renderResult(4);
			return;
		}else{
			//更新
			int shopid = Integer.valueOf(id).intValue();
			SerShop shop = new SerShop()
					.set("id", shopid)
					.set("name", name)
					.set("address", address)
					.set("tel", tel)
					.set("isWork", isWork)
					.set("enabled", enabled);
			if(service.editShop(shop)){
				renderSuccess(service.findById(Integer.valueOf(id)));
				return;
			}
		}
		renderFailed();
	}
	
	public void toEditPwd(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("editPwd.jsp");
	}
	
	/**
	 * 更新密码
	 * @return -1：异常，0：成功，1：密码为空，2：密码过长
	 */
	public void doEditPwd(){
		//商家ID
		String shopid = getPara("shopid");
		//密码
		String password = getPara("password");
		if(StrKit.isBlank(shopid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(password)){
			renderResult(1);
			return;
		}else if(password.length()>100){
			renderResult(2);
			return;
		}else{
			//更新
			if(service.editShop(new SerShop()
				.set("id", shopid)
				.set("password", new DefaultPasswordService().encryptPassword(password)))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delShop(id)){
			logger.info("删除服务商家【"+id+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
