创建COOKIE，保存数据
$.cookie('the_cookie', 'the_value');

创建7天有效期的COOKIE
$.cookie('the_cookie', 'the_value', { expires: 7 });

读取COOKIE的值
$.cookie('the_cookie'); // => "the_value"
$.cookie('not_existing'); // => undefined

读取所有的COOKIE
$.cookie(); // => { "the_cookie": "the_value", "...remaining": "cookies" }

删除指定COOKIE的值
#返回值为true时表示cookie存在，返回false时表示对应的cookie不存在
$.removeCookie('the_cookie');

设置保存和读取COOKIE时经过编码
$.cookie.raw = true;

保存json格式的值
$.cookie.json = true;

取出COOKIE值时可设置类型转换
$.cookie('foo', '42');
$.cookie('foo', Number); // => 42