//
//  UserInfoModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "UserInfoModel.h"
@implementation UserInfoModel
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.nickName forKey:@"nickName"];
    [aCoder encodeObject:self.xiaoquID forKey:@"xiaoquID"];
    [aCoder encodeObject:self.xiaoquName forKey:@"xiaoquName"];
    [aCoder encodeObject:self.userName forKey:@"userName"];
    [aCoder encodeObject:self.userPassword forKey:@"userPassword"];
    [aCoder encodeObject:self.userImage forKey:@"userImage"];
    [aCoder encodeObject:self.userLevel forKey:@"userLevel"];
    [aCoder encodeObject:self.userMoney forKey:@"userMoney"];
    [aCoder encodeObject:self.userInvitecode forKey:@"userInvitecode"];
    [aCoder encodeObject:self.userInviteuserID forKey:@"userInviteuserID"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.nickName = [aDecoder decodeObjectForKey:@"nickName"];
        self.xiaoquID = [aDecoder decodeObjectForKey:@"xiaoquID"];
        self.xiaoquName = [aDecoder decodeObjectForKey:@"xiaoquName"];
        self.userName = [aDecoder decodeObjectForKey:@"userName"];
        self.userPassword = [aDecoder decodeObjectForKey:@"userPassword"];
        self.userImage = [aDecoder decodeObjectForKey:@"userImage"];
        self.userLevel = [aDecoder decodeObjectForKey:@"userLevel"];
        self.userMoney = [aDecoder decodeObjectForKey:@"userMoney"];
        self.userInvitecode = [aDecoder decodeObjectForKey:@"userInvitecode"];
        self.userInviteuserID = [aDecoder decodeObjectForKey:@"userInviteuserID"];
    }
    return self;
}

@end
