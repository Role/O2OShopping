//公共工具库
var Kit = {
	//初始化UI
	initUI:function(panel){
		panel = panel?panel:$("body");
		//初始化所有checkbox以及radio的样式为icheck样式
		if($("input[type='checkbox'].iCheck,input[type='radio'].iCheck",panel).length!=0){
			$("input[type='checkbox'].iCheck,input[type='radio'].iCheck",panel).iCheck({
				checkboxClass: 'icheckbox_minimal-blue',
				radioClass: 'iradio_minimal-blue',
			    //checkboxClass: 'icheckbox_polaris',
				//radioClass: 'iradio_polaris',
				increaseArea: '10%'
			}).on("ifChecked",function(){
				var checkbox = $(this), val = checkbox.val(), callback = checkbox.data("callback");
				if(callback){
					Kit.apply(callback,[val]);
				}
			});
		}
		//trigger callback when init
		$("input[type='checkbox'].iCheck,input[type='radio'].iCheck",panel).filter("[data-callback]").each(function(i,s){
			var $this = $(this), $val = $this.val(), $callback = $this.data("callback");
			if($this.is(":checked")&&$callback){
				Kit.apply($callback,[$val]);
			}
		});
		//初始化所有popover
		if($("[data-toggle='popover']",panel).length!=0){
			$("[data-toggle='popover']").popover();
		}
		//初始化所有select
		if($(".selectpicker",panel).not(".bs-select-hidden").length!=0){
			var selectpicker = $(".selectpicker",panel).not(".bs-select-hidden");
			selectpicker.selectpicker();
			selectpicker.on("change",function(){
				var $this = $(this), $selected = $this.find("option:selected"), $callback = $this.data("callback"), $next = $this.data("next");
				//remove val
				$this.data("val",false);
				//trigger the next
				if($next)$($next).trigger("selectpicker.prev.picked",[$selected.val()]);
				//changed callback
				if($callback){
					Kit.apply($callback,[$selected.val(),$selected.text()]);
				}
			});
			//trigger callback when init
			selectpicker.filter("[data-callback]").each(function(i,s){
				var $this = $(this), $selected = $this.find("option:selected"), $callback = $this.data("callback");
				if($selected&&$callback){
					Kit.apply($callback,[$selected.val(),$selected.text()]);
				}
			});
			//diy event
			selectpicker.on("selectpicker.prev.picked",function(e,pv){
				var $this = $(this), $url = $this.data("url"), $next = $this.data("next"), $val = $this.data("val"), $key = $this.data("key"), $defValue = $this.data("defValue"), $defText = $this.data("defText");
				//remove val
				$this.data("val",false);
				if($url){
					var valueKey = $key?$key.split(":")[0]:"value";
					var textKey = $key?$key.split(":")[1]:"text";
					//default option
					$this.empty().append("<option value='"+(($defValue!=undefined)?$defValue:"")+"'>"+($defText?$defText:"请选择")+"</option>").selectpicker("refresh");
					//get remote data
					var remoteurl = $url.replace('{value}', pv?pv:"");
					$.post(remoteurl,function(data){
						if(data&&data.length!=0){
							$(data).each(function(di,dd){
								$this.append("<option value='"+dd[valueKey]+"'"+($val&&$val==dd[valueKey]?"selected":"")+">"+dd[textKey]+"</option>");
							});
							//refresh
							$this.selectpicker("refresh");
							//trigger the next when has default val
							if($val&&$next){
								$($next).trigger("selectpicker.prev.picked",[$val]);
							}else if($next){
								$($next).trigger("selectpicker.clear");
							}
						}else{
							$($next).trigger("selectpicker.clear");
						}
					},"json");
				}
			}).on("selectpicker.clear",function(e){
				var $this = $(this), $next = $this.data("next"), $defValue = $this.data("defValue"), $defText = $this.data("defText");
				//clear and set default option
				$this.empty().append("<option value='"+(($defValue!=undefined)?$defValue:"")+"'>"+($defText?$defText:"请选择")+"</option>").selectpicker("refresh");
				if($next){
					$($next).trigger("selectpicker.clear");
				}
			});
			//trigger the first
			selectpicker.filter("[data-url]").each(function(indx,select){
				var $this = $(this), $isfirst = $this.data("isfirst"), $issingle = $this.data("issingle"), $url = $this.data("url"),
				$next = $this.data("next"), $val = $this.data("val"), $defValue = $this.data("defValue"), $defText = $this.data("defText");
				//is first or has no next
				if($isfirst && $url || $issingle && $url){
					$this.trigger("selectpicker.prev.picked",[$val]);
				}else{
					//default option
					$this.empty().append("<option value='"+(($defValue!=undefined)?$defValue:"")+"'>"+($defText?$defText:"请选择")+"</option>").selectpicker("refresh");
				}
			});
		}
		//初始化所有file
		if($("input[type='file']",panel).not(".bs-fileinput-ed").length>0){
			$("input[type='file']",panel).not(".bs-fileinput-ed").each(function(indx,file){
				var $this = $(this), $val = $this.data("val"), $ext = $this.data("ext"), $showCaption = $this.data("showCaption");
				//初始化上传控件参数
				var options = {language: 'zh',showUpload: false,showCaption: !!$showCaption,showRemove:false,browseClass: "btn btn-primary"};
				//设置默认允许上传的文件格式
				if($ext&&$ext.length!=0){
					options.allowedFileExtensions = $ext.split(",");
				}else{
					options.allowedFileExtensions = ['jpg', 'png'];
				}
				//如果有默认要显示的图片，则设置默认显示
				if($val&&$val.length!=0)options.initialPreview = ["<img src='"+FTP_PATH+$val+"' class='file-preview-image' width='100%'>"];
				//初始化
				$this.fileinput(options);
				//添加标识，防止重复初始化
				$this.addClass("bs-fileinput-ed");
			});
		}
		//为所有的缩略图增加点击显示大图事件
		$(".thumbnail img",panel).on("click",function(){
			var $img = $(this);
			Kit.photo($img.attr("src"));
		});
		//为所有的日期时间控件初始化
		if($(".datetimepicker",panel).not(".custom-inited").length!=0){
			$(".datetimepicker",panel).not(".custom-inited").each(function(indx,item){
				var $this = $(item), $format = $this.data("format"), $start = $this.data("startDate"), $end = $this.data("endDate");
				Kit.dataPicker($this,$format,$start,$end);
			});
		}
	},
	//显示加载中遮罩动画
	showLoading:function(){
		window.loading = layer.load();
	},
	//关闭加载中遮罩动画
	closeLoading:function(){
		if(window.loading>=0){
			layer.close(window.loading);
		}
	},
	//消息弹出框
	alert:function(message){
		layer.msg(message);
	},
	/**
	 * 执行普通函数，或是字符串代码的函数，2015/10/15添加
	 * @param func 字符串函数名或函数体
	 * @param args 函数入参
	 * @param defaultValue 最终函数若是没执行，则返回默认值
	 */
	apply:function (func,args,defaultValue) {
	    if (typeof func === 'string') {
	    	//support obj.func1.func2
	        var fs = func.split('.');
	        if (fs.length > 1) {
	            func = window;
	            $.each(fs, function (i, f) {
	                func = func[f];
	            });
	        } else {
	            func = window[func];
	        }
	    }
	    if (typeof func === 'function') {
	        return func.apply(null, args);
	    }
	    return defaultValue;
	},
	/**
	 * 创建dialog面板，并返回dialog对象，2015/10/12添加
	 * @param title 标题
	 * @param url 要加载的远程URL地址
	 * @param options 其他选项
	 * 		size：'size-normal'、'size-wide'、'size-large'
	 * 		cssType：'type-default'、'type-info'、'type-primary' (default)、'type-success'、'type-warning'、'type-danger'
	 * 		draggable：是否可拖动，默认true
	 * 		closable：是否可关闭（包含右上角关闭按钮，以及点击dialog以外页面时自行关闭功能），默认为false
	 */
	dialog:function(title,url,options){
		var dialogBody = $("<div></div>").load(url);
		return new BootstrapDialog({
			title:title,
			size:options&&options.size?options.size:"size-normal",
			type:options&&options.cssType?options.cssType:"type-primary",
			draggable:options&&options.draggable?options.draggable:true,
			closable:options&&options.closable?options.closable:false,
	        message:dialogBody,
	        onshown: function(dialogRef){
	        	Kit.initUI(dialogBody);
            },
	    });
	},
	/**
	 * 创建confirm面板，2015/10/12添加
	 * @param title 标题
	 * @param message 提示内容
	 * @param confirmCallback 点击确定按钮执行回调
	 * @param options 其他选项
	 * 		cssType：'type-default'、'type-info'、'type-primary' (default)、'type-success'、'type-warning'、'type-danger'
	 * 		cancelCallback：点击取消按钮执行回调
	 */
	confirm:function(title,message,confirmCallback,options){
		BootstrapDialog.confirm({
			title:title?title:"提示",
			message:message,
			type:options&&options.cssType?options.cssType:"type-danger",
			btnCancelLabel:"取消",
			btnOKLabel:"确定",
			callback:function(isConfirm){
				if(isConfirm){
					if(confirmCallback){confirmCallback();}
				}else{
					if(options&&options.cancelCallback){options.cancelCallback();}
				}
			}
		});
	},
	/**
	 * 分页，2015/10/12添加
	 * @param selector 分页信息所在容器（理论上ul、div等均可，但是经试验，如果是div的话，会在div下创建ul，效果没有直接使用ul作容器好）
	 * @param action 分页查询URL地址，每点击页码时会请求该url，并以RESTFUL的形式拼接上要查询的页码，例：/xx/xx/getPageData/1
	 * @param params 请求参数
	 * @param callback 数据请求回来后需要执行的回调，可在此方法中处理查询出来的数据
	 */
	pagination:function(selector,action,params,callback){
		return $().bootstrapPaginator({
			selector:selector,
			href:action,
			params:params,
			callback:callback
		});
	},
	/** 
	 * 对日期进行格式化，2015/10/11添加
	 * @param date 要格式化的日期 
	 * @param format 进行格式化的模式字符串
	 *     支持的模式字母有： 
	 *     y:年, 
	 *     M:年中的月份(1-12), 
	 *     d:月份中的天(1-31), 
	 *     h:小时(0-23), 
	 *     m:分(0-59), 
	 *     s:秒(0-59), 
	 *     S:毫秒(0-999),
	 *     q:季度(1-4)
	 * @return String
	 * @author yanis.wang
	 * @see	http://yaniswang.com/frontend/2013/02/16/dateformat-performance/
	 */
	dateFormat:function (dateStr, format){
		var date = new Date(Date.parse(dateStr.replace(/-/g,   "/")));
	    var map = {
	        "M": date.getMonth() + 1, //月份 
	        "d": date.getDate(), //日 
	        "h": date.getHours(), //小时 
	        "m": date.getMinutes(), //分 
	        "s": date.getSeconds(), //秒 
	        "q": Math.floor((date.getMonth() + 3) / 3), //季度 
	        "S": date.getMilliseconds() //毫秒 
	    };
	    format = format.replace(/([yMdhmsqS])+/g, function(all, t){
	        var v = map[t];
	        if(v !== undefined){
	            if(all.length > 1){
	                v = '0' + v;
	                v = v.substr(v.length-2);
	            }
	            return v;
	        }
	        else if(t === 'y'){
	            return (date.getFullYear() + '').substr(4 - all.length);
	        }
	        return all;
	    });
	    return format;
	},
	/** 
	 * 对标识进行转换，2015/10/11添加
	 * @param flag 要转换的标识 
	 * @param 相应可选值1
	 * @param 相应可选值1所对应的中文释义
	 * @param 相应可选值2
	 * @param 相应可选值2所对应的中文释义
	 * @param ......
	 * @return 相应值对应的中文释义
	 * 举例：
	 * var flag = 2;
	 * flagTransform(flag,1,'类型1',2,'类型2',3,'类型3'......);
	 * 返回值为：'类型2'
	 */
	flagTransform:function(flag){
		var argLen = arguments.length;
		if(argLen<1){
			return "NaN";
		}else if(argLen==1||(argLen-1)%2!=0){
			return flag;
		}else{
			for(var i=0;i<argLen-1;){
				if(flag==arguments[i+1]){
					return arguments[i+2];
				}
				i=i+2;
			}
		}
	},
	/** 
	 * 时间控件，2015/10/28添加
	 * @param target 时间控件绑定的对象 
	 * @param format 需要的时间格式可以为空默认格式为：yyyy-mm-dd hh:ii:ss
	 * 举例：
	 * <input type="text" id="dataPicker">
     * kit.dataPicker('#datetimepicker','yyyy-mm-dd');	
	 */
	dataPicker:function(target,format,startDate,endDate){
		var options = {
			format:"yyyy-mm-dd hh:ii:ss",
			weekStart:1,
			todayBtn:1,
			autoclose:1,
			startDate:"2001-01-01",
			endDate:"2021-12-30",
			todayHighlight:1,
			startView:1,
			minView:2,
			forceParse:1,
			language:'zh-CN'
		};
		//判断自定义显示格式时，使用自定义的
		if(format){
			options.format = format;
		}
		if(startDate){
			options.startDate = startDate;
		}
		if(endDate){
			options.endDate = endDate;
		}
		//根据格式不同，初始化的视图也不同
		if(format.indexOf("yyyy-mm-dd")<0){
			//当不包含年月日时，初始化视图为时间视图，结束于秒视图
			options.startView=1;
			options.minView=0;
		}else if(format=="yyyy-mm-dd"){
			//当格式为年月日时，初始化视图为日期视图，结束于日期视图
			options.startView=2;
			options.minView=2;
		}
		$(target).datetimepicker(options);
	},
	/**
	 * 弹框显示图片，2015/12/07添加
	 */
	photo:function(src){
		layer.photos({
	        photos: {
	            "title": "",//相册标题
	            "id": 0,//相册id
	            "start": 0,//初始显示的图片序号，默认0
	            "data": [//相册包含的图片，数组格式
	                {
	                    "alt": "",//图片名
	                    "pid": 0, //图片id
	                    "src": src, //原图地址
	                    "thumb": "" //缩略图地址
	                }
	            ]
	        }
	    });
	},
	curentTime:function(){ 
        var now = new Date();
       
        var year = now.getFullYear();       //年
        var month = now.getMonth() + 1;     //月
        var day = now.getDate();            //日
       
        var hh = now.getHours();            //时
        var mm = now.getMinutes();          //分
        var ss = now.getSeconds();			//妙
       
        var clock = year + "-";
       
        if(month < 10) clock += "0";
       
        clock += month + "-";
       
        if(day < 10) clock += "0";
           
        clock += day + " ";
       
        if(hh < 10) clock += "0";
           
        clock += hh + ":";
        if (mm < 10) clock += '0';

        clock += mm + ":";
        if (ss < 10) clock += '0'; 
        clock += ss;
        return(clock); 
    } 
};