package cn.com.dashihui.web.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.jfinal.kit.PropKit;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.LogKeywordService;

@RequiresAuthentication
public class LogKeywordController extends BaseController{
	private LogKeywordService service = new LogKeywordService();
    
	@RequiresPermissions("log:keyword:index")
    public void index(){
		render("index.jsp");
    }
	
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(getCurrentUser().getInt("storeid"), pageNum, pageSize));
	}
}
