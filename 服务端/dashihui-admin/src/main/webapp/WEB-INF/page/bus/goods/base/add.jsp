<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		rules: {
			marketPrice: {isMoney: true},
			sellPrice: {isMoney: true}
		},
		messages:{
			name: {required: "请输入商品标题"},
			marketPrice: {required: "请输入商品市场价",isMoney: "请填写正确的金额，最多保留两位小数位"},
			sellPrice: {required: "请输入商品销售价",isMoney: "请填写正确的金额，最多保留两位小数位"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 2:
						Kit.alert("请选择商品分类");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						addDialog.close();
						return;
					}
				}
			});
		}
	});
});
</script>
<form id="addForm" action="${BASE_PATH}/bus/goods/base/doAdd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">标题</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" class="form-control" placeholder="请输入商品标题" required maxlength="100">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">分类</label>
	    <div class="col-lg-9">
    		<select id="categoryonid" name="categoryonid" class="selectpicker" data-width="24%" data-url="${BASE_PATH}/api/category" data-isfirst="true" data-next="#categorytwid" data-key="id:name"></select>
			<select id="categorytwid" name="categorytwid" class="selectpicker" data-width="24%" data-url="${BASE_PATH}/api/category/{value}" data-next="#categorythid" data-key="id:name"></select>
			<select id="categorythid" name="categorythid" class="selectpicker" data-width="24%" data-url="${BASE_PATH}/api/category/{value}" data-next="#categoryfoid" data-key="id:name"></select>
			<select id="categoryfoid" name="categoryfoid" class="selectpicker" data-width="24%" data-url="${BASE_PATH}/api/category/{value}" data-key="id:name"></select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">品牌</label>
	    <div class="col-lg-9">
    		<select name="brandid" class="selectpicker form-control" data-url="${BASE_PATH}/api/brand" data-issingle="true" data-key="id:name"></select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">规格</label>
	    <div class="col-lg-9">
	        <input type="text" name="spec" class="form-control" placeholder="请输入商品规格" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">描述</label>
	    <div class="col-lg-9">
        	<textarea name="shortInfo" class="form-control" placeholder="请输入商品短简介" maxlength="120"></textarea>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">市场价</label>
	    <div class="col-lg-9">
        	<input type="text" name="marketPrice" class="form-control" placeholder="请输入商品市场价" required>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">销售价</label>
	    <div class="col-lg-9">
        	<input type="text" id="sellPrice" name="sellPrice" class="form-control" placeholder="请输入商品销售价" required>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">是否可用</label>
	    <div class="col-lg-9">
	        <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="state" value="1" checked> 可用</li>
	    		<li><input type="radio" class="iCheck" name="state" value="0"> 不可用</li>
	    	</ul>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left"  type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>