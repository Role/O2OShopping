//
//  Strort_Breakfast_Bottom.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/25.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol Strort_Breakfast_BottomDelegate<NSObject>
//立即抢购
-(void)gotoShoppingCar;
@end
@interface Strort_Breakfast_Bottom : UIView
@property(nonatomic, unsafe_unretained)id<Strort_Breakfast_BottomDelegate>delegate;
@property(nonatomic,retain)UILabel *totalPrice;
@property(nonatomic,retain)UILabel *totalSingular;
@property(nonatomic,retain)UIImageView *imageView;
@property(nonatomic,retain)UIButton *shopCarBtn;
@end

