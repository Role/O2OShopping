//
//  Strore_detail_ContentModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Strore_detail_ContentModel.h"

@implementation Strore_detail_ContentModel

-(instancetype)initWithDict:(NSDictionary *)dict
{
    if (self =[super init]) {
        //使用kVC
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}
+(instancetype)productModelWithDict:(NSDictionary *)dict
{
    return [[self alloc]initWithDict:dict];
}
@end
