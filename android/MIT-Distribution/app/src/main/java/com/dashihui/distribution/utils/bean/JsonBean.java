package com.dashihui.distribution.utils.bean;

import org.json.JSONArray;

/**
 * Created by WYY on 2016/4/5.
 */
public class JsonBean {
    public String position;
    public String size;
    public String rowContent;
    public JSONArray image;
}