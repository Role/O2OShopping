//
//  Service_RootCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
//添加代理，用于按钮加减的实现
@protocol Service_RootCellDelegate <NSObject>

- (void)selectedItemWithItemContent:(NSDictionary*)itemDic;
@end
@interface Service_RootCell : UITableViewCell
@property(assign,nonatomic)id<Service_RootCellDelegate>delegate;

-(void)setUpSuberViewWithCategory:(NSMutableArray *)category;
- (CGFloat)cellFactHight;

@end
