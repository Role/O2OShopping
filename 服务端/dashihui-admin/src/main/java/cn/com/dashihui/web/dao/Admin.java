package cn.com.dashihui.web.dao;

import com.jfinal.plugin.activerecord.Model;

public class Admin extends Model<Admin>{
	private static final long serialVersionUID = 1L;
	private static Admin me = new Admin();
	public static Admin me(){
		return me;
	}
	
	public String getTrueName(){
		return getStr("trueName");
	}
}
