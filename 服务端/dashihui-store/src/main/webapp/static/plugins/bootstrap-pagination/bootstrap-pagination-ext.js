//twbs-pagination扩展，更易于页面使用，如果twbs-pagination升级，可直接替换原js，本js不需修改
(function($){
	$.fn.extend({
		bootstrapPaginator : function(options){
			var selector = options.selector;
			var href = options.href;
			var params = options.params;
			var callback = options.callback;
			var $container = $("tbody", this) || $(this);
			//加载第一页数据
			$(options.selector).data("currPage",1);
			$.post(options.href + "/1", options.params, function(result){
				callback(result);
				//根据总页码，以判断是否要绑定显示分页栏
				if(result.object.totalPage>0){
					$(options.selector).twbsPagination({
						totalPages : result.object.totalPage,
						first: "首页",
						prev:"上一页",
						next:"下一页",
						last :"尾页",
						startPage : 1,
						//绑定每次翻页执行的回调
						onPageClick : loadPage
					});
				}
			},"json");
			var loadPage = function(event,pageNumber){
				//保存当前页码
				$(options.selector).data("currPage",pageNumber);
				$.post(options.href + "/" + pageNumber, options.params, function(result){
					callback(result);
				});
			}
			return {
				loadPage: function(pageNumber){
					loadPage(null,pageNumber);
				},refreshPage: function(){
					loadPage(null,parseInt($(options.selector).data("currPage")));
				},destroy: function(){
					var obj = $(options.selector).data("twbs-pagination");
					if(obj)obj.destroy();
				}
			};
		}
	});
})(jQuery,window,document);