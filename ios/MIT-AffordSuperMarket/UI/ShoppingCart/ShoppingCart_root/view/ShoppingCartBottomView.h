//
//  ShoppingCartBottomView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/19.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ShoppingCartBottomView
 Created_Date： 20151107
 Created_People： GT
 Function_description： 购物车底部视图（编辑和未编辑状态）
 ***************************************/

#import <UIKit/UIKit.h>
//添加代理，用于按钮加减的实现
@protocol ShoppingCartBottomViewDelegate <NSObject>
- (void)selectedAllProductsWithSelectedStatus:(BOOL)isSelected;
- (void)goJieSuan;
- (void)addGuanZhu;
- (void)deleteProduct;
@end

@interface ShoppingCartBottomView : UIView
@property(nonatomic, unsafe_unretained)id<ShoppingCartBottomViewDelegate>delegate;
@property(nonatomic)BOOL isAllSelected;//是否全选
@property(nonatomic, strong)NSString *totalPrice;//总价
@property(nonatomic, strong)NSString *productCount;//选中商品个数

- (id)initWithFrame:(CGRect)frame withEditStatus:(BOOL)isEdit;
- (void)changeSubViewShowWithEditStatus:(BOOL)isEdit;
@end
