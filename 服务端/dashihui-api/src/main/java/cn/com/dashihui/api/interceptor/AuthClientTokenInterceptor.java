package cn.com.dashihui.api.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.ResultField;
import cn.com.dashihui.api.common.ResultState;

/**
 * 验证请求中是否携带有终端标识，如果有，则跳过，如果没有，则返回错误
 */
public class AuthClientTokenInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		String signature = c.getPara(ResultField.FIELD_CLIENT_TOKEN);
		if(StrKit.isBlank(signature)){
			c.renderResult(ResultState.STATE_CLIENT_TOKEN_EXCEPTED);
		}else if(signature.length()!=36){
			c.renderResult(ResultState.STATE_CLIENT_TOKEN_ERROR);
		}else{
			inv.invoke();
		}
	}
}
