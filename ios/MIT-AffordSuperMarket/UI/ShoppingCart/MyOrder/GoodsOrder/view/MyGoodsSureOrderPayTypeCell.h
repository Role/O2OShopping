//
//  MyGoodsSureOrderPayTypeCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/12.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, CellSepLineShowType) {
    ktopSepLineShow = 0,//上边分割线展示
    kbottomSepLineShow = 1 ,//下边分割线展示
    kallSepLineShow //上下分割线都展示
};
@interface MyGoodsSureOrderPayTypeCell : UITableViewCell
- (void)setupViewWithData:(NSDictionary*)dataDic cellSepLineShowType:(CellSepLineShowType)sepLineShowType;
@end
