//
//  StoreShowRatingCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/19.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： StoreShowRatingCell
 Created_Date： 20160319
 Created_People： GT
 Function_description： 门店评价cell
 ***************************************/

#import <UIKit/UIKit.h>

@interface StoreShowRatingCell : UITableViewCell
- (void)updateViewWithData:(NSDictionary*)dataDic;
- (CGFloat)cellFactHight;
@end
