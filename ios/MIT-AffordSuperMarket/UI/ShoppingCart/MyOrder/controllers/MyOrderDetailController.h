//
//  MyOrderDetailController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyOrderDetailController
 Created_Date： 20151107
 Created_People：JSQ
 Function_description：我的订单详情页面
 ***************************************/

#import "BaseViewController.h"

@interface MyOrderDetailController : BaseViewController
//requsestDataType =1 是商品接口  =0 是服务接口
@property(nonatomic,strong)NSString *requestDataType; //requestDataType  商品订单 是1 服务订单是 0
@property(nonatomic,strong)NSString *homeMakeType;  //家政服务 有数据 说明是家政服务
@property(nonatomic, strong)NSString *orderNum;
@end
