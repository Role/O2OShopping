package cn.com.dashihui.web.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.jfinal.kit.PropKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.WebJoinService;

@RequiresAuthentication
public class WebJoinController extends BaseController{
	private static final Logger logger = Logger.getLogger(WebJoinController.class);
	private WebJoinService service = new WebJoinService();
    
	@RequiresPermissions("web:join:index")
    public void index(){
        render("index.jsp");
    }
    
    public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(pageNum, pageSize));
	}
    
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delJoin(id)){
			logger.info("删除加盟留言内容【"+id+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
