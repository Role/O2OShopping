var Plus = {
		iosBridge : "WebViewJavascriptBridge",
		androidBridge : "android",
		isIOS : function(){
			return /iphone|ipad|ipod/.test(navigator.userAgent.toLowerCase());
		},
		isAndroid : function(){
			return /android/.test(navigator.userAgent.toLowerCase());
		},
		onReady : function(callback){
			if (Plus.isIOS()) {
				if (window[Plus.iosBridge]) {
					if(!window.iosBridgeInited){
						window[Plus.iosBridge].init();
						window.iosBridgeInited = true;
					}
					callback(window[Plus.iosBridge]);
				} else {
					document.addEventListener('WebViewJavascriptBridgeReady', function() {
						if(!window.iosBridgeInited){
							window[Plus.iosBridge].init();
							window.iosBridgeInited = true;
						}
						callback(window[Plus.iosBridge])
					}, false);
				}
			} else if (Plus.isAndroid()) {
				callback(window[Plus.androidBridge]);
			}
		}
}