//
//  IntroductionController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/3.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.


/***************************************
 ClassName： IntroductionViewController
 Created_Date： 20151103
 Created_People： GT
 Function_description： 引导页
 ***************************************/

#import "IFTTTAnimatedPagingScrollViewController.h"

@interface IntroductionController : IFTTTAnimatedPagingScrollViewController

@end
