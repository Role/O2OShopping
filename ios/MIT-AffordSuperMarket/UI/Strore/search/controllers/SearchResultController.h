//
//  SearchResultController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： SearchHistoryController
 Created_Date： 20151103
 Created_People： GT
 Function_description：搜索结果页面
 ***************************************/
#import "BaseViewController.h"

@interface SearchResultController : BaseViewController
@property(nonatomic, strong)NSString *keyword;
@property(nonatomic, strong)NSString *keywordID;
@property(nonatomic, strong)NSString *sortType;
@end
