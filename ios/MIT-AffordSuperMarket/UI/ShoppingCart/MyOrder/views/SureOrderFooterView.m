//
//  SureOrderFooterView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/21.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SureOrderFooterView.h"
#define kleftSpce 15
//uinitls
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
@interface SureOrderFooterView ()
//新用户
@property(nonatomic, strong)UILabel *userYouhuiTitleLabel;
@property(nonatomic, strong)UIImageView *userYouhuiImageView;
@property(nonatomic, strong)UILabel *userhuiPriceContentLabel;
//优惠说明
@property(nonatomic, strong)UIButton *youhuiShouMingBtn;
//总价格
@property(nonatomic, strong)UILabel *totalPriceTitleLabel;
@property(nonatomic, strong)UILabel *totalPriceContentLabel;
//优惠价格
@property(nonatomic, strong)UILabel *youhuiPriceTitleLabel;
@property(nonatomic, strong)UILabel *youhuiPriceContentLabel;
//实际价格
@property(nonatomic, strong)UILabel *factPriceTitleLabel;
@property(nonatomic, strong)UILabel *factPriceContentLabel;
@end
@implementation SureOrderFooterView
- (instancetype)initWithFrame:(CGRect)frame isNewUser:(BOOL)isNewUser isHadYouHui:(BOOL)isHadYouHui isHideYouHuiShuoMing:(BOOL)isHideYouHuiShuoMing {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubViewIsNewUser:isNewUser isHadYouHui:isHadYouHui isHideYouHuiShuoMing:isHideYouHuiShuoMing];
    }
    return self;
}


#pragma mark -- 初始化视图
- (void)setupSubViewIsNewUser:(BOOL)isNewUser isHadYouHui:(BOOL)isHadYouHui isHideYouHuiShuoMing:(BOOL)isHideYouHuiShuoMing {
    CGFloat y = 0;
    if (isNewUser) {//是新用户
        //标题
        NSString *userYouHuiTitle = @"新用户立减";
        CGSize userYouHuiTitleSize = [userYouHuiTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(160, 44)];
        _userYouhuiTitleLabel = [self setLabelWithTitle:userYouHuiTitle fontSize:15 textColor:@"#000000"];
        _userYouhuiTitleLabel.frame = CGRectMake(kleftSpce
                                                 , 0
                                                 , userYouHuiTitleSize.width
                                                 , 44);
        [self addSubview:_userYouhuiTitleLabel];
        //图片
        UIImage *userYouHuiImage = [UIImage imageNamed:@"newUserLiJian_Image"];
        _userYouhuiImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_userYouhuiTitleLabel.frame) + 6
                                                                           , CGRectGetMidY(_userYouhuiTitleLabel.frame) - userYouHuiImage.size.height/2
                                                                           , userYouHuiImage.size.width
                                                                            , userYouHuiImage.size.height)];
        _userYouhuiImageView.image = userYouHuiImage;
        [self addSubview:_userYouhuiImageView];
        //内容
        _userhuiPriceContentLabel = [self setLabelWithTitle:@"-￥5" fontSize:15 textColor:@"#000000"];
        _userhuiPriceContentLabel.frame = CGRectMake(CGRectGetWidth(self.bounds) - kleftSpce - 80
                                                     , CGRectGetMinY(_userYouhuiTitleLabel.frame)
                                                     , 80, CGRectGetHeight(_userYouhuiTitleLabel.bounds));
        _userhuiPriceContentLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:_userhuiPriceContentLabel];
        y = CGRectGetMaxY(_userhuiPriceContentLabel.frame);
        
    }
    
    if (!isHideYouHuiShuoMing) {
        //优惠说明
        
        _youhuiShouMingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _youhuiShouMingBtn.frame = CGRectMake(CGRectGetWidth(self.bounds) - 120 - kleftSpce, y, 120, 44);
        [_youhuiShouMingBtn setTitle:@"优惠说明" forState:UIControlStateNormal];
        [_youhuiShouMingBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
        [_youhuiShouMingBtn setImage:[UIImage imageNamed:@"youHuiShuoMing_Image"] forState:UIControlStateNormal];
        _youhuiShouMingBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _youhuiShouMingBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 8);
        _youhuiShouMingBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self addSubview:_youhuiShouMingBtn];
        
        //分割线
        UIImage *sepLine = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"]
                                                   frame:CGRectMake(0
                                                                    ,0
                                                                    ,CGRectGetWidth(self.bounds) - 2*kleftSpce
                                                                    ,1)];
        UIImageView *sepLineView = [[UIImageView alloc]initWithImage:sepLine];
        sepLineView.frame = CGRectMake(kleftSpce
                                       , CGRectGetMaxY(_youhuiShouMingBtn.frame) + 5
                                       , CGRectGetWidth(self.bounds) - 2*kleftSpce
                                       , 1);
        [self addSubview:sepLineView];
    }
    
    
    //商品金额
    CGFloat minY = isHideYouHuiShuoMing ? 10 : CGRectGetHeight(_youhuiShouMingBtn.bounds)+1;
    NSString *totalPriceTitle = @"商品金额";
    CGSize totalPriceTitleSize = [totalPriceTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(100, 51)];
    _totalPriceTitleLabel = [self setLabelWithTitle:totalPriceTitle fontSize:15 textColor:@"#999999"];
    _totalPriceTitleLabel.frame = CGRectMake(kleftSpce
                                             , minY
                                             , totalPriceTitleSize.width
                                             , 51);
    [self addSubview:_totalPriceTitleLabel];
    
    _totalPriceContentLabel = [self setLabelWithTitle:@"￥216" fontSize:15 textColor:@"#000000"];
    _totalPriceContentLabel.frame = CGRectMake(CGRectGetMaxX(_totalPriceTitleLabel.frame)
                                               , CGRectGetMinY(_totalPriceTitleLabel.frame)
                                               , 45, CGRectGetHeight(_totalPriceTitleLabel.bounds));
    _totalPriceContentLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_totalPriceContentLabel];
    
    //优惠金额
    if (isHadYouHui) {
        NSString *youhuiPriceTitle = @"-优惠";
        CGSize youhuiPriceTitleSize = [youhuiPriceTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(100, 51)];
        _youhuiPriceTitleLabel = [self setLabelWithTitle:youhuiPriceTitle fontSize:15 textColor:@"#999999"];
        _youhuiPriceTitleLabel.frame = CGRectMake(CGRectGetMaxX(_totalPriceContentLabel.frame) + 4
                                                  , CGRectGetMinY(_totalPriceContentLabel.frame)
                                                  , youhuiPriceTitleSize.width, CGRectGetHeight(_totalPriceContentLabel.bounds));
        [self addSubview:_youhuiPriceTitleLabel];
        
        _youhuiPriceContentLabel = [self setLabelWithTitle:@"￥5" fontSize:15 textColor:@"#000000"];
        _youhuiPriceContentLabel.frame = CGRectMake(CGRectGetMaxX(_youhuiPriceTitleLabel.frame)
                                                  , CGRectGetMinY(_youhuiPriceTitleLabel.frame)
                                                  , 40
                                                  , CGRectGetHeight(_youhuiPriceTitleLabel.bounds));
        [self addSubview:_youhuiPriceContentLabel];
        
        
    }
    //实际金额
    _factPriceContentLabel = [self setLabelWithTitle:@"￥211" fontSize:17 textColor:@"#da411d"];
    _factPriceContentLabel.frame = CGRectMake(CGRectGetWidth(self.bounds) - kleftSpce - 45
                                                         , CGRectGetMinY(_totalPriceTitleLabel.frame)
                                                         , 45
                                                         , CGRectGetHeight(_totalPriceTitleLabel.bounds));
    _factPriceContentLabel.textAlignment = NSTextAlignmentRight;
                                              
    [self addSubview:_factPriceContentLabel];
    
    NSString *factPriceTitle = @"还需付：";
    CGSize factPriceTitleSize = [factPriceTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(80, 51)];
    _factPriceTitleLabel = [self setLabelWithTitle:factPriceTitle fontSize:15 textColor:@"#999999"];
    _factPriceTitleLabel.frame = CGRectMake(CGRectGetMinX(_factPriceContentLabel.frame) - factPriceTitleSize.width
                                            , CGRectGetMinY(_factPriceContentLabel.frame)
                                            , factPriceTitleSize.width
                                            , CGRectGetHeight(_factPriceContentLabel.bounds));
    [self addSubview:_factPriceTitleLabel];
    
}

- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}

#pragma mark -- 更新数据
- (void)updateDataWithDataModel:(SureOrderFooterViewModel*)model {
    
}
@end
