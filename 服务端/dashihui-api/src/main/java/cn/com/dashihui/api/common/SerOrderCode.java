package cn.com.dashihui.api.common;

/**
 * 家政类服务订单的各项状态
 */
public class SerOrderCode {
	
	/**
	 * 订单状态
	 * 订单状态 1：正常，2：完成，3：取消，4：拒单，5：删除，6：过期，7：无效
	 */
	public static class OrderState{
		public final static int  NORMAL = 1;
		public final static int  FINISH = 2;
		public final static int  CANCEL = 3;
		public final static int  REFUSE = 4;
		public final static int  DELETE = 5;
		public final static int  EXPIRE = 6;
		public final static int  WASTE  = 7;
	}
	
	/**
	 * 订单支付类型
	 * 支付方式，1：在线支付，2：服务后付款
	 */
	public static class OrderPayType{
		public final static int  ON_LINE    = 1;
		public final static int  AFTER_SERVICE= 2;
	}
	/**
	 * 订单支付渠道
	 * 在线支付渠道，1：微信支付，2：支付宝支付
	 */
	public static class OrderPayMethod{
		public final static int  WEIXIN  = 1;
		public final static int  ALIPAY  = 2;
	}
	/**
	 * 订单支付状态
	 * 支付状态，1：待支付，2：已支付
	 */
	public static class OrderPayState{
		public final static int  NO_PAY  = 1;
		public final static int  HAD_PAY = 2;
	}
	
	/**
	 * 服务类型
	 * 服务类型 1:日常保洁 2:深度保洁
	 */
	public static class OrderType{
		public final static int DAILY = 1;
		public final static int DEPTH = 2;
	}
	
	/**
	 * 派单状态
	 * 派单状态,1:店铺待接单,2:店铺已接单，3：店铺已派单 ,4:商家确认接单
	 */
	public static class OrderDispatchState{
		
		public final static int STORE_NO_ACCEPT = 1;
		public final static int STORE_HAD_ACCEPT = 2;
		public final static int STORE_HAD_DISPATCH = 3;
		public final static int PROSER_HAD_ACCEPT = 4;
	}
}
