<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		messages:{
			name: {required: "请输入名称"},
			code: {required: "请输入代码"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 5:
						Kit.alert("代码已经存在");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						onResourceEdited(data.object);
						editDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editForm" action="${BASE_PATH}/dict/city/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="name" value="${object.name}" class="form-control" placeholder="请输入名称" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">代码</label>
	    <div class="col-lg-9">
        	<input type="text" name="code" value="${object.code}" class="form-control" placeholder="请输入六位代码" required maxlength="6">
	    </div>
	</div>
	<c:if test="${parent!=null}">
	<div class="form-group">
		<label class="col-lg-2 control-label">归属于</label>
	    <div class="col-lg-9">
	    	<input type="text" class="form-control" value="${parent.name}" disabled="disabled"/>
	    </div>
		<input type="hidden" id="parentid" name="parentid" value="${parent.id}"/>
	</div>
	</c:if>
	<c:if test="${parent==null}"><input type="hidden" name="parentid" value="0"/></c:if>
	<div class="form-group">
	    <label class="col-lg-2 control-label">类型</label>
	    <div class="col-lg-9">
	    	<c:choose>
	    	<c:when test="${object.type==1}"><input type="text" class="form-control" value="省份/直辖市" disabled="disabled"/></c:when>
	    	<c:when test="${object.type==2}"><input type="text" class="form-control" value="地市" disabled="disabled"/></c:when>
	    	<c:when test="${object.type==3}"><input type="text" class="form-control" value="县/区" disabled="disabled"/></c:when>
	    	</c:choose>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="cityid" value="${object.id}"/>
</form>