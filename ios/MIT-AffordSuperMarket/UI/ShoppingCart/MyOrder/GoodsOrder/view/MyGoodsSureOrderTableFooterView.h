//
//  MyGoodsSureOrderTableFooterView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/11.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsSureOrderTableFooterView
 Created_Date： 20160411
 Created_People：GT
 Function_description： 商品提交订单table尾部视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol MyGoodsSureOrderTableFooterViewDelegate<NSObject>
- (void)addOrderMarkInfo;
@end
@interface MyGoodsSureOrderTableFooterView : UIView
@property(nonatomic,unsafe_unretained)id<MyGoodsSureOrderTableFooterViewDelegate>delegate;
@property(nonatomic, strong)NSString *markInfo;//备注信息
@property(nonatomic, strong)NSString *goodsPrice;//商品价格
@property(nonatomic, strong)NSString *peiSongPrice;//配送费
@end
