<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<meta http-equiv="X-UA-Compatible" content="edge" />
		<title>${STORE.title}</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- lib -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/lib/swiper/css/swiper.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_index.css" />
	</head>
	<body class="app-navbar-body">
		<div style="position:relative;">
			<header id="top" class="am-header app-header">
				<div class="am-g">
					<div class="am-u-sm-5 am-u-md-4 am-u-lg-2 am-text-truncate">
						<a href="${BASE_PATH}/location" id="locationBtn" class="app-title">
							<span>${STORE.title}</span>
							<i class="am-header-icon am-icon-angle-down"></i>
						</a>
					</div>
					<form action="${BASE_PATH}/search" method="post" id="searchForm">
				  		<div class="am-u-sm-7 am-u-md-8 am-u-lg-10">
				  			<div class="am-g app-top-search-bar">
								<input type="search" name="keyword" id="keyword" 
									class="am-fl am-u-sm-10 am-u-md-11 am-u-lg-11 app-top-search-inpt" placeholder="天天都实惠">
								<div class="am-fl am-u-sm-2 am-u-md-1 am-u-lg-1 app-top-search-btn" onclick="javascript:submit();">
									<span class="am-icon-search"></span>
								</div>
								<div class="am-cf"></div>
				  			</div>
						</div>
					</form>
				</div>
			</header>
			<div id="downloadTip" style="display:none;" data-am-sticky>
				<div class="app-tip-download" style="position:absolute;z-index:1;">
					<img src="${BASE_PATH}/static/app/img/w_logo.png" height="45">
					<span>下载APP，享更多优惠！</span>
					<a href="javascript:void(0);" class="app-btn-close am-fr"><img src="${BASE_PATH}/static/app/img/close.png" width="30" height="30"></a>
					<a href="http://www.91dashihui.com/app/download" class="app-btn-download am-fr">立即下载</a>
					<div class="am-cf"></div>
				</div>
			</div>
		</div>
		<c:if test="${adList!=null&&fn:length(adList)!=0}">
		<div class="am-slider am-slider-a1">
			<ul class="am-slides">
				<c:forEach items="${adList}" var="ad">
					<c:choose>
					<c:when test="${ad.type==1}">
					<li><a href="${ad.link}"><img src="${FTP_PATH}${ad.thumb}"></a></li>
					</c:when>
					<c:when test="${ad.type==2}">
					<li><a href="${BASE_PATH}/goods/detail/${ad.goodsid}"><img src="${FTP_PATH}${ad.thumb}"></a></li>
					</c:when>
					</c:choose>
				</c:forEach>
			</ul>
		</div>
		</c:if>
		
		<c:if test="${tip!=null&&fn:length(tip.content)!=0}">
		<div class="app-tip">
			<div id="app-tip-icon" class="app-tip-icon am-fl" style="width:100px;">
	           	<div class="app-hr-vertical am-fr"></div>
				<img src="${BASE_PATH}/static/app/img/tip.png" height="20" class="am-center"/>
	           	<div class="am-cf"></div>
			</div>
			<div id="app-tip-content" class="app-tip-content am-fl" style="padding:0 5px;">
				<marquee behavior="scroll" direction="left" scrollamount="2">${tip.content}</marquee>
			</div>
			<div class="am-cf"></div>
		</div>
		</c:if>
		
		<ul class="am-avg-sm-4 am-avg-md-4 am-avg-lg-4 app-category">
			<li>
				<a href="${BASE_PATH}/goods/index?code=010000000">
					<img src="${BASE_PATH}/static/app/img/homepage1.png" width="80%"/>
					<span class="am-navbar-label">生鲜蔬果</span>
				</a>
			</li>
			<li>
				<a href="${BASE_PATH}/goods/index?code=030000000">
					<img src="${BASE_PATH}/static/app/img/homepage2.png" width="80%"/>
					<span class="am-navbar-label">生活百货</span>
				</a>
			</li>
			<li>
				<a href="${BASE_PATH}/goods/index?code=040000000">
					<img src="${BASE_PATH}/static/app/img/homepage3.png" width="80%"/>
					<span class="am-navbar-label">粮油调料</span>
				</a>
			</li>
			<li>
				<a href="${BASE_PATH}/goods/index?code=020000000">
					<img src="${BASE_PATH}/static/app/img/homepage4.png" width="80%"/>
					<span class="am-navbar-label">酒水饮料</span>
				</a>
			</li>
		</ul>
		
		<c:if test="${tagList!=null&&fn:length(tagList)!=0}">
		<c:forEach items="${tagList}" var="tag">
		<div class="app-part app-part-row">
			<div class="app-part-title">
				<span class="am-fl">${tag.tagName}</span>
				<a href="${BASE_PATH}/goods/tag/${tag.tagCode}" class="am-fr">查看更多 <i class="am-icon-angle-right"></i></a>
				<div class="am-cf"></div>
			</div>
			<div class="app-goods-list app-swiper swiper-container">
				<div class="swiper-wrapper">
					<c:forEach items="${tag.list}" var="goods">
					<div class="app-goods-item swiper-slide">
		            	<dl>
		            		<dt><a href="${BASE_PATH}/goods/detail/${goods.id}"><img data-src="${FTP_PATH}${goods.thumb}" class="swiper-lazy" width="100%"></a></dt>
		            		<dd class="app-item-title">${goods.name}</dd>
		            		<dd>
		            			<span class="app-item-price-normal"><i class="am-header-icon am-icon-rmb"></i> ${goods.sellPrice}</span>
		            			<span class="app-item-price-del"><i class="am-header-icon am-icon-rmb"></i> ${goods.marketPrice}</span>
		            		</dd>
		            	</dl>
		            	<div class="app-hr-vertical"></div>
		            </div>
					</c:forEach>
	            </div>
	        </div>
	    </div>
	    </c:forEach>
	    </c:if>
	    
	    <a href="http://www.91dashihui.com/app/download"><img src="${BASE_PATH}/static/app/img/ad/ad2.png" width="100%" class="app-m-t-10"></a>
	    
	    <!-- 精品推荐 -->
	    <div class="app-part app-part-flow">
			<div class="app-part-title">
				<span class="am-fl">精品推荐</span>
				<div class="am-cf"></div>
			</div>
			<div id="recomGoodsList" class="am-g app-goods-list"></div>
	    </div>
		
		<div class="am-navbar am-cf am-no-layout app-navbar">
			<ul class="am-navbar-nav am-cf am-avg-sm-3">
				<li>
					<a href="javascript:void(0);" class="app-active">
						<img src="${BASE_PATH}/static/app/img/menu_bot22.png"/>
						<span class="am-navbar-label">便利店</span>
					</a>
				</li>
				<li class="app-navbar-cart">
					<a href="${BASE_PATH}/cart">
						<img src="${BASE_PATH}/static/app/img/menu_bot03.png"/>
						<span class="am-navbar-label">购物车</span>
						<span id="cartNum" class="am-badge am-round" style="display:none;"></span>
					</a>
				</li>
				<li>
					<a href="${BASE_PATH}/my">
						<img src="${BASE_PATH}/static/app/img/menu_bot05.png"/>
						<span class="am-navbar-label">我的</span>
					</a>
				</li>
			</ul>
		</div>
		
		<!-- 购物车+1的动画效果，需要app-navbar设置z-index小于本控件 -->
		<span id="cartNumAnimate" style="color:red;font-weight:bold;font-size:18px;position:fixed;left:48%;bottom:20px;z-index:2;display:none;">+1</span>
		
		<div data-am-widget="gotop" class="am-gotop am-gotop-fixed" >
			<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
		</div>
				
		<!-- include -->
		<%@include file="include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/swiper/js/swiper.jquery.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-marquee/jquery.marquee.min.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as goods}}
			<div class="am-u-sm-6 am-u-md-4 am-u-lg-3 app-goods-item">
            	<dl>
            		<dt><a href="${BASE_PATH}/goods/detail/{{goods.id}}"><img data-original="${FTP_PATH}{{goods.thumb}}" class="am-center lazyload" width="100%"></a></dt>
					{{if goods.isSelf==1}}
            		<dd class="app-item-title"><img src="${BASE_PATH}/static/app/img/goods_self.png" height="13"/>{{goods.name}}</dd>
					{{else}}
					<dd class="app-item-title">{{goods.name}}</dd>
					{{/if}}
            		<dd>
            			<span class="app-item-price-normal"><i class="am-icon-rmb"></i> {{goods.sellPrice}}</span>
            			<span class="app-item-price-del"><i class="am-icon-rmb"></i> {{goods.marketPrice}}</span>
						<span>
							<a onclick="javascript:addCart('{{goods.id}}','{{goods.isSelf}}','{{goods.name}}','{{goods.spec}}','{{goods.thumb}}','{{goods.sellPrice}}','{{goods.marketPrice}}');">
								<img src="${BASE_PATH }/static/app/img/add_car.png" style="width:30px;heigt:30px;padding-top:6px;"/>
							</a>
						</span>
            		</dd>
					
            	</dl>
            </div>
			{{/each}}
		</script>
		<script type="text/javascript">
			var pageNum = 0,totalPage = 1;
			//加载标识，表示当前是否有请求未完成，防止同时多个请求
			var loading = false;
			$(function(){
				//下载客户端提示框事件初始化
				if(!Kit.dao.get("isDownloadTipClosed")){
					$("#downloadTip").show().find(".app-btn-close").on("click",function(){
						$("#downloadTip").hide();
						Kit.dao.save("isDownloadTipClosed",true);
					});
				}
				//轮播图初始化
				$('.am-slider').flexslider();
				//商品横向列表，判断数量越过个时才初始化滚动
				var swiperOption = {
					//默认每页显示3个
			        slidesPerView: 3,
			        //响应式配置
			        breakpoints: {
			        	2048: {slidesPerView: 7,spaceBetween: 40},
			        	1280: {slidesPerView: 6,spaceBetween: 40},
			            1024: {slidesPerView: 5,spaceBetween: 40},
			            768: {slidesPerView: 4,spaceBetween: 30},
			            640: {slidesPerView: 3,spaceBetween: 20},
			            319: {slidesPerView: 2,spaceBetween: 10}
			        }
			    };
				$(".app-swiper").each(function(index,swiper){
					if($(".app-goods-item",swiper).length>=3){
						$(swiper).attr("id","swiper"+index);
						//初始化swiper，并设置图片自动加载，需要img标签添加class="swiper-lazy"
						new Swiper("#swiper"+index, $.extend(swiperOption,{autoplay:2500+index*1500,lazyLoading:true}));
					}
				});
				//绑定“加载中”进入加载事件
				Kit.util.onPageEnd(function(){
	               	if(pageNum < totalPage && !loading){
	               		loading = true;
	               		$("#recomGoodsList").append("<div class=\"app-loading\">正在加载</div>");
	                	Kit.ajax.post("${BASE_PATH}/goods/pageByRecom",{pageNum:pageNum+1,pageSize:10},function(result){
							$("#recomGoodsList").append(template("dataTpl",result.object));
							//amazeui要求在最后一个元素上添加am-u-end样式，否则默认为右浮动
							$(".app-goods-item","#recomGoodsList").removeClass("am-u-end").last().addClass("am-u-end");
							//图片延迟加载
							$("img.lazyload","#recomGoodsList").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							$(".app-loading","#recomGoodsList").remove();
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
							//重置加载标识
							loading = false;
						});
	               	}
				});
				//用JS来计算屏幕宽度，然后设置“小惠通知”内容的宽度
				$(window).on("load",function(){
					var iconWidth = $("#app-tip-icon").width();
					var windowWidth = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
					$("#app-tip-content").css({"width": (windowWidth-iconWidth)+"px"});
					//初始化marquee
					$('#app-tip-content marquee').attr("width",windowWidth-iconWidth-10).marquee("pointer");
				});
				//查询购物车状态
				Kit.ajax.post("${BASE_PATH}/cart/state",{},function(result){
					if(result.object.CART_COUNTER!=0)
						$("#cartNum").text(result.object.CART_COUNTER).show();
				});
			});
			function addCart(id,isSelf,name,spec,thumb,sellPrice,marketPrice){
				var goods = {
					"id":id,
					"isSelf":isSelf,
					"name":name,
					"spec":spec,
					"thumb":thumb,
					"sellPrice":sellPrice,
					"marketPrice":marketPrice
				};
				Kit.ajax.post("${BASE_PATH}/cart/add",goods,function(result){
					$("#cartNum").show().text(result.object.counter);
					$("#cartNumAnimate").show().animate({bottom:70,opacity:0},500,"linear",function(){
						$("#cartNumAnimate").css({"bottom":20+"px","opacity":100}).hide();
					});
				});
			}
			function submit() {
				var keyword = $("#keyword").val();
				if(keyword == '' || keyword == null) {
					return;
				}
				$("#searchForm").submit();
			}
		</script>
	</body>
</html>