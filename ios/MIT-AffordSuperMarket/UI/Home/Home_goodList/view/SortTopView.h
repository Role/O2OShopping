//
//  SortTopView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, BtnName) {
    kTogetherBtn = 100,//综合
    kProprietaryBtn ,//自营
    kPriceBtn  //价格
};

@protocol SortTopViewDelegate <NSObject>

-(void)kTogetherBtnClick;
-(void)kPriceBtnClickWithSort:(NSString *)sort;// 1是价格从低到高 2是价格从高到低
-(void)kProprietaryBtnWithSelectedState:(BOOL)selectedState;
@end
@interface SortTopView : UIView
@property(nonatomic)BtnName btnName;
@property(nonatomic,assign)id<SortTopViewDelegate>delegate;
@end
