//
//  Strore_detail_ContentModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Strore_detail_ContentModel : NSObject


//商品名字
@property(nonatomic,copy)NSString *productName;

//商品销售量
@property(nonatomic,copy)NSString *productSaleNumber;
//商品关注量
@property(nonatomic,copy)NSString *productFollow;
//商品价格
@property(nonatomic,copy)NSString *productMoney;

//商品品牌
@property(nonatomic,copy)NSString  *productBrand;
//商品规格
@property(nonatomic,copy)NSString  *productnorms;
//商品介绍
@property(nonatomic,copy)NSString *productDetaile;

-(instancetype)initWithDict:(NSDictionary *)dict;
+(instancetype)productModelWithDict:(NSDictionary *)dict;
@end
