package cn.com.dashihui.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.kit.FreeMarkerKit;
import cn.com.dashihui.web.base.BaseController;

@RequiresAuthentication
public class WxArticleOutputController extends BaseController{

	@RequiresPermissions(value={"wx:dashihui01:article:output:index","wx:iotocy:article:output:index"},logical=Logical.OR)
    public void index(){
		if(SecurityUtils.getSubject().isPermitted("wx:dashihui01:article:output:index")){
			setAttr("mp", 1);
		}else if(SecurityUtils.getSubject().isPermitted("wx:iotocy:article:output:index")){
			setAttr("mp", 2);
		}
        render("index.jsp");
    }
	
	/**
	 * 查询数据并生成静态页面
	 */
	public void output(){
		String ftlPath = getPara("ftlPath");
		String outputPath = getPara("outputPath");
		if(StrKit.isBlank(ftlPath)||StrKit.isBlank(outputPath)){
			renderResult(1);
			return;
		}
		try {
			int mp = getParaToInt("mp");
			//查询所需要的数据并整理
			List<Record> articleTopList = Db.find("SELECT A.id,A.title,A.thumb,A.categoryid FROM t_wx_article A INNER JOIN t_wx_article_category C ON A.categoryid=C.id AND C.mp=? WHERE A.isTop=1 ORDER BY A.createDate DESC LIMIT 5",mp);
			List<Record> categoryList = Db.find("SELECT C.* FROM t_wx_article_category C WHERE C.mp=? ORDER BY C.orderNo",mp);
			List<Record> articleList = Db.find("SELECT A.* FROM t_wx_article A INNER JOIN t_wx_article_category C ON A.categoryid=C.id AND C.mp=? ORDER BY A.createDate DESC",mp);
			for(Record category : categoryList){
				for(Record article : articleList){
					if(category.getInt("id").intValue()==article.getInt("categoryid").intValue()){
						List<Record> children = category.get("children");
						if(children==null){
							children = new ArrayList<Record>();
						}
						children.add(article);
						category.set("children", children);
					}
				}
			}
			//生成html页面
			//1.生成列表页
			Map<String,Object> indexDatas = new HashMap<String,Object>();
			indexDatas.put("FTP_PATH", PropKit.get("constants.ftppath"));
			indexDatas.put("articleTopList", articleTopList);
			indexDatas.put("categoryList", categoryList);
			FreeMarkerKit.output(ftlPath, "articleIndex.ftl", outputPath+"\\index.html", indexDatas);
			//2.循环生成各个详情页
			Map<String,Object> itemDatas = new HashMap<String,Object>();
			itemDatas.put("FTP_PATH", PropKit.get("constants.ftppath"));
			for(Record article : articleList){
				itemDatas.put("detail", article);
				FreeMarkerKit.output(ftlPath, "articleDetail.ftl", outputPath+"\\detail\\"+article.getInt("categoryid")+"-"+article.getInt("id")+".html", itemDatas);
			}
		}catch(Exception e){
			e.printStackTrace();
			renderFailed();
			return;
		}
		renderSuccess();
	}
}
