//提交留言
function doJoin(){
	var params = {
			name : $("#name").val(),
			msisdn : $("#msisdn").val(),
			qq : $("#qq").val(),
			mail : $("#mail").val(),
			address : $("#address").val(),
			context : $("#context").val()
		}
		if(params.name==""){
			layer.msg("请输入姓名");
		}else if(params.name.length>4){
			layer.msg("姓名过长");
		}else if(params.msisdn==""){
			layer.msg("请输入手机号码");
		}else if(params.msisdn.length>11){
			layer.msg("手机号码过长");
		}else if(params.context==""){
			layer.msg("请输入留言内容");
		}else if(params.context.length>200){
			layer.msg("留言内容过长");
		}else{
			window.loading = layer.load();
			$.post(window.BASE_PATH+"/doJoin",params,function(result){
				layer.close(window.loading);
				switch(result.flag){
					case 1:layer.msg("请输入姓名");return;
					case 2:layer.msg("请输入中文姓名");return;
					case 4:layer.msg("请输入手机号码");return;
					case 5:layer.msg("手机号码不正确");return;
					case 6:layer.msg("QQ号码不正确");return;
					case 8:layer.msg("邮箱地址不正确");return;
					case 11:layer.msg("请输入留言内容");return;
					case 13:layer.msg("操作太频繁");return;
					default:layer.msg("留言已提交，我们会尽快与您联系！");
				}
			},"json");
		}
}