//
//  Strore_TeHui_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/6.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Strore_TeHui_Controller
 Created_Date： 20151106
 Created_People： GT
 Function_description： 特惠专区
 ***************************************/

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, TeHuiType) {
    krecommendType = 100,//推荐
    klimitCountType = 101,//限量
    kyiYuanGouType  //一元购
};
@interface Strore_TeHui_Controller : BaseViewController
@property(nonatomic)TeHuiType teHuiType;
@property(nonatomic)NSInteger selectedIndex;

@end
