//
//  SearchViewController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SearchViewController.h"
//untls
#import "UIColor+Hex.h"
#import "AppDelegate.h"
//vendor
#import "RDVTabBarController.h"
//controllers
#import "SearchRootController.h"
@interface SearchViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong)NSArray *titles;
@property(nonatomic, strong)UITableView *tableView;
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavgationTitleView];
    [self addBackNavItem];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    
    _titles = @[@"按关注排名搜索",@"按销售数量搜索"];
    [self setupSubView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appdelgate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appdelgate.tabBarController setTabBarHidden:YES animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appdelgate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appdelgate.tabBarController setTabBarHidden:NO animated:YES];
}
#pragma mark -- 初始化视图

- (void)setNavgationTitleView {
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(54, 0, CGRectGetWidth(self.view.bounds) - 54, 44)];
    bottomView.backgroundColor = [UIColor colorWithHexString:@"#c52720"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 8, CGRectGetWidth(bottomView.bounds) - 10 , 28);
    [button setTitle:@"请输入要搜索的商品" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor whiteColor];
    [button.layer setMasksToBounds:YES];
    [button.layer setCornerRadius:4];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
    
    [button setImage:[UIImage imageNamed:@"search_logo"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(beatSearchEvent) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:button];
    self.navigationItem.titleView = bottomView;
    
}
- (void)setupSubView {
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0
                                                             , 0
                                                             , CGRectGetWidth(self.view.bounds)
                                                              , CGRectGetHeight(self.view.bounds) - 64)
                                             style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
}
#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = [self.titles objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    return cell;
}

#pragma mark -- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}
#pragma mark -- button Action
- (void)beatSearchEvent {
    SearchRootController *searchRootVC = [[SearchRootController alloc]init];
    [self.navigationController pushViewController:searchRootVC animated:NO];
}
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
