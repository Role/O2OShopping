//
//  MyGoodsOrderTrackingHeaderView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOrderTrackingHeaderView.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
#import "NSString+TextSize.h"
#define kleftSpace 16
#define ktopSpace 8
#define kItemHight  (HightScalar(98)-20)/3
#define kfontSize  FontSize(15)
@interface MyGoodsOrderTrackingHeaderView ()
@property(nonatomic, strong)NSMutableArray *contentViews;//存放内容视图
@end
@implementation MyGoodsOrderTrackingHeaderView

//初始化视图根据标题个数
- (id)initWithFrame:(CGRect)frame titles:(NSArray*)titles {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
        _contentViews = [NSMutableArray arrayWithCapacity:titles.count];
        if (titles.count != 0) {
            [self setupSubviewWithTitles:titles];
          
        }
    }
    return self;
}
- (void)setupSubviewWithTitles:(NSArray*)titles {
    for (NSInteger i = 0; i < [titles count]; i++) {
        NSString *title = [titles objectAtIndex:i];
        CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:kfontSize] maxSize:CGSizeMake(160, kItemHight)];
        CGRect titleFrame = CGRectMake(kleftSpace
                                       , ktopSpace + kItemHight*i
                                       ,titleSize.width
                                       , kItemHight);
        UILabel *titleLabel = [self createLabelWithFrame:titleFrame title:title fontSize:kfontSize titleColor:@"#555555"];
        titleLabel.font = [UIFont systemFontOfSize:kfontSize];
        [self addSubview:titleLabel];
        CGRect contentFrame = CGRectMake(CGRectGetMaxX(titleLabel.frame)
                                         , CGRectGetMinY(titleLabel.frame)
                                         , VIEW_WIDTH - CGRectGetMaxX(titleLabel.frame) - kleftSpace
                                         , CGRectGetHeight(titleLabel.bounds));
        UILabel *contentLabel = [self createLabelWithFrame:contentFrame title:@"" fontSize:kfontSize titleColor:@"#000000"];
        contentLabel.font = [UIFont systemFontOfSize:kfontSize];
        [self addSubview:contentLabel];
        [self.contentViews addObject:contentLabel];
        
    }
}

- (UILabel*)createLabelWithFrame:(CGRect)frame title:(NSString*)title fontSize:(CGFloat)fontSize titleColor:(NSString*)titleColor{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.font = [UIFont systemFontOfSize:FontSize(fontSize)];
    label.textColor = [UIColor colorWithHexString:titleColor];
    label.text = title;
    label.backgroundColor = [UIColor clearColor];
    return label;
}
//给视图赋值
- (void)updateViewWithDatas:(NSArray*)datas {
    if (datas.count != self.contentViews.count) {
        return ;
    }
    for (NSInteger i = 0; i < datas.count; i++) {
        UILabel *label = (UILabel*)[self.contentViews objectAtIndex:i];
        label.text = [NSString stringTransformObject:[datas objectAtIndex:i]];
    }
}

@end
