<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><link rel="stylesheet" href="${BASE_PATH}/static/plugins/kindEditor/themes/default/default.css" />
<link rel="stylesheet" href="${BASE_PATH}/static/plugins/kindEditor/plugins/code/prettify.css" />
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/kindeditor.js"></script>
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/lang/zh_CN.js"></script>
<script type="text/javascript" src="${BASE_PATH}/static/plugins/kindEditor/plugins/code/prettify.js"></script>
<form id="editDetail"  method="post" class="form-horizontal" >
	<input type="hidden" name="id" value="${object.id}">
	<div class="form-group">
		<div class="col-lg-12">
			<textarea name="describe">${object.describe}</textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="button" onclick="confirmButton()" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:detailEditDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>
<script>
function confirmButton(){
	var urldetail="${BASE_PATH}/bus/goods/base/doDetailEdit";
	$.post(urldetail, { 'id': $("input[name='id']").val(),'describe': $("textarea[name='describe']").val() },function(result){
		switch(result.flag){
		case 0:
			detailEditDialog.close();
			return;
		default:
			Kit.alert("系统异常，请重试");return;
		}
	});
}
var kindEditor;
kindEditor = KindEditor.create($("textarea[name='describe']"), {
	width:'100%',
	height:'500px',
	resizeType :0,//2或1或0，2时可以拖动改变宽度和高度，1时只能改变高度，0时不能拖动。
	langType : 'zh_CN',
    allowUpload : true, 
    uploadJson : '${BASE_PATH}/bus/goods/base/uploadimg',
    // fileManagerJson : '/bus/goods/imgscan',//指定浏览远程图片的服务器端程序
	items : [
		'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
		'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
		'insertunorderedlist', '|', 'emoticons', 'image', 'link'], 
	afterCreate : function() {this.sync();}, 
    afterBlur:function() {this.sync();} 
 });
</script>
