<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
<!--
var setSellerDataPaginator;
function doRefresh(){
	var p = $("#phone").val(), k = $("#keyword").val();
	if(setSellerDataPaginator){
		setSellerDataPaginator.destroy();
	}
	setSellerDataPaginator = Kit.pagination("#setSellerDataPagination","${BASE_PATH}/api/sellerPage",{p:p,k:k,"pageSize":10},function(result){
		$("#setSellerDataList").empty().append(template("setSellerDataTpl",result.object));
	});
}
function doSetSeller(id,title){
	onSellerChoosed('${orderNum}',id,title);
	setSellerDialog.close();
}
$(doRefresh);
//-->
</script>
<script type="text/html" id="setSellerDataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.name}}</td>
		<td>{{item.tel}}</td>
		<td>{{item.sex | flagTransform:1,'男',2,'女'}}</td>
		<td><button type="button" class="btn btn-sm btn-primary" onclick="doSetSeller('{{item.id}}','{{item.name}}')" title="选择">选择</button></td>
	</tr>
	{{/each}}
</script>
<div class="row">
	<div class="col-lg-12">
		<input type="text" id="phone" value="" class="form-control pull-left m-l-5 width200" placeholder="搜索手机号" maxlength="11">
		<input type="text" id="keyword" value="" class="form-control pull-left m-l-5 width200" placeholder="搜索关键字" maxlength="100">
		<button type="button" class="btn btn-success pull-left m-l-5" onclick="doRefresh()">查询</button>
		<div class="clearfix"></div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12" style="min-height:300px;">
		<div class="table-responsive">
			<table class="table table-striped table-hover">
	            <thead>
	                <tr>
	                    <th width="35%">配送员</th>
	                    <th width="30%">手机号</th>
	                    <th width="20%">性别</th>
	                    <th width="15%">操作</th>
	                </tr>
	            </thead>
	            <tbody id="setSellerDataList"></tbody>
	        </table>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<ul id="setSellerDataPagination" class="pagination-sm pull-right"></ul>
	</div>
</div>