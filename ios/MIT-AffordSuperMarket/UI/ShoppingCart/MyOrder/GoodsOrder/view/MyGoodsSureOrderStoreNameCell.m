//
//  MyGoodsSureOrderStoreNameCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/13.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsSureOrderStoreNameCell.h"

//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#define kleftSpace 15
#define krightSpace 15
#define kcellHight HightScalar(47)
#define ksepLineHight 0.5
#define khSpace 10
@interface MyGoodsSureOrderStoreNameCell()
@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)UILabel *contentLabel;
@property(nonatomic, strong)UIImageView *sepLineImageView;
@end
@implementation MyGoodsSureOrderStoreNameCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //初始化视图
        [self setSubView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setSubView {
    NSString *titleStr = kDaShiHuiStoreName;
    CGSize titleSize = [titleStr sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                      maxSize:CGSizeMake(120, kcellHight)];
    
    _titleLabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#555555"];
    _titleLabel.frame = CGRectMake(kleftSpace, 0, titleSize.width , kcellHight - ksepLineHight);
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_titleLabel];
    
    _contentLabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#e73f18"];
    _contentLabel.frame = CGRectMake(CGRectGetMaxX(_titleLabel.frame) + khSpace
                                    , CGRectGetMinY(_titleLabel.frame)
                                    , VIEW_WIDTH - CGRectGetMaxX(_titleLabel.frame) - khSpace
                                     , CGRectGetHeight(_titleLabel.bounds));
    [self.contentView addSubview:_contentLabel];
    
    UIImage *sepLineImage = [UIImage imageNamed:@"gray_line"];
    _sepLineImageView = [[UIImageView alloc]initWithImage:sepLineImage];
    _sepLineImageView.frame = CGRectMake(CGRectGetMinX(_titleLabel.frame)
                                         , CGRectGetMaxY(_titleLabel.frame)
                                         , VIEW_WIDTH - kleftSpace
                                         , ksepLineHight);
    [self.contentView addSubview:_sepLineImageView];
    
}
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
#pragma mark -- 给视图进行赋值
- (void)setupViewWithData:(NSDictionary*)dataDic  {
    self.titleLabel.text = [dataDic objectForKey:@"title"];
    self.contentLabel.text = [dataDic objectForKey:@"content"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

