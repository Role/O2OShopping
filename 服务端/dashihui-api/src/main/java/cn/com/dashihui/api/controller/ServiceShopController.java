package cn.com.dashihui.api.controller;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.ResultMap;
import cn.com.dashihui.api.dao.ServiceShop;
import cn.com.dashihui.api.service.ServiceShopService;
import cn.com.dashihui.kit.CommonKit;

public class ServiceShopController extends BaseController{
	
	ServiceShopService shopService = new ServiceShopService();
	 
	/**
	 *  服务分类获取
	 *  @param STOREID 店铺ID
	 */
	public void category(){
		String storeidStr = getPara("STOREID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}
		renderSuccess(shopService.getCategoryByStoreid(Integer.valueOf(storeidStr)));
		return;
	}
	
	/**
	 *  服务商家列表获取
	 *  @param CATEGORYCODE 分类代码
	 *  @param STOREID		店铺ID
	 *  @param PAGENUM	          页码
	 * 	@param PAGESIZE		数量
	 */
	public void shops(){
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		String categoryCodeStr = getPara("CATEGORYCODE");
		String storeidStr = getPara("STOREID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else{
			renderSuccess(shopService.getPageShop(pageNum, pageSize, Integer.valueOf(storeidStr),categoryCodeStr));
			return;
		}
		
	}
	
	/**
	 *  商家详情
	 *  @param  STOREID 店铺ID
	 *  @param  SHOPID  商家ID
	 */
	public void detail(){
		String storeidStr = getPara("STOREID");
		String shopidStr = getPara("SHOPID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else if(StrKit.isBlank(shopidStr)){
			renderFailed("参数SHOPID不能为空");
    		return;
		}else{
			int id = Integer.valueOf(shopidStr);
			int storeid = Integer.valueOf(storeidStr);
			
			ServiceShop shop =  shopService.getShopById(id,storeid);
			if(shop!=null){
				List<Record>  imglist = shopService.getListShopImgByShopid(id);
				List<String> strList = new ArrayList<String>();
				if(imglist!=null&&imglist.size()!=0){
					for(Record image : imglist){
						strList.add(image.getStr("THUMB"));
					}
				}
				//设置给商品详情信息
				shop.put("IMAGES", strList);
				shop.put("ITEMS", shopService.getListShopItemByShopid(id));
				renderSuccess(shop);
			}else{
				renderFailed("商家不存在");
			}
		}
	}
	
	/**
	 *  商家详情描述
	 *  @param  STOREID 店铺ID
	 *  @param  SHOPID  商家ID
	 */
	public void describe(){
		String storeidStr = getPara("STOREID");
		String shopidStr = getPara("SHOPID");
		
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else if(StrKit.isBlank(shopidStr)){
			renderFailed("参数SHOPID不能为空");
    		return;
		}else{
			ServiceShop shop = shopService.getShopDescribe(Integer.valueOf(shopidStr),Integer.valueOf(storeidStr));
			renderSuccess(ResultMap.newInstance().put("DESCRIBE", CommonKit.ifNull(shop.get("describe"),"")));
			return;
		}
	}

}
