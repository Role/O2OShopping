package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;

import cn.com.dashihui.web.dao.WxArticle;

public class WxArticleService {
	
	public boolean addArticle(WxArticle newObject){
		return newObject.save();
	}
	
	public boolean delArticle(int id){
		return WxArticle.me().deleteById(id);
	}
	
	public boolean editArticle(WxArticle object){
		return object.update();
	}
	
	public WxArticle findById(int id){
		return WxArticle.me().findFirst("SELECT * FROM t_wx_article WHERE id=?",id);
	}
	
	public Page<WxArticle> findByPage(int pageNum, int pageSize,int mp,String keyword,String beginDate,String endDate,int categoryid,String isTop){
		StringBuffer sBuffer = new StringBuffer();
		List<Object> params = new ArrayList<Object>();
		sBuffer.append("FROM t_wx_article A INNER JOIN t_wx_article_category B ON A.categoryid=B.id WHERE B.mp=?");
		params.add(mp);
		if(!StrKit.isBlank(keyword)){
			sBuffer.append(" AND (A.title LIKE ? OR A.describe LIKE ?)");
			params.add("%"+keyword+"%");
			params.add("%"+keyword+"%");
		}
		if(!StrKit.isBlank(beginDate)&&!StrKit.isBlank(endDate)){
			sBuffer.append(" AND A.startDate BETWEEN ? AND ?");
			params.add(beginDate);
			params.add(endDate);
		}else if(!StrKit.isBlank(beginDate)&&StrKit.isBlank(endDate)){
			sBuffer.append(" AND DATE_FORMAT(A.startDate,'%Y-%m-%d')>=?");
			params.add(beginDate);
		}else if(StrKit.isBlank(beginDate)&&!StrKit.isBlank(endDate)){
			sBuffer.append(" AND DATE_FORMAT(A.startDate,'%Y-%m-%d')<=?");
			params.add(endDate);
		}
		if(categoryid!=0){
			sBuffer.append(" AND A.categoryid=?");
			params.add(categoryid);
		}
		if(!StrKit.isBlank(isTop)&&(isTop.equals("1")||isTop.equals("0"))){
			sBuffer.append(" AND A.isTop=?");
			params.add(isTop);
		}
		
		sBuffer.append(" ORDER BY A.createDate DESC");
		//查询出符合条件的列表
		return WxArticle.me().paginate(pageNum, pageSize, "SELECT A.id,A.title,A.describe,A.createDate,A.isTop,B.name categoryName ", sBuffer.toString(),params.toArray());
	}
}
