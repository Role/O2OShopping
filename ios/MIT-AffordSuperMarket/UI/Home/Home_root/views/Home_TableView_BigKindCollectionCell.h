//
//  Home_TableView_BigKindCollectionCell.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/18.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： Home_Root_HeaderView
 Created_Date： 20160217
 Created_People： JSQ
 Function_description： 首页大类ConllertionViewCell
 ***************************************/
#import <UIKit/UIKit.h>

@interface Home_TableView_BigKindCollectionCell : UICollectionViewCell
- (void)updateViewWithData:(NSDictionary*)dataDic;
- (void)updateServiceViewWithData:(NSDictionary*)dataDic;
- (void)setTitleColor:(NSString*)colorName;
@end
