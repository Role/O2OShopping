package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.web.dao.GoodsStore;
import cn.com.dashihui.web.dao.GoodsStoreImages;

public class GoodsStoreService {
	/**
	 * 分页查找店铺产品
	 * @param pageNum
	 * @param pageSize
	 * @param storeid 店铺ID
	 * @param s 商品在售状态，1：在售，2：已下架，3：待审核
	 * @param c1,c2,c3,c4 四级分类
	 * @param keyword 搜索关键字
	 */
	public Page<Record> findByPage(int pageNum,int pageSize,int storeid,int s,int c1,int c2,int c3,int c4,int type,String keyword){
		StringBuffer sBuffer = new StringBuffer("FROM t_bus_goods A LEFT JOIN t_dict_store S ON A.storeid=S.id WHERE 1=1");
		List<Object> params = new ArrayList<Object>();
		if(storeid!=0){
			sBuffer.append(" AND A.storeid=?");
			params.add(storeid);
		}else{
			sBuffer.append(" AND A.isSelf=1");
		}
		if(s!=0){
			sBuffer.append(" AND A.state=?");
			params.add(s);
		}
		if(c4!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=? AND A.categorythid=? AND A.categoryfoid=?");
			params.add(c1);params.add(c2);params.add(c3);params.add(c4);
		}else if(c3!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=? AND A.categorythid=?");
			params.add(c1);params.add(c2);params.add(c3);
		}else if(c2!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=?");
			params.add(c1);params.add(c2);
		}else if(c1!=0){
			sBuffer.append(" AND A.categoryonid=?");
			params.add(c1);
		}
		if(type!=0){
			sBuffer.append(" AND A.type=?");
			params.add(type);
		}
		if(!StrKit.isBlank(keyword)){
			sBuffer.append(" AND A.name LIKE ?");
			params.add("%"+keyword+"%");
		}
		sBuffer.append(" ORDER BY A.storeid,A.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.id,S.title storeName,A.state,A.name,A.marketPrice,A.sellPrice,A.type,A.thumb,A.createDate,A.isSelf", sBuffer.toString(), params.toArray());
	}
	/**
	 * 查找店铺产品
	 */
	public GoodsStore findById(int id){
		return GoodsStore.me().findById(id);
	}
	/**
	 *增加便利店商品
	 */
	public boolean add(GoodsStore goods){
		return goods.save();
	}
	/**
	 *修改便利店商品
	 */
	public boolean update(GoodsStore goods){
		return goods.update();
	}
	/**
	 *查找出便利店商品
	 */
	public Record findGoodsById(int id){
		String sql="SELECT A.id,A.describe From t_bus_goods A where id=?";
	    return Db.findFirst(sql, id);
	}
	
	/**
	 * 图片排序
	 * @param sortMap
	 * @return
	 */
	public boolean sortImages(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_bus_goods_images SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	/**
	 * 商品图片列表
	 * @param goodsid
	 * @return
	 */
	public List<GoodsStoreImages> findAllImages(int goodsid){
		return GoodsStoreImages.me().find("SELECT A.* FROM t_bus_goods_images A WHERE A.goodsid=? ORDER BY A.orderNo",goodsid);
	}
	
	/**
	 * 添加图片
	 * @param newObject
	 * @return
	 */
	public boolean addImage(GoodsStoreImages newObject){
		return newObject.save();
	}
	
	/**
	 * 删除图片
	 * @param id
	 * @return
	 */
	public boolean delImage(int id){
		return GoodsStoreImages.me().deleteById(id);
	}
	
	/**
	 * 设置图片为商品logo
	 * @param id
	 * @return
	 */
	@Before(Tx.class)
	public boolean setImageLogo(int goodsid,int id){
		List<String> sqlList = new ArrayList<String>();
		//更新该商品所有LOGO的isLogo标识为0
		sqlList.add("UPDATE t_bus_goods_images SET isLogo=0 WHERE goodsid="+goodsid);
		//更新当前LOGO的isLogo标识为1
		sqlList.add("UPDATE t_bus_goods_images SET isLogo=1 WHERE goodsid="+goodsid+" AND id="+id);
		//更新该商品的LOGO字段值为当前LOGO的地址
		sqlList.add("UPDATE t_bus_goods SET thumb=(SELECT thumb FROM t_bus_goods_images WHERE id="+id+") WHERE id="+goodsid);
		Db.batch(sqlList, sqlList.size());
		return true;
	}
}