package cn.com.dashihui.api.controller;

import com.jfinal.kit.StrKit;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.service.AdService;

public class AdController extends BaseController{
	private AdService service = new AdService();
	
	/**
	 * 查询公共广告位
	 */
    public void publicAdList(){
		renderSuccess(service.findPublicAd());
    }
    
    /**
	 * 查询指定店铺的广告位
	 * @param STOREID 店铺ID
	 */
	public void storeAdList(){
		String storeidStr = getPara("STOREID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else{
			int storeid = Integer.valueOf(storeidStr);
			renderSuccess(service.findStoreAd(storeid));
		}
	}
}
