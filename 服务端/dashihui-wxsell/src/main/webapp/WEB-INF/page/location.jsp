<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title>定位</title>
	<!-- amazeui -->
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
	<!-- app -->
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
	<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_location.css" />
</head>
<body>
	<div class="am-g">
		<c:if test="${STORE!=null}">
		<div class="app-title">
			<i class="am-icon-location-arrow"></i> 当前位置
		</div>
		<div class="app-list-wrapper">
			<ul class="app-list">
			<li>${STORE.title}</li>
			</ul>
		</div>
		</c:if>
		<div class="app-title">
			<i class="am-icon-home"></i> 附近的小区
		</div>
		<div class="app-list-wrapper"><ul id="others" class="app-list"></ul></div>
	</div>

	<!-- include -->
	<%@include file="include.jsp"%>
	<!-- lib -->
	<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
	<!-- amazeui -->
	<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
	<!-- app -->
	<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript">
	wx.config({appId: '${appId}',timestamp: '${timestamp}',nonceStr: '${nonceStr}',signature: '${signature}',jsApiList: ['getLocation']});
	wx.ready(function(){
    	wx.getLocation({
		    type: 'wgs84',
		    success: function (res) {
				//云检索
				Kit.ajax.post("${BASE_PATH}/lbs",{location:(res.longitude+","+res.latitude)},function(result){
					if(result.flag==0){
						var data = result.object.lbs;
						if(data.status==0){
							if(data.total!=0){
								var size = data.contents.length;
								$(data.contents).each(function(index,item){
									var li = $('<li data-key='+item.uid+'>'+item.title+'</li>');
									$("#others").append(li);
									li.on("click",function () {
										var key = $(this).data("key");
										//保存定位
										Kit.ajax.post("${BASE_PATH}/locate",{baidukey:key},function(result){
											if(result.flag==0){
												Kit.render.redirect("${BASE_PATH}/index");
											}
										});
									});
								});
							}
						}
					}
				});
		    }
		});
	});
	</script>
</body>
</html>