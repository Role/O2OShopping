//artTemplate一些常用的自定义函数
/** 
 * 对日期进行格式化，详见custom-kit.js中的dateFormat方法说明，2015/10/11添加
 */
template.helper('dateFormat', Kit.util.dateFormat);
/**
 * 对标识进行转换，详见custom-kit.js中的flagTransform方法说明，2015/10/11添加
 */
template.helper('flagTransform', Kit.util.flagTransform);
/**
 * 对内容进行空判断，2015/10/11添加
 */
template.helper('isBlank', Kit.validate.isBlank);