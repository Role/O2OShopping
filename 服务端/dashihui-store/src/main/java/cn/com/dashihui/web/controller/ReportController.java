package cn.com.dashihui.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.kit.DatetimeKit;
import cn.com.dashihui.kit.MapKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.ReportService;

public class ReportController extends BaseController{
	private ReportService service = new ReportService();
    
	/**
	 * 统计店铺商品订单各订单状态数<br/>
	 * 首页店铺商品订单状态统计用
	 */
	public void orderStateCount(){
		renderSuccess(service.queryOrderStatesCount(getStoreid()));
	}
	
	/**
	 * 查询近一周内日店铺商品订单量及日成交订单量<br/>
	 * 首页图表用
	 */
	public void orderStateChart(){
		List<String> labels = new ArrayList<String>();
		List<String> datas1 = new ArrayList<String>();
		//所有订单
		List<Record> orderList1 = service.queryOrderOfSevenDay(getStoreid());
		for(Record item : orderList1){
			labels.add(item.getStr("date"));
			datas1.add(String.valueOf(item.getLong("total")));
		}
		List<String> datas2 = new ArrayList<String>();
		//成功的订单
		List<Record> orderList2 = service.queryFinishOrderOfSevenDay(getStoreid());
		for(Record item : orderList2){
			datas2.add(String.valueOf(item.getLong("total")));
		}
		//返回数据
		renderSuccess(MapKit.newInstance().put("labels", labels).put("datas1", datas1).put("datas2", datas2).getAttrs());
	}
	
	/**
	 * 统计商家服务订单各订单状态数<br/>
	 * 首页商家服务订单状态统计用
	 */
	public void serviceOrderStateCount(){
		renderSuccess(service.querySerOrderStatesCount(getStoreid()));
	}
	
	/**
	 * 查询近一周内日商家服务订单量及日成效订单量<br/>
	 * 首页图表用
	 */
	public void serviceOrderStateChart(){
		List<String> labels = new ArrayList<String>();
		List<String> datas1 = new ArrayList<String>();
		//所有订单
		List<Record> orderList1 = service.queryServiceOrderOfSevenDay(getStoreid());
		for(Record item : orderList1){
			labels.add(item.getStr("date"));
			datas1.add(String.valueOf(item.getLong("total")));
		}
		List<String> datas2 = new ArrayList<String>();
		//成功的订单
		List<Record> orderList2 = service.queryFinishServiceOrderOfSevenDay(getStoreid());
		for(Record item : orderList2){
			datas2.add(String.valueOf(item.getLong("total")));
		}
		//返回数据
		renderSuccess(MapKit.newInstance().put("labels", labels).put("datas1", datas1).put("datas2", datas2).getAttrs());
	}
	
	/**
	 * 跳转至订单成交情况页面
	 */
	public void orderCount(){
		setAttr("today", DatetimeKit.getFormatDate("yyyy-MM-dd"));
		render("orderCount.jsp");
	}
	
	public void orderCountPage(){
		//时间范围
		String beginDate = getPara("beginDate");
		String endDate = getPara("endDate");
		List<Record> list = service.queryOrderCountPage(getStoreid(),beginDate,endDate);
		if(list!=null){
			double amountTotal = 0;
			int orderCountTotal = 0, userCountTotal = 0;
			for(Record item : list){
				amountTotal = amountTotal + item.getBigDecimal("amount").doubleValue();
				orderCountTotal = orderCountTotal + item.getInt("orderCount");
				userCountTotal = userCountTotal + item.getInt("userCount");
			}
			//总金额保留两位小数
			amountTotal = new BigDecimal(amountTotal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			list.add(new Record().set("amount", amountTotal).set("orderCount", orderCountTotal).set("userCount", userCountTotal));
		}
		renderResult(0,list);
	}
	
	/**
	 * 跳转至商品成交排名页面
	 */
	public void goodsRank(){
		render("goodsRank.jsp");
	}
	
	public void goodsRankPage(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		//时间范围
		String beginDate = getPara("beginDate");
		String endDate = getPara("endDate");
		renderResult(0,service.queryGoodsRankPage(pageNum,pageSize,getStoreid(),beginDate,endDate));
	}
	
	/**
	 * 跳转至服务订单成交情况页面
	 */
	public void serviceOrderCount(){
		setAttr("today", DatetimeKit.getFormatDate("yyyy-MM-dd"));
		render("serviceOrderCount.jsp");
	}
	
	public void serviceOrderCountPage(){
		//时间范围
		String beginDate = getPara("beginDate");
		String endDate = getPara("endDate");
		List<Record> list = service.queryServiceOrderCountPage(getStoreid(),beginDate,endDate);
		if(list!=null){
			double amountTotal = 0;
			int orderCountTotal = 0, userCountTotal = 0;
			for(Record item : list){
				amountTotal = amountTotal + item.getBigDecimal("amount").doubleValue();
				orderCountTotal = orderCountTotal + item.getInt("orderCount");
				userCountTotal = userCountTotal + item.getInt("userCount");
			}
			//总金额保留两位小数
			amountTotal = new BigDecimal(amountTotal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			list.add(new Record().set("amount", amountTotal).set("orderCount", orderCountTotal).set("userCount", userCountTotal));
		}
		renderResult(0,list);
	}
	
	/**
	 * 跳转至服务商成交排名页面
	 */
	public void serviceShopRank(){
		render("serviceShopRank.jsp");
	}
	
	public void serviceShopRankPage(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		//时间范围
		String beginDate = getPara("beginDate");
		String endDate = getPara("endDate");
		renderResult(0,service.queryServiceShopRankPage(pageNum,pageSize,getStoreid(),beginDate,endDate));
	}
	
	/**
	 * 跳转至服务项成交排名页面
	 */
	public void serviceItemRank(){
		render("serviceItemRank.jsp");
	}
	
	public void serviceItemRankPage(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		//时间范围
		String beginDate = getPara("beginDate");
		String endDate = getPara("endDate");
		renderResult(0,service.queryServiceItemRankPage(pageNum,pageSize,getStoreid(),beginDate,endDate));
	}
}
