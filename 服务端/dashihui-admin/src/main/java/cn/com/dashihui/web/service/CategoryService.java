package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Category;

public class CategoryService{
	
	/**
	 * 查找出所有产品分类
	 * 2015-10-26
	 */
	public List<Category> findAllCategory(){
		return trim(Category.me().find("SELECT * FROM t_dict_category r ORDER BY categoryNo ASC , categoryCreateDate DESC"));
	}
	/**
	 * 对分类进行等级分类
	 * 2015-10-26
	 */
	private List<Category> trim(List<Category> CategoryList){
		if(CategoryList==null||CategoryList.size()<=0){
			return null;
		}
		List<Category> listTypeone=new ArrayList<Category>();
		List<Category> listTypetwo=new ArrayList<Category>();
		List<Category> listTypethree=new ArrayList<Category>();
		List<Category> listTypefour=new ArrayList<Category>();
		
		//进行分类
		for(Category Category : CategoryList){
			if(Category.getInt("categoryType").intValue()==1){
				listTypeone.add(Category);
			}else if(Category.getInt("categoryType").intValue()==2){
				listTypetwo.add(Category);
			}else if(Category.getInt("categoryType").intValue()==3){
				listTypethree.add(Category);
			}else{
				listTypefour.add(Category);
			}
		}
		//组合4-3
		if(listTypefour.size()!=0){
			for(Category c4 : listTypefour){
				for(Category c3 : listTypethree){
					if(c4.getInt("categoryFatherId").equals(c3.getInt("categoryId"))){
						c3.addChild(c4);
						break;
					}
				}
			}
		}
		//组合3-2
		if(listTypethree.size()!=0){
			for(Category c3 : listTypethree){
				for(Category c2 : listTypetwo){
					if(c3.getInt("categoryFatherId").equals(c2.getInt("categoryId"))){
						c2.addChild(c3);
						break;
					}
				}
			}
		}
		//组合2-1
		if(listTypetwo.size()!=0){
			for(Category c2 : listTypetwo){
				for(Category c1 : listTypeone){
					if(c2.getInt("categoryFatherId").equals(c1.getInt("categoryId"))){
						c1.addChild(c2);
						break;
					}
				}
			}
		}
		return listTypeone;
	}
	/**
	 * 根据CategoryId查找出分类
	 * 2015-10-26
	 */
	public Category findById(int categoryId){
		return Category.me().findFirst("SELECT * FROM t_dict_category WHERE categoryId=?",categoryId);
	}
	/**
	 * 保存分类
	 * 2015-10-26
	 */
	public boolean addCategory(Category newObject){
		return newObject.save();
	}
	/**
	 * 修改分类
	 * 2015-10-26
	 */
	public boolean editCategory(Category object){
		return object.update();
	}
	
	/**
	 * 根据父CategoryId删除分类批量删除
	 * 2015-10-26
	 */
	public void delBathByFatherId( int categoryId){
		List<Integer> list= new ArrayList<Integer>();
		List<Record>listTwo=getByFatherId(categoryId);
		list.add(categoryId);
		for(Record reTwo:listTwo){
			int idtwo=reTwo.getInt("CategoryId");
			List<Record> listThree=getByFatherId(idtwo);
			for(Record reThree:listThree){
				int idthree=reThree.getInt("CategoryId");
				list.add(idthree);
			}
		}
		List<String> sqlList=new ArrayList<String>();
		for(int id:list){
			if(list.size()>1){
				String strDel="DELETE FROM t_dict_category WHERE categoryFatherId="+id;
				sqlList.add(strDel);
			}
		}
		sqlList.add("DELETE FROM t_dict_category WHERE categoryId="+categoryId);
		Db.batch(sqlList, list.size());
	}
	/**
	 * 保存分类排序
	 * 2015-10-26
	 */
	public void sortCategory(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_dict_category SET categoryNo="+sortMap.get(id)+" WHERE categoryId="+id);
		}
		Db.batch(sqlList,batchSize);
	}
	/**
	 * 根据fatherId找到子节点 
	 * 2015-10-22
	 */
	private List<Record> getByFatherId(int categoryFatherId){
		return  Db.find("SELECT * FROM t_dict_category WHERE categoryFatherId=?",categoryFatherId);
	}
}


