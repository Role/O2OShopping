<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		rules:{link:{url:true}},
		messages:{
			goodsTitle: {required: "请选择商品"},
			link: {required: "请输入正确的链接地址"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 2:
						Kit.alert("请上传图片");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						onEditSuccess(data.object);
						editDialog.close();
					}
				}
			});
		}
	});
});
function onTypeSelected(value,name){
	if(value==1){
		$("#onNormalType").show();
		$("#onLinkGoodsType").hide();
	}else{
		$("#onNormalType").hide();
		$("#onLinkGoodsType").show();
	}
}
var setGoodsDialog;
function toLinkGoods(){
	setGoodsDialog = Kit.dialog("选择商品","${BASE_PATH}/ad/toSetGoods",{closable:true,size:"size-wide"}).open();
}
function onGoodsChoosed(id,name){
	$("#goodsid").val(id);
	$("#goodsTitle").val(name);
	$("#goodsTitle").valid();
}
//-->
</script>
<form id="editForm" action="${BASE_PATH}/ad/doEdit" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">
		<label class="col-lg-2 control-label">图片</label>
		<div class="col-lg-9">
			<input type="file" id="thumb" name="thumb" accept="image/jpg" title="选择文件" <c:if test="${object.thumb!=null}">data-val="${object.thumb}"</c:if>>
		</div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">类型</label>
	    <div class="col-lg-9">
	    	<select name="type" class="selectpicker form-control" data-callback="onTypeSelected">
				<option value="1" <c:if test="${object.type==1}">selected="selected"</c:if>>普通链接</option>
				<option value="2" <c:if test="${object.type==2}">selected="selected"</c:if>>链接商品</option>
	        </select>
	    </div>
	</div>
	<div id="onLinkGoodsType" class="form-group" <c:if test="${object.type==1}">style="display:none;"</c:if>>
	    <label class="col-lg-2 control-label">商品</label>
	    <div class="col-lg-9">
        	<div class="input-group">
				<input type="text" class="form-control" id="goodsTitle" name="goodsTitle" value="${object.goodsName}" readonly="readonly" required placeholder="请选择关联商品">
				<span class="input-group-btn"><button class="btn btn-primary" type="button" onclick="toLinkGoods()">选择</button></span>
			</div>
	    </div>
	</div>
	<input type="hidden" id="goodsid" name="goodsid" value="${object.goodsid}">
	<div id="onNormalType" class="form-group" <c:if test="${object.type==2}">style="display:none;"</c:if>>
	    <label class="col-lg-2 control-label">链接</label>
	    <div class="col-lg-9">
        	<input type="text" name="link" value="${object.link}" class="form-control" placeholder="请输入链接地址" maxlength="200">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">显示</label>
	    <div class="col-lg-9">
	    	<ul class="iCheckList">
	    		<li><input type="checkbox" class="iCheck" name="isShowOnAPP" value="1" <c:if test="${object.isShowOnAPP==1}">checked</c:if>> APP</li>
	    		<li><input type="checkbox" class="iCheck" name="isShowOnWX" value="1" <c:if test="${object.isShowOnWX==1}">checked</c:if>> 微商城</li>
	    	</ul>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="thumbOld" value="${object.thumb}"/>
	<input type="hidden" name="adid" value="${object.id}"/>
</form>