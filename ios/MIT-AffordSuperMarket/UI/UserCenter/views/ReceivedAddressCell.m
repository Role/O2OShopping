//
//  ReceivedAddressCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "ReceivedAddressCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#define kleftSpace 15
#define ktopSpace 10
#define kcellHight HightScalar(72)
@interface ReceivedAddressCell ()
@property(nonatomic, strong)UILabel *nameAndTelNumLabel;
@property(nonatomic, strong)UILabel *addressLabel;
@property(nonatomic, strong)UIButton *markBtn;
@end
@implementation ReceivedAddressCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryNone;
        [self setupSubViewWithCellStyle:style];
    }
    return self;
}

#pragma mark -- 初始化视图
- (void)setupSubViewWithCellStyle:(UITableViewCellStyle)style {
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, kcellHight)];
    bottomView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    [self.contentView addSubview:bottomView];
    if (!_nameAndTelNumLabel) {//电话和名字
        _nameAndTelNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                       , ktopSpace
                                                                       , VIEW_WIDTH - 2*kleftSpace - 30
                                                                       , (kcellHight - 2*ktopSpace)/2)];
        _nameAndTelNumLabel.textColor = [UIColor colorWithHexString:@"#555555"];
        _nameAndTelNumLabel.textAlignment = NSTextAlignmentLeft;
        _nameAndTelNumLabel.font = [UIFont systemFontOfSize:FontSize(16)];
        _nameAndTelNumLabel.backgroundColor = [UIColor clearColor];
        [bottomView addSubview:_nameAndTelNumLabel];
    }
    
    if (!_markBtn) {//修改图标
        UIImage *btnImage = [UIImage imageNamed:@"changeXiaoQuImage"];
        _markBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _markBtn.frame = CGRectMake(VIEW_WIDTH - 44, 0, 44, kcellHight);
        _markBtn.backgroundColor = [UIColor clearColor];
        [_markBtn setImage:btnImage forState:UIControlStateNormal];
        [_markBtn addTarget:self action:@selector(modifiedReceivedAddresssEvent) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:_markBtn];
    }
    CGFloat x = kleftSpace;
    if (style == UITableViewCellStyleValue1) {//带默认图标标示
        if (!_defautMarkLabel) {
            NSString *defautMarkText = @"默认";
            CGSize defautMarkTextSize = [defautMarkText sizeWithFont:[UIFont systemFontOfSize:FontSize(14)] maxSize:CGSizeMake(44, CGRectGetHeight(_nameAndTelNumLabel.bounds))];
            
            _defautMarkLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_nameAndTelNumLabel.frame)
                                                                        , CGRectGetMaxY(_nameAndTelNumLabel.frame)
                                                                        ,defautMarkTextSize.width + 10
                                                                        , (kcellHight - 2*ktopSpace)/2)];
            _defautMarkLabel.textColor = [UIColor colorWithHexString:@"#ffffff"];
            _defautMarkLabel.textAlignment = NSTextAlignmentCenter;
            _defautMarkLabel.font = [UIFont systemFontOfSize:FontSize(14)];
            _defautMarkLabel.backgroundColor = [UIColor colorWithHexString:@"#c52720"];
            _defautMarkLabel.text = defautMarkText;
            [_defautMarkLabel.layer setMasksToBounds:YES];
            [_defautMarkLabel.layer setCornerRadius:FontSize(11)];
            [bottomView addSubview:_defautMarkLabel];
           
            
        }
         x = CGRectGetMaxX(_defautMarkLabel.frame) + 4;
    } else if (style == UITableViewCellStyleValue2) {
         x = kleftSpace;
    }
    
    if (!_addressLabel) {
        _addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(x
                                                                 , CGRectGetMaxY(_nameAndTelNumLabel.frame)
                                                                 , VIEW_WIDTH - x - kleftSpace - 30
                                                                 , CGRectGetHeight(_nameAndTelNumLabel.bounds))];
        _addressLabel.textColor = [UIColor colorWithHexString:@"#999999"];
        _addressLabel.textAlignment = NSTextAlignmentLeft;
        _addressLabel.font = [UIFont systemFontOfSize:FontSize(15)];
        _addressLabel.minimumScaleFactor = 0.6;
        _addressLabel.adjustsFontSizeToFitWidth = YES;
        _addressLabel.backgroundColor = [UIColor clearColor];
        [bottomView addSubview:_addressLabel];
    }
}
#pragma mark -- button Action
- (void)modifiedReceivedAddresssEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(selectedReceivedAddressWithItemIndex:)]) {
        [_delegate selectedReceivedAddressWithItemIndex:self.itemIndexPath.row];
    }
}
//更新数据
- (void)updataWithName:(NSString*)name telNumber:(NSString*)telNumber address:(NSString*)address {
    NSString *nameAndTelNumStr = [NSString stringWithFormat:@"%@    %@",name,telNumber];
    self.nameAndTelNumLabel.text = nameAndTelNumStr;
    self.addressLabel.text = address;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
