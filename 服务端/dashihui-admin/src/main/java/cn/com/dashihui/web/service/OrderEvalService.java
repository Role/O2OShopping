package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class OrderEvalService {
	
	public Page<Record> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_bus_order_eval boe INNER JOIN t_bus_order bo ON boe.orderNum=bo.orderNum AND bo.isSelf=1");
		sqlExcept.append(" ORDER BY boe.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT boe.* ", sqlExcept.toString());
	}
}
